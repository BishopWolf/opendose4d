# Information for developers

This is the official repository for the `Slicer` extension `OpenDose4D`.

## Setup

To activate and use this module please follow these steps:

1. Either `unzip` the source code or `clone` it from Gitlab.
2. Add the respective path in `Slicer` by *Edit -> Application settings -> Modules -> Additional module paths -> Add -> OK*.
3. `Restart` Slicer to apply the changes.
4. `Choose` the module under *Modules -> Nuclear Medicine -> MRT OpenDose 4D*.

Note that you don't need to build Slicer from scratch but rather can rely on the provided [installers](https://download.slicer.org/). OpenDose 4D thereby builds upon the `Preview Release`, which supports `Python 3`.

## Testing

The tests in `Logic/Dosimetry4DLogic` can be run in `Slicer` itself. Therefore, the module has to be already imported and activated.

Additionally, you have to enable the `developer mode` under *Edit -> Application settings -> Developer -> Enable developer mode -> OK*.

After reloading Slicer, you should see a column `Reload and Test`. To run the tests and changes to the source code, just click this button. This reimports the module with the current changes and runs the code against the given tests.

When adding new test, please stay conform with the general structure of this module's testing:

```python
def method_parameter_state(self):
        self.logger.info('Test %s started!', inspect.stack()[0][3])

        # arrange
        some_setup()

        # act
        some_method_calling()

        # assert
        some_assertion()

        self.logger.info('Test %s passed!', inspect.stack()[0][3])
```

## Ressources

The following link collection should facilitate understanding the code in this extension and the bigger of applied concepts:

- [Slicer Utils](https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/util.py)
- [DICOM Introduction](https://www.slicer.org/wiki/Documentation/Nightly/FAQ/DICOM)
- [DICOM Structure](https://www.slicer.org/wiki/Documentation/4.0/Modules/CreateDICOMSeries)
- [DICOM Browser](https://dicom.innolitics.com/ciods)
- [Subject Hierarchy](https://www.slicer.org/wiki/Documentation/4.5/Modules/SubjectHierarchy)

## Sample Data

The example data is part of this package and it is located in the folder `Resources`. It contains a DICOM dataset provided by the IAEA CRP E2.30.05 on “Dosimetry in Radiopharmaceutical therapy for personalized patient treatment”. It also contains a sample segmentation.

## Continuous Integration (CI)

Since this extension will be used for clinical research, the software quality and correctness of calculation have top priority for the project team. To ensure maximal stability and validity of the extension's calculations, basic software engineering principles should be met.

Contributions to the code have to be peer-reviewed in a pull request process. In order to verify the correctness of the code, the contributed code has to be shipped with appropriate testing. In order to automate this effort `gitlab-ci`, `docker` and `sonarqube` are used. A theoretical introduction to `Continuous Integration` can be found on [Gitlab](https://about.gitlab.com/2019/07/12/guide-to-ci-cd-pipelines/).

### Docker

`Docker` is a technology, which allows to create images that can be applied as `shared environment` in testing in productive deploying. For more detailled information, please refer to their [starting guide](https://docs.docker.com/engine/docker-overview/).

The basic docker image used for the CI-Pipeline `unnmdnwb3/slicer3d-nightly:0.7.0`, is publicly accessable on [github](https://github.com/unnmdnwb3/slicer3d-nightly) and [dockerhub](https://cloud.docker.com/u/unnmdnwb3/repository/docker/unnmdnwb3/slicer3d-nightly). Is is currently based on a fixed commit for the Slicer branch `nightly-master` and supports the use of `python3`. For detailled information, please head to its github.

### Sonarqube

Sonarqube is used for `static code analysis`, which takes the code at `compile-time` and runs it against `qualitity gates`. In the future, we want to support `sonarcloud`, which would allow to use its cloud service to test these metrics continuously.

To download `sonarqube` for local use, head to their official [website](https://www.sonarqube.org/downloads/).

To use sonarqube, you need to have a `java` installation. Further details are listed as [requirements](https://docs.sonarqube.org/latest/requirements/requirements/). On `mac`, you can install the newest distribution using `brew`:

```bash
brew cask install java
```

To start a local sonarqube instance, use the following commands:

```bash
echo "alias sonarqube=\'$HOME/sonarqube-7.9.1/bin/macosx-universal-64/sonar.sh console\'" >> ~/.bashrc
source ~/.bashrc
sonarqube
```

You can now log in using the default credentials: 'admin / admin'.

As a next step, install `sonar-scanner` using the distribution of choice. Subsequently, include sonar-scanner to your `PATH`:

```bash
echo export PATH="$PATH:$HOME/sonar-scanner-4.0.0.1744-macosx/bin" >> ~/.bashrc
source ~/.bashrc
```

To test if correctly configured:

```bash
sonar-scanned -h
```

To run `sonar-scanner` using the `sonar-project.properties`, be sure to be in the `root` of the project and type:

```bash
sonar-scanner
```

If a local sonarqube instance is running, you should now be able to review the results on <http://localhost:9000/projects>.

### Gitlab-CI

Currently, Gitlab-CI is our platform of choice for the `continuous integration`. The only thing needed for Gitlab to run all test for a new push or merge, is a the `.gitlab-ci.yml` at the project's root directory.

During CI, a production-like environment is built using `docker` and subsequently all tests are run to verify the code's integrity. If the testing fails, the build fails as well.

You can test the `.gitlab-ci.yml` locally using the `gitlab-runner`, without obfuscating the git-history. Please download a runner of choice, on [gitlab's website](https://docs.gitlab.com/runner/install/). The runner can be deployed on the host-OS, docker or other services.

```bash
gitlab-runner exec <service> <job>
```

One of gitlab-runner's [several limits](https://docs.gitlab.com/runner/commands/#limitations-of-gitlab-runner-exec) is that only one job can be run at once. Since gitlab-ci seemingly does not activly support passing an image between stages and jobs, we use a single job `build-and-test` for the whole process.

```bash
gitlab-runner exec docker build-and-test
```

Please be wary though, that `gitlab-runner` always builds your code based on your **last commit** rather than the current (uncommited) state!

### Sentry

This is a tool to capture, store, analyze and organize errors across the user-base. You can find more high level informations in their [documentation](https://docs.sentry.io/error-reporting/quickstart/?platform=python).

Important to note for our extension are the following points:

`sentry` attaches itself to `logging`, hence by default every log would cause sentry to capture the respective error. We do not want this behaviour, since it inflates and obliterates the `issues` on sentry - and raises the costs for us.

Hence, we use `envs` with values `prod` and `test`. Only in production, errors are sent to sentry. `logging` is wrapped to create a env-aware logger and thereby include the current env, so that we don't polute the code with this state.

But we still don't want to send every error in `prod`, hence we the error is only relay when using the log-level `critical` (`logging.critical`). In order to then send additional data, which makes the analysis easier, please use `extras` - which can be wrapped with an exception using:

```python
sentry.extras(exception, data)
```

Hence, only logs with the following convention actually push to sentry, when in prod:

```python
logging.critical('Some message', extras=sentry.extras(err, data))
```

Additionally, we don't want our users to send critical data about their patients. Therefore we have to anonymize especially dicom-data before the the error is sent to sentry.

## Contribute

If you'd like to contribute, you can find an orientation on the Slicer [documentation for developers](https://www.slicer.org/wiki/Documentation/Nightly/Developers).

## License

OpenDose 4D is subject to the `Apache License`, which is deposited in the project's root.