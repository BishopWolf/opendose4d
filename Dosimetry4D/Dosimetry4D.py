import importlib
import json
from pathlib import Path
import sqlite3
import ctk
import qt

import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModule, ScriptedLoadableModuleWidget

#Check requirements
from Logic import utils
utils.checkRequirements()

from Logic import Dosimetry4DLogic, Dosimetry4DTest, vtkmrmlutils, vtkmrmlutilsTest, xmlutilsTest, nodesTest, logging, sentry, utils, xmlexport, dbutils, gate
from Logic.fit_values import FIT_FUNCTIONS
import Logic.config as config
import Logic.gate as gate
from simpleeval import SimpleEval

__submoduleNames__ = ['Dosimetry4DLogic', 'Dosimetry4DTest', 'fit_values', 'fitutils', 'PhysicalUnits', 'vtkmrmlutils', 'vtkutils', 'constants',
                      'vtkmrmlutilsTest', 'testbuilder', 'testutils', 'xmlutils', 'xmlutilsTest', 'nodes', 'nodesTest', 'errors', 'Process',
                      'attributes', 'xmlconstants', 'config', 'sentry', 'logging', 'utils', 'xmlexport', 'dbutils', 'gate', 'singleton']
__package__ = 'Dosimetry4D'
mod = importlib.import_module('Logic', __name__)
importlib.reload(mod)
__all__ = ['Dosimetry4D', 'Dosimetry4DWidget',
           'Dosimetry4DLogic', 'Dosimetry4DTest']


#######################
#                     #
# Dosimetry4D         #
#                     #
#######################


class Dosimetry4D(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent):
        super().__init__(parent)
        self.script_path = utils.getScriptPath()
        # TODO make this more human readable by adding spaces
        self.parent.title = "OpenDose Dosimetry 4D"
        self.parent.categories = ["Radiotherapy"]
        self.parent.dependencies = []
        # replace with "Firstname Lastname (Organization)"
        self.parent.contributors = [
            "Alex Vergara Gil (INSERM, France)", "Janick Rueegger (KSA, Switzerland)", "Ludovic Ferrer (NANTES, France)"]
        self.parent.helpText = '''
            This module implements the full 3D Dosimetry for molecular radiotherapy on multiple time points.\n
            Naming convention: \n
              Volumes must contain the time in hours like 4HR\n
              Volumes must contain the identifier "CTCT" for CT and "ACSC" for SPECT\n
              By pressing the Clean Scene button all files that does not follow this convention will be removed from scene!!\n
            TODO: write wiki
        '''
        self.parent.helpText += self.getDefaultModuleDocumentationLink()
        self.parent.acknowledgementText = '''
            This software was originally developed by Alex Vergara Gil (INSERM, France) and Janick Rueegger (KSA, Switzerland),
            in collaboration with Ludovic Ferrer (NANTES, France) and Thiago NM Lima (KSA, Switzerland).
            TODO: redact better
        '''  # replace with organization, grant and thanks.

        sentry.init()

    def runTest(self, msec=100, **kwargs):
        """
        :param msec: delay to associate with :func:`ScriptedLoadableModuleTest.delayDisplay()`.
        """
        # test vtkmrmlutilsTest
        testCase = vtkmrmlutilsTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test nodesTest
        testCase = nodesTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test xmlutilsTest
        testCase = xmlutilsTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test Dosimetry4dTest
        # name of the test case class is expected to be <ModuleName>Test
        module = importlib.import_module(self.__module__)
        className = self.moduleName + 'Test'
        try:
            TestCaseClass = getattr(module, className)
        except AttributeError:
            # Treat missing test case class as a failure; provide useful error message
            raise AssertionError(
                f'Test case class not found: {self.__module__}.{className} ')

        testCase = TestCaseClass()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        print('\n******* All tests passed **********\n')


#######################
#                     #
# Dosimetry4DWidget   #
#                     #
#######################


class Dosimetry4DWidget(ScriptedLoadableModuleWidget):
    """Uses ScriptedLoadableModuleWidget base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setupOptions(self):
        self.options = {}
        self.optionsFile = self.script_path / "Resources" / "JSON" / "options.json"
        if not self.optionsFile.exists():
            self.options = {
                'Isotope': 2, 'InjectionActivity': '6848', 'usePath': str(self.usePath)}
            with self.optionsFile.open('w') as f:
                f.write(json.dumps(self.options))
        else:
            with self.optionsFile.open('r') as f:
                self.options = json.load(f)
        self.usePath = Path(self.options['usePath'])
        self.calibration = {}
        self.calibrationFile = self.script_path / \
            "Resources" / "JSON" / "calibration.json"
        if self.calibrationFile.exists():
            with self.calibrationFile.open('r') as f:
                self.calibration = json.load(f)

    def saveOptions(self):
        if self.optionsFile.exists():
            self.optionsFile.unlink()
        self.options = {'Isotope': self.isotopsList.currentIndex,
                        'InjectionActivity': self.InjectionActivity.text,
                        'usePath': str(self.usePath)}
        with self.optionsFile.open('w') as f:
            f.write(json.dumps(self.options))

    def setup(self):
        ''' Creation of the Module Widgets
            This procedure set up all widgets and define all conections
            TODO: Add CT calibration curve
        '''
        self.script_path = utils.getScriptPath()
        self.usePath = self.script_path

        # self.developerMode = False # Comment to show developer mode
        super().setup()
        self.setupOptions()
        self.logger = logging.getLogger('Dosimetry4D')
        self.logger.setLevel(logging.WARNING)
        self.DoseCalculated = False
        self.SegmentationPropagated = False
        self.Evaluator = SimpleEval()
        self.db = self.script_path / "Resources" / "Dosimetry4D.db3"

        # Instantiate and connect widgets ...

        #
        # Start of Parameters Area
        #
        ''' This section is for calibration data (CT, SPECT) and for general data input like isotope.
        '''

        ParametersCollapsibleButton = ctk.ctkCollapsibleButton()
        ParametersCollapsibleButton.text = "Parameters"
        self.layout.addWidget(ParametersCollapsibleButton)
        ParametersFormLayout = qt.QFormLayout(ParametersCollapsibleButton)

        calibrationHContainer = qt.QHBoxLayout()

        label = qt.QLabel("Select clinical center")
        calibrationHContainer.addWidget(label)

        self.sensitivity = config.readSPECTSensitivity()
        centers = list(self.sensitivity.keys())
        self.ClinicalCenter = qt.QComboBox()
        self.ClinicalCenter.setToolTip(
            "Create your center in Calibration module")
        self.ClinicalCenter.addItems(centers)
        calibrationHContainer.addWidget(self.ClinicalCenter)

        label = qt.QLabel("Select calibration date")
        calibrationHContainer.addWidget(label)

        self.calibrationDate = qt.QComboBox()
        self.calibrationDate.setToolTip(
            "Create calibration data in Calibration module")
        calibrationHContainer.addWidget(self.calibrationDate)

        ParametersFormLayout.addRow(calibrationHContainer)

        # isotopes
        self.isotopsList = qt.QComboBox()
        isotopes = {}
        with sqlite3.connect(str(self.db)) as connectDosimetry:
            cursor = connectDosimetry.cursor()
            sql = 'SELECT * FROM isotopes'
            rows = cursor.execute(sql).fetchall()
            for row in rows:
                isotopes[row[1]] = row
        self.defaultIsotopesList = isotopes.keys()
        self.isotopsList.addItems(sorted(self.defaultIsotopesList))
        self.isotopsList.setCurrentIndex(int(self.options['Isotope']))
        ParametersFormLayout.addRow("Isotope", self.isotopsList)

        # SPECT sensitivity (MBq/counts)
        sensitivityHContainer = qt.QHBoxLayout()
        self.sensitivityUnits = qt.QComboBox()
        self.sensitivityUnits.addItems(['Bq/counts', 'MBq/counts', 'counts/Bq', 'counts/MBq',
                                        'Bqs/counts', 'MBqs/counts', 'counts/Bqs', 'counts/MBqs'])
        index = self.sensitivityUnits.findText(
            self.calibration['SPECTSensitivity']['Units'], qt.Qt.MatchFixedString)
        if index > 0:
            self.sensitivityUnits.setCurrentIndex(index)

        self.spectSensitivity = qt.QLineEdit()
        self.spectSensitivity.setText(
            str(self.calibration['SPECTSensitivity']['Value']))
        self.spectSensitivity.setToolTip("Camera factor, select proper unit.")

        sensitivityHContainer.addWidget(self.spectSensitivity)
        sensitivityHContainer.addWidget(self.sensitivityUnits)
        ParametersFormLayout.addRow("Camera factor", sensitivityHContainer)

        self.calibrationTimeContainer = qt.QFrame()
        layout = qt.QHBoxLayout()
        layout.addStretch()
        label = qt.QLabel("Calibration Time (seconds)")
        layout.addWidget(label)
        self.spectCalibrationTime = qt.QLineEdit()
        self.spectCalibrationTime.setText(
            str(self.calibration['SPECTSensitivity']['Time']))
        layout.addWidget(self.spectCalibrationTime)
        self.calibrationTimeContainer.setLayout(layout)
        ParametersFormLayout.addRow(self.calibrationTimeContainer)
        self.setCalibrationTimeVisibility()

        # Injection Activity
        self.InjectionActivity = qt.QLineEdit()
        self.InjectionActivity.setText(str(self.options['InjectionActivity']))
        ParametersFormLayout.addRow(
            "Injected Activity (MBq)", self.InjectionActivity)

        self.isotopsList.currentTextChanged.connect(self.onIsotopeChange)
        self.ClinicalCenter.currentTextChanged.connect(self.onIsotopeChange)
        self.calibrationDate.currentTextChanged.connect(
            self.onCalibrationDateChanged)
        self.sensitivityUnits.currentIndexChanged.connect(
            self.setCalibrationTimeVisibility)

        #
        # End of Parameters Area
        #

        #
        # Start of Preprocessing Area
        #
        ''' This section is for preprocessing
              1. Rename files will create the required variables, will rename all files to a proper convention and will group everything in time points folders
              2. Resample CTs will make use of Resample Volume (BRAINS) to resample the CTs to SPECT resolution required by most methods
              3. Rescale will convert the CT volumes to density volumes and will apply sensitivity to SPECT so it will also create activity volumes
        '''

        PreprocessingCollapsibleButton = ctk.ctkCollapsibleButton()
        PreprocessingCollapsibleButton.text = "Preprocessing"
        self.layout.addWidget(PreprocessingCollapsibleButton)
        PreprocessingFormLayout = qt.QFormLayout(
            PreprocessingCollapsibleButton)
        PreprocessingHContainer = qt.QHBoxLayout()

        # rename files
        self.renameButton = qt.QPushButton("Rename files")
        self.renameButton.toolTip = "Rename original DICOM files and sort them by time point."
        self.renameButton.enabled = True
        PreprocessingHContainer.addWidget(self.renameButton)

        # resample the CTs
        self.resampleButton = qt.QPushButton("Resample CTs")
        self.resampleButton.toolTip = "Resample the CTs to SPECT resolution using Lanczos interpolation."
        self.resampleButton.enabled = False
        PreprocessingHContainer.addWidget(self.resampleButton)

        # scale the SPECT values and calculate density matrixes
        self.rescaleButton = qt.QPushButton("Rescale")
        self.rescaleButton.toolTip = "Rescale the SPECT values and calculate density matrixes."
        self.rescaleButton.enabled = False
        PreprocessingHContainer.addWidget(self.rescaleButton)

        PreprocessingFormLayout.addRow(PreprocessingHContainer)

        # connections
        self.renameButton.clicked.connect(self.onRenameButton)
        self.resampleButton.clicked.connect(self.onResampleButton)
        self.rescaleButton.clicked.connect(self.onRescaleButton)

        #
        # End of Preprocessing Area
        #

        #
        # Start of Registration Area
        #
        ''' This section is for registration of multiple time points.
              1. The registration is made CT to CT, you need to provide the reference CT to start with. Once is executed, a transformation will be applied to all volumes in each time point.
              2. User are expected to check the output of this module, is it is incorrect then go to transform module and manually adjust the transformations to get a closer registration in each time point. The finally run again the registration provided here.
              3. Users are not supposed to provide his own transformations as they will be unreadable by the functionality, follow strictly the steps: execute -> manual check -> execute
        '''

        registrationCollapsibleButton = ctk.ctkCollapsibleButton()
        registrationCollapsibleButton.text = "Registration"
        self.layout.addWidget(registrationCollapsibleButton)

        # Layout within the dummy collapsible button
        registrationFormLayout = qt.QFormLayout(registrationCollapsibleButton)

        # input volume selector
        self.initialPoint = slicer.qMRMLNodeComboBox()
        self.initialPoint.nodeTypes = ["vtkMRMLScalarVolumeNode"]
        self.initialPoint.selectNodeUponCreation = True
        self.initialPoint.addEnabled = False
        self.initialPoint.removeEnabled = False
        self.initialPoint.noneEnabled = False
        self.initialPoint.showHidden = False
        self.initialPoint.showChildNodeTypes = False
        self.initialPoint.setMRMLScene(slicer.mrmlScene)
        self.initialPoint.setToolTip("Select the reference Volume.")
        self.initialPoint.enabled = False
        registrationFormLayout.addRow("Reference Volume: ", self.initialPoint)

        # Execute Button
        transformationHContainer = qt.QHBoxLayout()
        self.registrationButton = qt.QPushButton("Execute")
        self.registrationButton.toolTip = "Execute the CT-CT registration."
        self.registrationButton.enabled = False
        transformationHContainer.addWidget(self.registrationButton)
        self.elastixContainer = qt.QHBoxLayout()
        self.installElastixButton = qt.QPushButton("Install Elastix")
        self.installElastixButton.toolTip = "Installs Slicer Elastix if it is not installed."
        self.installElastixButton.enabled = True
        transformationHContainer.addWidget(self.installElastixButton)
        emm = slicer.app.extensionsManagerModel()
        if emm.isExtensionInstalled("SlicerElastix"):
            self.installElastixButton.hide()
        self.switchtoTransformsButton = qt.QPushButton("Check Transforms")
        self.switchtoTransformsButton.toolTip = "Switches to Transforms Module for checking the transformations."
        self.switchtoTransformsButton.enabled = True
        transformationHContainer.addWidget(self.switchtoTransformsButton)
        registrationFormLayout.addRow(transformationHContainer)

        # connections
        self.registrationButton.clicked.connect(self.onRegistrationButton)
        self.installElastixButton.clicked.connect(lambda: self.onInstallModuleButton("SlicerElastix"))
        self.initialPoint.currentNodeChanged.connect(self.onInitialPointChange)
        self.switchtoTransformsButton.clicked.connect(
            lambda: self.switchModule('Transforms'))

        #
        # End of Registration Area
        #

        #
        # Start of Absorbed Dose Rate Calculation Area
        #
        ''' This section is for absorbed dose rate calculation. User has to select the proper method depending on study type. If more isotopes are needed then make a request.
        '''
        doseCollapsibleButton = ctk.ctkCollapsibleButton()
        doseCollapsibleButton.text = "Absorbed Dose Rate calculation"
        self.layout.addWidget(doseCollapsibleButton)

        # Layout within the dummy collapsible button
        doseFormLayout = qt.QFormLayout(doseCollapsibleButton)

        # threshold value
        self.activityThresholdSliderWidget = ctk.ctkSliderWidget()
        self.activityThresholdSliderWidget.singleStep = 0.1
        self.activityThresholdSliderWidget.minimum = 0
        self.activityThresholdSliderWidget.maximum = 10
        self.activityThresholdSliderWidget.value = 1
        self.activityThresholdSliderWidget.enabled = False
        self.activityThresholdSliderWidget.suffix = '%'
        self.activityThresholdSliderWidget.decimals = 1
        self.activityThresholdSliderWidget.setToolTip(
            "Set threshold value (%) for computing the Dose image. Voxels that have activity lower than this value will be set to zero.")
        doseFormLayout.addRow("Activity threshold (%)",
                              self.activityThresholdSliderWidget)

        # Dose Rate calculation algorithm
        self.doseRateAlgorithm = qt.QComboBox()
        Algorithms = ['Local Energy Deposition (LED)',
                      'FFT convolution (homogeneous)',
                      #   'Convolution (homogeneous)', # Removed by Manuel Bardies
                      'Convolution (heterogeneous)',
                      'Monte Carlo']
        self.doseRateAlgorithm.addItems(Algorithms)
        doseFormLayout.addRow(
            "Absorbed dose rate algorithm", self.doseRateAlgorithm)

        self.kernelDistanceLimitContainer = qt.QFrame()
        layout = qt.QHBoxLayout()
        layout.addStretch()
        label = qt.QLabel("kernel limit (mm)")
        layout.addWidget(label)
        self.kernelDistanceLimit = qt.QDoubleSpinBox()
        self.kernelDistanceLimit.setDecimals(1)
        self.kernelDistanceLimit.setMinimum(0)
        self.kernelDistanceLimit.setMaximum(1000)
        self.kernelDistanceLimit.value = 0
        self.kernelDistanceLimit.setToolTip(
            "Maximum distance in mm to consider for the convolution kernel.\n A value of 0 mm means to use the entire kernel")
        layout.addWidget(self.kernelDistanceLimit)
        self.kernelDistanceLimitContainer.setLayout(layout)
        doseFormLayout.addRow(self.kernelDistanceLimitContainer)
        self.kernelDistanceLimitContainer.hide()

        # Calculate Dose Rate Button
        self.doseRateButton = qt.QPushButton("Calculate Dose Rate Images")
        self.doseRateButton.toolTip = "Execute the absorbed dose rate algorithm specified."
        self.doseRateButton.enabled = False
        doseFormLayout.addRow(self.doseRateButton)

        # Monte Carlo section
        self.MonteCarloContainer = qt.QFrame()
        MonteCarloHContainer = qt.QHBoxLayout()
        self.GenerateGateButton = qt.QPushButton("Generate Gate")
        self.GenerateGateButton.toolTip = "Generates Gate macros to run Monte Carlo."
        self.GenerateGateButton.enabled = True
        MonteCarloHContainer.addWidget(self.GenerateGateButton)
        self.RunGateButton = qt.QPushButton("Run Gate")
        self.RunGateButton.toolTip = "Run the generated Gate macros."
        self.RunGateButton.enabled = True
        MonteCarloHContainer.addWidget(self.RunGateButton)
        self.ImportGateButton = qt.QPushButton("Import Gate")
        self.ImportGateButton.toolTip = "Import Gate results."
        self.ImportGateButton.enabled = True
        MonteCarloHContainer.addWidget(self.ImportGateButton)
        self.MonteCarloContainer.setLayout(MonteCarloHContainer)
        doseFormLayout.addRow(self.MonteCarloContainer)
        self.MonteCarloContainer.hide()

        # connections
        self.doseRateAlgorithm.currentIndexChanged.connect(
            self.onDoseRateAlgorithm)
        self.doseRateButton.clicked.connect(self.onDoseRateButton)
        self.GenerateGateButton.clicked.connect(self.onGenerateGate)

        #
        # End of Absorbed Dose Rate Calculation Area
        #

        #
        # Start of Segmentation Area
        #
        ''' This section is to handle with segmentations. User is supposed to provide a segmentation either by the use of Segment Editor module, or by importing his own segmentation. If RTStructs are imported for this reason they must be converted to slicer segments (they must contain a binary map)
              1. The segmentation is propagated to each time point
              2. The user is expected to modify each time point segmentation after propagation to match the movements of desired structures
        '''
        segmentationCollapsibleButton = ctk.ctkCollapsibleButton()
        segmentationCollapsibleButton.text = "Segmentation"
        self.layout.addWidget(segmentationCollapsibleButton)

        # Layout within the dummy collapsible button
        segmentationFormLayout = qt.QFormLayout(segmentationCollapsibleButton)

        # Segmentation Propagation
        self.segmentation = slicer.qMRMLNodeComboBox()
        self.segmentation.nodeTypes = ["vtkMRMLSegmentationNode"]
        self.segmentation.selectNodeUponCreation = True
        self.segmentation.addEnabled = False
        self.segmentation.removeEnabled = False
        self.segmentation.noneEnabled = False
        self.segmentation.showHidden = False
        self.segmentation.showChildNodeTypes = False
        self.segmentation.setMRMLScene(slicer.mrmlScene)
        self.segmentation.setToolTip("Select the reference segmentation.")
        self.segmentation.enabled = True
        segmentationFormLayout.addRow(
            "Reference Segmentation: ", self.segmentation)

        segmentationHContainer = qt.QHBoxLayout()
        self.propagateButton = qt.QPushButton("Propagate Segmentation")
        self.propagateButton.toolTip = "Propagates the segmentation to all time points."
        self.propagateButton.enabled = False
        segmentationHContainer.addWidget(self.propagateButton)
        self.installExtraEffectsButton = qt.QPushButton("Install extra effects")
        self.installExtraEffectsButton.toolTip = "Installs Segmentation Extra Effects if it is not installed."
        self.installExtraEffectsButton.enabled = True
        segmentationHContainer.addWidget(self.installExtraEffectsButton)
        if emm.isExtensionInstalled("SegmentEditorExtraEffects"):
            self.installExtraEffectsButton.hide()
        self.switchtoSegmentEditorButton = qt.QPushButton("Segment Editor")
        self.switchtoSegmentEditorButton.toolTip = "Switches to Segment Editor for creating the segmentation."
        self.switchtoSegmentEditorButton.enabled = True
        segmentationHContainer.addWidget(self.switchtoSegmentEditorButton)
        segmentationFormLayout.addRow(segmentationHContainer)

        # connections
        self.propagateButton.clicked.connect(self.onPropagateButton)
        self.installExtraEffectsButton.clicked.connect(lambda: self.onInstallModuleButton("SegmentEditorExtraEffects"))
        self.segmentation.currentNodeChanged.connect(self.onSelect)
        self.switchtoSegmentEditorButton.clicked.connect(
            lambda: self.switchModule('SegmentEditor'))

        #
        # End of Segmentation Area
        #

        #
        # Start of Creation of tables and Plots Area
        #
        ''' This section is for creation of tables and plots
              1. It will create activity(dose rate) tables for each segment in each time point, it also creates the segment densities tables (required for mass calculation)
              2. It will create the time plots of activity(dose rate)
              3. Plots are supposed to behave nicely, if this is not happening please check segmentation!!
        '''
        TablesandPlotsCollapsibleButton = ctk.ctkCollapsibleButton()
        TablesandPlotsCollapsibleButton.text = "Segment Tables and Plots"
        self.layout.addWidget(TablesandPlotsCollapsibleButton)
        TablesandPlotsFormLayout = qt.QFormLayout(
            TablesandPlotsCollapsibleButton)

        tablesHContainer = qt.QHBoxLayout()

        self.ACTMTablesButton = qt.QPushButton("Create ACTM Tables")
        self.ACTMTablesButton.toolTip = "Creates the activity tables for all structures in each time point."
        self.ACTMTablesButton.enabled = False
        tablesHContainer.addWidget(self.ACTMTablesButton)

        self.DOSETablesButton = qt.QPushButton("Create DOSE Tables")
        self.DOSETablesButton.toolTip = "Creates absorbed dose rate tables for all structures in each time point."
        self.DOSETablesButton.enabled = False
        tablesHContainer.addWidget(self.DOSETablesButton)

        TablesandPlotsFormLayout.addRow(tablesHContainer)

        plotsHContainer = qt.QHBoxLayout()

        self.ACTMPlotsButton = qt.QPushButton("Create ACTM Plots")
        self.ACTMPlotsButton.toolTip = "Creates activity/time plots for all structures."
        self.ACTMPlotsButton.enabled = False
        plotsHContainer.addWidget(self.ACTMPlotsButton)

        self.DOSEPlotsButton = qt.QPushButton("Create DOSE Plots")
        self.DOSEPlotsButton.toolTip = "Creates absorbed dose rate/time plots for all structures."
        self.DOSEPlotsButton.enabled = False
        plotsHContainer.addWidget(self.DOSEPlotsButton)

        TablesandPlotsFormLayout.addRow(plotsHContainer)

        # connections
        self.ACTMTablesButton.clicked.connect(self.onACTMTablesButton)
        self.DOSETablesButton.clicked.connect(self.onDOSETablesButton)
        self.ACTMPlotsButton.clicked.connect(self.onACTMPlotsButton)
        self.DOSEPlotsButton.clicked.connect(self.onDOSEPlotsButton)

        #
        # End of Creation of tables and Plots Area
        #

        #
        # Start Time Integration Area
        #
        TimeIntegrationCollapsibleButton = ctk.ctkCollapsibleButton()
        TimeIntegrationCollapsibleButton.text = "Time integration"
        self.layout.addWidget(TimeIntegrationCollapsibleButton)
        TimeIntegrationFormLayout = qt.QFormLayout(
            TimeIntegrationCollapsibleButton)
        timeintHContainer = qt.QHBoxLayout()

        self.incorporationMode = qt.QComboBox()
        self.incorporationMode.addItems(['linear', 'constant', 'exponential'])
        self.incorporationMode.toolTip = "Select incorporation part aproximation"
        TimeIntegrationFormLayout.addRow(
            'Incorporation Mode', self.incorporationMode)

        self.integrationMode = qt.QComboBox()
        self.integrationMode.addItems(
            ['trapezoid', *tuple(FIT_FUNCTIONS.keys()), 'auto-fit'])
        self.integrationMode.toolTip = "Select integration algorithm"
        TimeIntegrationFormLayout.addRow(
            'Integration Algorithm', self.integrationMode)

        self.IntegrateActivityButton = qt.QPushButton("Integrate Activity")
        self.IntegrateActivityButton.toolTip = "Integrates activity in time for all structures. Returns cumulative activity per segment"
        self.IntegrateActivityButton.enabled = False
        timeintHContainer.addWidget(self.IntegrateActivityButton)

        self.IntegrateDoseButton = qt.QPushButton(
            "Integrate Absorbed Dose Rate")
        self.IntegrateDoseButton.toolTip = "Integrates absorbed dose rates in time for all structures. Returns absorbed dose per segment"
        self.IntegrateDoseButton.enabled = False
        timeintHContainer.addWidget(self.IntegrateDoseButton)

        TimeIntegrationFormLayout.addRow(timeintHContainer)

        exportHContainer = qt.QHBoxLayout()

        self.ACTMExportButton = qt.QPushButton("Export XML: ACTM")
        self.ACTMExportButton.toolTip = "Export XML for Cumulated Activity Mode."
        self.ACTMExportButton.enabled = True
        exportHContainer.addWidget(self.ACTMExportButton)

        self.DOSEExportButton = qt.QPushButton("Export XML: ADRM")
        self.DOSEExportButton.toolTip = "Export XML for Absorbed Dose Rate Mode."
        self.DOSEExportButton.enabled = True
        exportHContainer.addWidget(self.DOSEExportButton)

        TimeIntegrationFormLayout.addRow(exportHContainer)

        # connections

        self.IntegrateActivityButton.clicked.connect(
            self.onIntegrateActivityButton)
        self.IntegrateDoseButton.clicked.connect(self.onIntegrateDoseButton)
        self.ACTMExportButton.clicked.connect(self.onACTMExportButton)
        self.DOSEExportButton.clicked.connect(self.onDOSEExportButton)

        #
        # End Time Integration Area
        #

        #
        # Start Utilities Area
        #
        ''' This section contains two functionalities:
              1. Clean Scene: check naming convention and erase everything that does not follow it, also erases all additional data for reprocessing
        '''

        UtilitiesCollapsibleButton = ctk.ctkCollapsibleButton()
        UtilitiesCollapsibleButton.text = "Utilities"
        UtilitiesCollapsibleButton.collapsed = True
        self.layout.addWidget(UtilitiesCollapsibleButton)
        UtilitiesFormLayout = qt.QFormLayout(UtilitiesCollapsibleButton)
        UtilitiesHContainer = qt.QHBoxLayout()

        # Clean up
        self.CleanUpButton = qt.QPushButton("Clean Scene")
        self.CleanUpButton.toolTip = "Cleans Scene from all additional or intermediate results."
        self.CleanUpButton.enabled = True
        UtilitiesHContainer.addWidget(self.CleanUpButton)

        UtilitiesFormLayout.addRow(UtilitiesHContainer)

        # connections
        self.CleanUpButton.clicked.connect(self.onCleanUpButton)

        #
        # End Utilities Area
        #

        #
        # Start Options Area
        #

        OptionsCollapsibleButton = ctk.ctkCollapsibleButton()
        OptionsCollapsibleButton.text = "Options"
        OptionsCollapsibleButton.collapsed = False
        self.layout.addWidget(OptionsCollapsibleButton)
        OptionsFormLayout = qt.QFormLayout(OptionsCollapsibleButton)
        OptionsHContainer = qt.QHBoxLayout()

        self.LegendButton = qt.QCheckBox("Show Legend in graphs")
        self.ShowLegend = False
        self.LegendButton.setChecked(self.ShowLegend)
        self.LnyaxisButton = qt.QCheckBox("ln y axis")
        self.Lnyaxis = False
        self.LnyaxisButton.setChecked(self.Lnyaxis)
        self.MTButton = qt.QCheckBox("Use multithreads")
        self.multithreading = True
        self.MTButton.setChecked(self.multithreading)

        OptionsHContainer.addWidget(self.LegendButton)
        OptionsHContainer.addWidget(self.LnyaxisButton)
        OptionsHContainer.addWidget(self.MTButton)
        OptionsFormLayout.addRow(OptionsHContainer)

        self.loggingLevels = {
            'Critical': logging.CRITICAL,
            'Error': logging.ERROR,
            'Warning': logging.WARNING,
            'Info': logging.INFO,
            'Debug': logging.DEBUG,
            'All': logging.NOTSET
        }
        self.loggingButton = qt.QComboBox()
        self.loggingButton.addItems(list(self.loggingLevels.keys()))
        self.loggingButton.setCurrentIndex(2)
        OptionsFormLayout.addRow('Set logging level', self.loggingButton)

        self.LegendButton.stateChanged.connect(self.onLegendButton)
        self.LnyaxisButton.stateChanged.connect(self.onLnyaxisButton)
        self.MTButton.stateChanged.connect(self.onMTButton)
        self.loggingButton.currentIndexChanged.connect(self.onloggingButton)

        #
        # End Options Area
        #

        # Add vertical spacer
        self.layout.addStretch(1)

        # Initial setup of the isotope
        isotopeText=self.isotopsList.currentText
        self.logic = Dosimetry4DLogic(parent=self, isotopeText=isotopeText)

        # Refresh Apply button state
        self.onSelect()

    def onReload(self):
        """
        Override reload scripted module widget representation.
        """
        self.logger.warning("Reloading Dosimetry4D")
        importlib.reload(mod)
        for submoduleName in __submoduleNames__:
            mod1 = importlib.import_module(
                '.'.join(['Logic', submoduleName]), __name__)
            importlib.reload(mod1)

        if isinstance(self, ScriptedLoadableModuleWidget):
            ScriptedLoadableModuleWidget.onReload(self)

    def onMTButton(self):
        self.multithreading = self.MTButton.isChecked()

    def onLegendButton(self):
        self.ShowLegend = self.LegendButton.isChecked()
        graphnodes = slicer.util.getNodesByClass('vtkMRMLPlotChartNode')

        for graphnode in graphnodes:
            graphnode.SetLegendVisibility(self.ShowLegend)

    def onLnyaxisButton(self):
        if self.logic:
            self.logic.logyaxis = self.LnyaxisButton.isChecked()
        graphnodes = slicer.util.getNodesByClass('vtkMRMLPlotChartNode')

        for graphnode in graphnodes:
            graphnode.SetYAxisLogScale(self.LnyaxisButton.isChecked())

    def onInstallModuleButton(self, module="SlicerElastix"):
        utils.installModule(module)

    def onloggingButton(self):
        levelText = self.loggingButton.currentText
        levelValue = self.loggingLevels[levelText]
        self.logger.setLevel(levelValue)

    def onSelect(self):
        self.propagateButton.enabled = vtkmrmlutils.hasSegmentation(
            self.segmentation.currentNode())
        self.ACTMTablesButton.enabled = vtkmrmlutils.hasSegmentation(
            self.segmentation.currentNode())
        self.DOSETablesButton.enabled = vtkmrmlutils.hasSegmentation(
            self.segmentation.currentNode())

    def switchModule(self, moduleName):
        # Switch to another module
        pluginHandlerSingleton = slicer.qSlicerSubjectHierarchyPluginHandler.instance()
        pluginHandlerSingleton.pluginByName(
            'Default').switchToModule(moduleName)

    # Definition of connections events

    def onCleanUpButton(self):
        self.logic.clean()
        self.rescaleButton.enabled = False
        self.resampleButton.enabled = False
        self.registrationButton.enabled = False
        self.initialPoint.enabled = False
        self.doseRateButton.enabled = False
        self.activityThresholdSliderWidget.enabled = False
        self.segmentation.enabled = False
        self.SegmentationPropagated = False
        self.propagateButton.enabled = False
        self.DoseCalculated = False
        self.ACTMTablesButton.enabled = False
        self.DOSETablesButton.enabled = False

    def onRenameButton(self):
        self.logic.standardise(self.InjectionActivity.text)
        if self.InjectionActivity.text != self.logic.injectedActivity:
            self.InjectionActivity.setText(str(self.logic.injectedActivity))
            self.InjectionActivity.setToolTip(
                f"Injected activity {self.logic.injectedActivity} extracted from DICOMheader")
        self.saveOptions()
        self.resampleButton.enabled = True
        self.initialPoint.enabled = True
        self.activityThresholdSliderWidget.enabled = True
        self.doseRateButton.enabled = True

    def onResampleButton(self):
        self.logic.resampleCT()
        self.rescaleButton.enabled = True

    def onRescaleButton(self):
        self.saveOptions()

        try:
            sensitivity = self.Evaluator.eval(self.spectSensitivity.text)
        except Exception as e:
            self.logger.error(f'Sensitivity not valid, please check\n{e}')

        try:
            calibrationTime = self.Evaluator.eval(
                self.spectCalibrationTime.text)
        except Exception as e:
            self.logger.error(f'Calibration Time not valid, please check\n{e}')

        self.calibration['SPECTSensitivity'] = {
            'Value': float(sensitivity),
            'Units': self.sensitivityUnits.currentText,
            'Time': int(calibrationTime)
        }

        self.logic.scaleValues(
            calibration=self.calibration,
            injectedActivity=self.InjectionActivity.text
        )
        self.initialPoint.enabled = True

    def onRegistrationButton(self):
        referenceNode = self.initialPoint.currentNode()
        self.logic.createTransforms(referenceNode)
        self.doseRateButton.enabled = True
        self.activityThresholdSliderWidget.enabled = True
        self.segmentation.enabled = True
        self.propagateButton.enabled = self.segmentation.currentNode(
        ) and self.segmentation.enabled

    def onDoseRateAlgorithm(self):
        algorithm = self.doseRateAlgorithm.currentText
        if "convolution" in algorithm.lower():
            self.kernelDistanceLimitContainer.show()
        else:
            self.kernelDistanceLimitContainer.hide()
        if "Monte Carlo" in algorithm:
            self.MonteCarloContainer.show()
            self.doseRateButton.hide()
        else:
            self.MonteCarloContainer.hide()
            self.doseRateButton.show()

    def setCalibrationTimeVisibility(self):
        if 'Bqs' in self.sensitivityUnits.currentText:
            self.calibrationTimeContainer.hide()
        else:
            self.calibrationTimeContainer.show()

    def onCalibrationDateChanged(self):
        center = self.ClinicalCenter.currentText
        isotopeText = self.isotopsList.currentText
        calibrationDate = self.calibrationDate.currentText
        if center in self.sensitivity and isotopeText in self.sensitivity[center] and calibrationDate in self.sensitivity[center][isotopeText]:
            lsensitivity = self.sensitivity[center][isotopeText][calibrationDate]
            self.spectSensitivity.setText(lsensitivity["Sensitivity"])
            index = self.sensitivityUnits.findText(
                lsensitivity['Units'], qt.Qt.MatchFixedString)
            if index > 0:
                self.sensitivityUnits.setCurrentIndex(index)
            self.spectCalibrationTime.setText(lsensitivity['Time'])

    def onIsotopeChange(self):
        self.logger.info(
            f'Loading data for isotope {self.isotopsList.currentText}')
        center = self.ClinicalCenter.currentText
        isotopeText = self.isotopsList.currentText
        self.logic.Isotope = dbutils.setupIsotope(isotopeText)
        if center in self.sensitivity and isotopeText in self.sensitivity[center]:
            lsensitivity = self.sensitivity[center][isotopeText]
            dates = list(lsensitivity.keys())
            self.calibrationDate.clear()
            self.calibrationDate.addItems(dates)

    def onInitialPointChange(self):
        lnode = self.initialPoint.currentNode()
        if not lnode:  # The node must has been selected
            self.registrationButton.enabled = False
            return
        if lnode.GetImageData() is None:  # The selected node must contain an image
            self.registrationButton.enabled = False
            return
        lName = lnode.GetName()
        # This node must be a CT
        if 'CTCT' in lName:
            self.registrationButton.enabled = True
            self.logger.info(f"Selected {lName} as reference volume")
        else:
            self.registrationButton.enabled = False
            self.logger.error(f"Selected volume {lName} is not a CT node")

    def onDoseRateButton(self):
        try:
            # initialize
            self.doseRateButton.enabled = False
            mode = self.doseRateAlgorithm.currentText
            # select method
            if 'LED' in mode:
                self.logic.LED(
                    threshold=self.activityThresholdSliderWidget.value / 100)
            elif 'convolution' in mode.lower():
                self.logic.convolution(
                    isotopeText=self.isotopsList.currentText,
                    DVK_density=1.00,
                    threshold=self.activityThresholdSliderWidget.value/100,
                    kernellimit=self.kernelDistanceLimit.value,
                    mode=mode,
                    multithreaded=self.multithreading
                )
            elif 'monte' in mode.lower():
                self.logic.importMonteCarlo()
            self.DoseCalculated = True
            self.DOSETablesButton.enabled = vtkmrmlutils.hasSegmentation(
                self.segmentation.currentNode()) or self.SegmentationPropagated
        finally:
            self.doseRateButton.enabled = True

    def onMonteCarloButton(self):
        pass  # TODO: implement

    def onGenerateGate(self):
        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            "Choose Directory",
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
        )
        if directory:
            self.usePath = Path(directory)
            self.Gate = gate.Gate(
                directory=self.usePath,
                calibration=self.calibration,
                isotope=self.isotopsList.currentText
            )
            self.Gate.generate()


    def onPropagateButton(self):
        self.logic.Propagate(self.segmentation.currentNode())
        self.ACTMTablesButton.enabled = vtkmrmlutils.hasSegmentation(
            self.segmentation.currentNode())

        self.SegmentationPropagated = True
        self.DOSETablesButton.enabled = self.DoseCalculated

    def onACTMTablesButton(self):
        self.logic.computeNewValues(mode='Activity')
        self.ACTMPlotsButton.enabled = True

    def onDOSETablesButton(self):
        self.logic.computeNewValues(
            mode='DoseRate',
            algorithm=self.doseRateAlgorithm.currentText
        )
        self.DOSEPlotsButton.enabled = True

    def onACTMPlotsButton(self):
        self.logic.processTimeIntegrationVariables(mode='Activity')
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode='Activity', showlegend=self.ShowLegend)
        self.IntegrateActivityButton.enabled = True

    def onDOSEPlotsButton(self):
        self.logic.processTimeIntegrationVariables(
            mode='DoseRate',
            algorithm=self.doseRateAlgorithm.currentText
        )
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode='DoseRate',
            algorithm=self.doseRateAlgorithm.currentText,
            showlegend=self.ShowLegend
        )
        self.IntegrateDoseButton.enabled = True

    def onIntegrateActivityButton(self):
        self.logic.integrate(
            mode='Activity',
            algorithm='',
            incorporationmode=self.incorporationMode.currentText,
            integrationmode=self.integrationMode.currentText
        )
        self.ACTMExportButton.enabled = True

    def onIntegrateDoseButton(self):
        self.logic.integrate(
            mode='DoseRate',
            algorithm=self.doseRateAlgorithm.currentText,
            incorporationmode=self.incorporationMode.currentText,
            integrationmode=self.integrationMode.currentText
        )
        self.DOSEExportButton.enabled = True

    def onACTMExportButton(self):
        # TODO: Define input for Clinical Study Title and ID
        referenceFolder = self.logic.getReferenceFolder()

        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            "Choose Directory",
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
        )
        if directory:
            self.usePath = Path(directory)
            XMLString = xmlexport.XML_MEDIRAD(
                referenceFolder,
                self.usePath,
                'Dosimetry calculation - I-131 ablation of thyroid',
                '755523-t33'
            )
            XMLString.fillACTM_XMLfile()
            self.saveOptions()

    def onDOSEExportButton(self):
        # TODO: Define input for Clinical Study Title and ID
        referenceFolder = self.logic.getReferenceFolder()

        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            "Choose Directory",
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
        )
        if directory:
            self.usePath = Path(directory)
            XMLString = xmlexport.XML_MEDIRAD(
                referenceFolder,
                self.usePath,
                'Dosimetry calculation - I-131 ablation of thyroid',
                '755523-t33'
            )
            XMLString.fillADRM_XMLfile(
                mode='DoseRate',
                algorithm=self.doseRateAlgorithm.currentText
            )
            self.saveOptions()
