from datetime import datetime
from pathlib import Path
import importlib
import numpy as np
import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModule, ScriptedLoadableModuleWidget, ScriptedLoadableModuleLogic
import ctk
import vtk
import qt

from Logic import utils, sentry, logging, vtkmrmlutils, errors, dbutils, config, xmlexport
from Logic.nodes import Node
from Logic.PhysicalUnits import PhysicalUnits
from Logic.attributes import Attributes

__submoduleNames__ = ['utils', 'sentry', 'logging', 'nodes',
                      'vtkmrmlutils', 'constants', 'errors', 'dbutils', 'config']
__package__ = 'Calibration'
mod = importlib.import_module('Logic', __name__)
importlib.reload(mod)

#######################
#                     #
# Calibration         #
#                     #
#######################


class Calibration(ScriptedLoadableModule):

    def __init__(self, parent):
        super().__init__(parent)
        self.script_path = utils.getScriptPath()
        # TODO make this more human readable by adding spaces
        self.parent.title = "OpenDose Calibration"
        self.parent.categories = ["Nuclear Medicine"]
        self.parent.dependencies = []
        # replace with "Firstname Lastname (Organization)"
        self.parent.contributors = [
            "Alex Vergara Gil (INSERM, France)"]
        self.parent.helpText = '''
            This module implements the Calibration steps for molecular radiotherapy.\n
        '''
        self.parent.helpText += self.getDefaultModuleDocumentationLink()
        self.parent.acknowledgementText = '''
            This software was originally developed by Alex Vergara Gil (INSERM, France)
        '''  # replace with organization, grant and thanks.

        sentry.init()

#######################
#                     #
# CalibrationWidget   #
#                     #
#######################


class CalibrationWidget(ScriptedLoadableModuleWidget):

    def setup(self):
        ''' Creation of the Module Widgets
            This procedure set up all widgets and define all conections
            TODO: Add CT calibration curve
        '''
        self.script_path = utils.getScriptPath()
        self.usePath = self.script_path

        # self.developerMode = False # Comment to show developer mode
        super().setup()
        self.logger = logging.getLogger('Calibration')
        self.logger.setLevel(logging.WARNING)

        self.logic = CalibrationLogic(parent=self)

        ParametersCollapsibleButton = ctk.ctkCollapsibleButton()
        ParametersCollapsibleButton.text = "Parameters"
        self.layout.addWidget(ParametersCollapsibleButton)
        ParametersFormLayout = qt.QFormLayout(ParametersCollapsibleButton)
        ParametersHContainer = qt.QHBoxLayout()

        self.ClinicalCenter = qt.QComboBox()
        clinicalcenters = config.readClinicalCenters().keys()
        self.ClinicalCenter.addItems(list(clinicalcenters))
        ParametersHContainer.addWidget(self.ClinicalCenter)

        self.newClinicalCenterButton = qt.QPushButton(
            "Create new clinical center")
        self.newClinicalCenterButton.toolTip = "Creates a new clinical center in the database."
        self.newClinicalCenterButton.enabled = True
        ParametersHContainer.addWidget(self.newClinicalCenterButton)

        self.newClinicalCenterButton.clicked.connect(
            self.onNewClinicalCenterButton)

        ParametersFormLayout.addRow(
            'Select Clinical Center', ParametersHContainer)

        PreprocessingCollapsibleButton = ctk.ctkCollapsibleButton()
        PreprocessingCollapsibleButton.text = "Preprocessing"
        self.layout.addWidget(PreprocessingCollapsibleButton)
        PreprocessingFormLayout = qt.QFormLayout(
            PreprocessingCollapsibleButton)
        PreprocessingHContainer = qt.QHBoxLayout()

        # rename files
        self.renameButton = qt.QPushButton("Standardize files")
        self.renameButton.toolTip = "Rename original DICOM files and standardize them (Add required attributes)."
        self.renameButton.enabled = True
        PreprocessingHContainer.addWidget(self.renameButton)
        self.renameButton.clicked.connect(self.onrenameButton)

        PreprocessingFormLayout.addRow(PreprocessingHContainer)

        SPECTCalibrationCollapsibleButton = ctk.ctkCollapsibleButton()
        SPECTCalibrationCollapsibleButton.text = "SPECT Sensitivity Calibration"
        self.layout.addWidget(SPECTCalibrationCollapsibleButton)
        SPECTCalibrationFormLayout = qt.QFormLayout(
            SPECTCalibrationCollapsibleButton)

        # input volume selector
        self.SPECTSensitivityTank = slicer.qMRMLNodeComboBox()
        self.SPECTSensitivityTank.nodeTypes = ["vtkMRMLScalarVolumeNode"]
        self.SPECTSensitivityTank.selectNodeUponCreation = True
        self.SPECTSensitivityTank.addEnabled = False
        self.SPECTSensitivityTank.removeEnabled = False
        self.SPECTSensitivityTank.noneEnabled = False
        self.SPECTSensitivityTank.showHidden = False
        self.SPECTSensitivityTank.showChildNodeTypes = False
        self.SPECTSensitivityTank.setMRMLScene(slicer.mrmlScene)
        self.SPECTSensitivityTank.setToolTip(
            "Select the reference Volume (phantom CT).")
        self.SPECTSensitivityTank.enabled = True
        SPECTCalibrationFormLayout.addRow(
            "SPECT Sensitivity Volume: ", self.SPECTSensitivityTank)

        self.SPECTSensitivityTank.currentNodeChanged.connect(
            self.onSPECTSensitivityTankChange)

        self.SPECTInjectedActivity = qt.QLineEdit()
        self.SPECTInjectedActivity.setText("50")
        self.SPECTInjectedActivity.setAlignment(qt.Qt.AlignRight)
        SPECTCalibrationFormLayout.addRow(
            'Enter syringe Activity (MBq)', self.SPECTInjectedActivity)

        self.SPECTResidualActivity = qt.QLineEdit()
        self.SPECTResidualActivity.setText("0")
        self.SPECTResidualActivity.setAlignment(qt.Qt.AlignRight)
        SPECTCalibrationFormLayout.addRow(
            'Enter residual Activity (MBq)', self.SPECTResidualActivity)

        self.SPECTMeasurementDate = qt.QLineEdit()
        self.SPECTMeasurementDate.setText(utils.getCurrentTime())
        self.SPECTMeasurementDate.setAlignment(qt.Qt.AlignRight)
        SPECTCalibrationFormLayout.addRow(
            'Enter measurement date/time (YYYY-MM-DD HH:mm:ss)', self.SPECTMeasurementDate)

        self.SPECTMeasurementDate.textChanged.connect(self.onDateChanged)

        self.SPECTTankVolume = qt.QLineEdit()
        self.SPECTTankVolume.setText("10000")
        self.SPECTTankVolume.setAlignment(qt.Qt.AlignRight)
        SPECTCalibrationFormLayout.addRow(
            'Enter tank Volume (cm^3)', self.SPECTTankVolume)

        self.SPECTIsotope = qt.QLineEdit()
        self.SPECTIsotope.setText("Lu-177")
        self.SPECTIsotope.setAlignment(qt.Qt.AlignRight)
        SPECTCalibrationFormLayout.addRow(
            'Enter isotope (nn-AAA)', self.SPECTIsotope)

        self.SPECTWaitDuration = qt.QLineEdit()
        self.SPECTWaitDuration.setText('0')
        self.SPECTWaitDuration.setAlignment(qt.Qt.AlignRight)
        SPECTCalibrationFormLayout.addRow(
            'Enter wait duration (ss)', self.SPECTWaitDuration)

        self.SPECTAcquisitionDate = qt.QLineEdit()
        self.SPECTAcquisitionDate.setText(utils.getCurrentTime())
        self.SPECTAcquisitionDate.setAlignment(qt.Qt.AlignRight)
        SPECTCalibrationFormLayout.addRow(
            'Enter acquisition date/time (YYYY-MM-DD HH:mm:ss)', self.SPECTAcquisitionDate)

        self.SPECTAcquisitionDate.textChanged.connect(self.onDateChanged)

        self.SPECTAcquisitionDuration = qt.QLineEdit()
        self.SPECTAcquisitionDuration.setText('0')
        self.SPECTAcquisitionDuration.setAlignment(qt.Qt.AlignRight)
        SPECTCalibrationFormLayout.addRow(
            'Enter acquisition duration (ss)', self.SPECTAcquisitionDuration)

        SPECTButtonsHContainer = qt.QHBoxLayout()
        # SPECT Button
        self.SPECTCalibrationButton = qt.QPushButton(
            "Calibrate SPECT (Sensitivity)")
        self.SPECTCalibrationButton.toolTip = "Automatically gets calibration data from current SPECT acquisition."
        self.SPECTCalibrationButton.enabled = True
        SPECTButtonsHContainer.addWidget(self.SPECTCalibrationButton)

        # XML Export Button
        self.SPECTCalibrationXMLButton = qt.QPushButton(
            "Export Calibration XML (Sensitivity)")
        self.SPECTCalibrationXMLButton.toolTip = "Exports XML for sensitivity calibration."
        self.SPECTCalibrationXMLButton.enabled = True
        SPECTButtonsHContainer.addWidget(self.SPECTCalibrationXMLButton)

        SPECTCalibrationFormLayout.addRow(SPECTButtonsHContainer)

        self.SPECTCalibrationButton.clicked.connect(
            self.onSPECTCalibrationButton)
        self.SPECTCalibrationXMLButton.clicked.connect(self.onSPECTCalibrationXMLButton)

        # Add vertical spacer
        self.layout.addStretch(1)

        self.onrenameButton()  # if it is the only one volume

    def onNewClinicalCenterButton(self):
        Dialog = qt.QDialog()
        formLayout = qt.QFormLayout()
        CenterCode = qt.QLineEdit()
        CenterCode.setAlignment(qt.Qt.AlignRight)
        formLayout.addRow('Enter new center code', CenterCode)
        CenterDescription = qt.QLineEdit()
        CenterDescription.setAlignment(qt.Qt.AlignRight)
        formLayout.addRow('Enter new center Description', CenterDescription)
        Proceed = qt.QPushButton("Create new center")
        Proceed.toolTip = "Saves new center to database."
        Proceed.enabled = True
        hbox = qt.QHBoxLayout()
        hbox.addWidget(Proceed)
        Cancel = qt.QPushButton("Cancel")
        Cancel.toolTip = "Cancels and return to Slicer."
        Cancel.enabled = True
        hbox.addWidget(Cancel)
        formLayout.addRow(hbox)
        Proceed.clicked.connect(lambda: Dialog.done(1))
        Cancel.clicked.connect(lambda: Dialog.reject())
        Dialog.setLayout(formLayout)
        Dialog.setWindowTitle(
            'Please enter code and description of new center')
        if Dialog.exec_() == qt.QDialog.Accepted:
            code = CenterCode.text
            description = CenterDescription.text
            self.addnewCenter(code, description)
            print('Saved')
        else:
            print('Cancelled')

    def addnewCenter(self, code, description):
        centers = config.readClinicalCenters()
        centers[code] = description
        print(centers)
        config.writeClinicalCenters(centers)
        self.ClinicalCenter.clear()
        self.ClinicalCenter.addItems(list(centers.keys()))
        index = self.ClinicalCenter.findText(code, qt.Qt.MatchFixedString)
        if index > 0:
            self.ClinicalCenter.setCurrentIndex(index)

    def onrenameButton(self):
        nodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLVolumeNode')
        for node in nodes:
            _ = Node.new(vtkmrmlutils.getItemID(node))
        self.onSPECTSensitivityTankChange()

    def onInitialPointChange(self):
        lnodeID = vtkmrmlutils.getItemID(self.initialPoint.currentNode())
        lnode = Node.new(lnodeID)
        if not lnode:  # The node must has been selected
            self.CTCalibrationButton.enabled = False
            return
        if lnode.data.GetImageData() is None:  # The selected node must contain an image
            self.CTCalibrationButton.enabled = False
            return
        modality = lnode.getModality()
        # This node must be a CT
        if modality in ['CT']:
            self.CTCalibrationButton.enabled = True
            self.logger.info(f"Selected {lnode.name} as reference volume")
        else:
            self.CTCalibrationButton.enabled = False
            self.logger.error(f"Selected volume {lnode.name} is not a CT node")

    def onSPECTSensitivityTankChange(self):
        lnodeID = vtkmrmlutils.getItemID(
            self.SPECTSensitivityTank.currentNode())
        if lnodeID <= 0 or self.SPECTSensitivityTank.currentNode() is None:
            return  # No studies selected
        lnode = Node.new(lnodeID)
        modality = lnode.getModality()
        if not lnode:  # The node must has been selected
            self.SPECTCalibrationButton.enabled = False
            return
        if lnode.data.GetImageData() is None:  # The selected node must contain an image
            self.SPECTCalibrationButton.enabled = False
            return

        try:
            print(lnode.getInjectionDateTime())
            injTime = datetime.strptime(
                lnode.getInjectionDateTime(), '%Y-%m-%d %H:%M:%S')
        except:
            raise IOError(lnode.getInjectionDateTime())

        try:
            acqTime = datetime.strptime(
                lnode.getAcquisition(), '%Y-%m-%d %H:%M:%S')
        except:
            acqTime = datetime.now()

        # This node must be a SPECT or PET
        if modality in ['PT', 'NM']:
            self.SPECTCalibrationButton.enabled = True
            self.logger.info(f"Selected {lnode.name} as reference volume")
            self.SPECTInjectedActivity.setText(lnode.getInjectedActivity())
            self.SPECTMeasurementDate.setText(
                injTime.strftime('%Y-%m-%d %H:%M:%S'))
            isotope = lnode.getRadiopharmaceutical()
            self.SPECTIsotope.setText(isotope)
            self.SPECTAcquisitionDate.setText(acqTime)
            self.SPECTAcquisitionDuration.setText(
                lnode.getAcquisitionDuration())
            tankVolume = vtkmrmlutils.getItemDataNodeAttributeValue(
                lnode.nodeID, "Tank Volume (cm^3)")
            if tankVolume:
                self.SPECTTankVolume.setText(tankVolume)
        else:
            self.SPECTCalibrationButton.enabled = False
            self.logger.error(
                f"Selected volume {lnode.name} is not a SPECT node")

    def onSPECTAttributesButton(self):
        SyrActivity = self.SPECTInjectedActivity.text
        resActivity = self.SPECTResidualActivity.text
        MeasurementDate = self.SPECTMeasurementDate.text
        AcquisitionDate = self.SPECTAcquisitionDate.text
        tankVolume = self.SPECTTankVolume.text
        Isotope = self.SPECTIsotope.text
        acquisitionDuration = self.SPECTAcquisitionDuration.text
        center = self.ClinicalCenter.currentText

        lnodeID = vtkmrmlutils.getItemID(
            self.SPECTSensitivityTank.currentNode())
        lnode = Node.new(lnodeID)
        vtkmrmlutils.setItemDataNodeAttribute(
            lnode.nodeID, Attributes().acquisition, AcquisitionDate)
        vtkmrmlutils.setItemDataNodeAttribute(
            lnode.nodeID, Attributes().injectionTime, MeasurementDate)
        vtkmrmlutils.setItemDataNodeAttribute(lnode.nodeID, Attributes(
        ).injectedActivity, str(float(SyrActivity)-float(resActivity)))
        vtkmrmlutils.setItemDataNodeAttribute(
            lnode.nodeID, Attributes().radiopharmaceutical, Isotope)
        vtkmrmlutils.setItemDataNodeAttribute(
            lnode.nodeID, Attributes().acquisitionDuration, acquisitionDuration)
        vtkmrmlutils.setItemDataNodeAttribute(
            lnode.nodeID, "Tank Volume (cm^3)", tankVolume)
        vtkmrmlutils.setItemDataNodeAttribute(
            lnode.nodeID, "Clinical Center", center)

        try:
            injTime = datetime.strptime(MeasurementDate, '%Y-%m-%d %H:%M:%S')
        except:
            injTime = datetime.now()

        try:
            acqTime = datetime.strptime(AcquisitionDate, '%Y-%m-%d %H:%M:%S')
        except:
            acqTime = datetime.now()

        restTime = round((acqTime - injTime).total_seconds() /
                         PhysicalUnits().getUnit('Time', 's'))
        vtkmrmlutils.setItemDataNodeAttribute(
            lnode.nodeID, "Wait Time (equilibrium)", restTime)
        return lnode

    def onDateChanged(self):
        try:
            injTime = datetime.strptime(
                self.SPECTMeasurementDate.text, '%Y-%m-%d %H:%M:%S')
        except:
            return

        try:
            acqTime = datetime.strptime(
                self.SPECTAcquisitionDate.text, '%Y-%m-%d %H:%M:%S')
        except:
            return

        restTime = round((acqTime - injTime).total_seconds() /
                         PhysicalUnits().getUnit('Time', 's'))
        self.SPECTWaitDuration.setText(str(restTime))

    def onSPECTCalibrationButton(self):
        lnode = self.onSPECTAttributesButton()
        self.logic.runSPECTSensitivity(lnode)

    def onSPECTCalibrationXMLButton(self):
        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            "Choose Directory",
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
        )
        lnodeID = vtkmrmlutils.getItemID(
            self.SPECTSensitivityTank.currentNode())
        lnode = Node.new(lnodeID)
        if directory:
            self.usePath = Path(directory)
            XMLString = xmlexport.XML_MEDIRAD(
                directory,
                self.options['usePath'],
                'Dosimetry calculation - I-131 ablation of thyroid',
                '755523-t33'
            )
            XMLString.fillSensitivity_XMLfile(lnode)
            self.saveOptions()

    def onReload(self):
        """
        Override reload scripted module widget representation.
        """
        #self.logger.warning("Reloading Dosimetry4D")
        importlib.reload(mod)
        for submoduleName in __submoduleNames__:
            mod1 = importlib.import_module(
                '.'.join(['Logic', submoduleName]), __name__)
            importlib.reload(mod1)

        if isinstance(self, ScriptedLoadableModuleWidget):
            ScriptedLoadableModuleWidget.onReload(self)


class CalibrationLogic(ScriptedLoadableModuleLogic):

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.logger = logging.getLogger('Calibration.Logic')
        self.CTCalibration = {}
        self.SPECTSensitivity = {}
        self.SPECTRecovery = {}
        self.units = {}
        self.units['cm^3'] = (PhysicalUnits().getUnit('Longitude', 'cm'))**3
        self.units['MBq'] = PhysicalUnits().getUnit('Activity', 'MBq')
        self.units['hour'] = PhysicalUnits().getUnit('Time', 'h')
        self.units['sec'] = PhysicalUnits().getUnit('Time', 's')

    def runSPECTSensitivity(self, lnode):
        nodeSensitivity = lnode.data
        modality = lnode.getModality()
        rf = lnode.getRadiopharmaceutical()
        Isotope = dbutils.setupIsotope(rf)
        isotopeText = Isotope['name']
        activity = float(lnode.getInjectedActivity()) * self.units['MBq']
        center = vtkmrmlutils.getItemDataNodeAttributeValue(
            lnode.nodeID, "Clinical Center")
        tankVolume = float(vtkmrmlutils.getItemDataNodeAttributeValue(
            lnode.nodeID, "Tank Volume (cm^3)")) * self.units['cm^3']
        restTime = float(vtkmrmlutils.getItemDataNodeAttributeValue(
            lnode.nodeID, "Wait Time (equilibrium)")) * self.units['sec']
        ln2 = np.log(2)

        if not modality in ['NM', 'PT']:
            raise errors.IOError('Wrong node, must be NM or PT')

        if float(activity) <= 0:
            raise errors.IOError('Activity can''t be zero')

        if tankVolume <= 0:
            raise errors.IOError('Tank volume can''t be zero')

        ActivityConcentration = activity / tankVolume  # MBq/cm^3
        duration = float(nodeSensitivity.GetAttribute(
            Attributes().acquisitionDuration)) * self.units['sec']
        T_h = Isotope['T_h'] * self.units['hour']

        # Correction for decay during rest Time
        decayRestCorrection = np.exp(-ln2 * float(restTime) / T_h)
        dac = ln2 * duration / T_h
        # Correction for decay during acquisition
        decayAcquisitionCorrection = (1-np.exp(-dac))/dac
        print(decayRestCorrection, decayAcquisitionCorrection)
        ActivityConcentrationCorrected = ActivityConcentration * \
            decayRestCorrection * decayAcquisitionCorrection

        arrayValues = lnode.getArrayData()
        maxValue = max(arrayValues.ravel())
        minValue = 0.1 * maxValue
        spacing = lnode.data.GetSpacing()
        voxelsize = np.mean(spacing)

        while True:
            segmentationNode = slicer.mrmlScene.GetFirstNodeByClass(
                "vtkMRMLSegmentationNode")
            if segmentationNode is None:
                break
            slicer.mrmlScene.RemoveNode(segmentationNode)

        # Create segmentation
        segmentationNode = slicer.mrmlScene.AddNewNodeByClass(
            "vtkMRMLSegmentationNode")
        segmentationNode.CreateDefaultDisplayNodes()  # only needed for display
        segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(
            nodeSensitivity)

        # Create temporary segment editor to get access to effects
        segmentEditorWidget = slicer.qMRMLSegmentEditorWidget()
        segmentEditorWidget.setMRMLScene(slicer.mrmlScene)
        segmentEditorNode = slicer.mrmlScene.AddNewNodeByClass(
            "vtkMRMLSegmentEditorNode")
        segmentEditorWidget.setMRMLSegmentEditorNode(segmentEditorNode)
        segmentEditorWidget.setSegmentationNode(segmentationNode)
        segmentEditorWidget.setMasterVolumeNode(nodeSensitivity)

        # Create segments by thresholding
        segmentsFromHounsfieldUnits = [
            ["Water", minValue, maxValue]]
        for segmentName, thresholdMin, thresholdMax in segmentsFromHounsfieldUnits:
            # Create segment
            addedSegmentID = segmentationNode.GetSegmentation().AddEmptySegment(segmentName)
            segmentEditorNode.SetSelectedSegmentID(addedSegmentID)
            # Fill by thresholding
            segmentEditorWidget.setActiveEffectByName("Threshold")
            effect = segmentEditorWidget.activeEffect()
            effect.setParameter("MinimumThreshold", str(thresholdMin))
            effect.setParameter("MaximumThreshold", str(thresholdMax))
            effect.self().onApply()
            # Remove noise
            segmentEditorWidget.setActiveEffectByName("Islands")
            effect = segmentEditorWidget.activeEffect()
            effect.setParameter('Operation', 'KEEP_LARGEST_ISLAND')
            effect.self().onApply()
            # Smooth
            segmentEditorWidget.setActiveEffectByName("Smoothing")
            effect = segmentEditorWidget.activeEffect()
            effect.setParameter("SmoothingMethod", "OPENING")
            effect.setParameter("KernelSizeMm", voxelsize * 3)
            effect.self().onApply()
            # Reduce to avoid borders
            segmentEditorWidget.setActiveEffectByName("Margin")
            effect = segmentEditorWidget.activeEffect()
            # Increase first to smooth further
            effect.setParameter("MarginSizeMm", voxelsize * 12)
            effect.self().onApply()
            effect.setParameter(
                "MarginSizeMm", -voxelsize * 24)  # Decrease now
            effect.self().onApply()

        # Delete temporary segment editor
        segmentEditorWidget = None
        slicer.mrmlScene.RemoveNode(segmentEditorNode)

        # Make segmentation results visible in 3D
        segmentationNode.CreateClosedSurfaceRepresentation()

        # Compute segment volumes
        name = f'T:TABL SPECT Sensitivity Helper'
        TableNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLTableNode')
        for node in TableNodes:
            if name == node.GetName():  # Table exists, erase it
                slicer.mrmlScene.RemoveNode(node)
        helperTableNode = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLTableNode')
        helperTableNode.SetName(name)
        vtkmrmlutils.helperSegmentStatisticsTable(
            segmentationNode, nodeSensitivity, helperTableNode)
        segmentationNode.GetDisplayNode().SetVisibility(1)
        # vtkmrmlutils.showTable(helperTableNode)

        # # Extract data from the table
        TableArray = vtkmrmlutils.GetTableAsArray(helperTableNode)
        SegmentName = TableArray['Segment'][0]
        segVolume = TableArray['Volume [cm3] (1)'][0] * self.units['cm^3']
        MeanCounts = TableArray['Mean'][0] * \
            TableArray['Number of voxels [voxels]'][0] / segVolume
        Sensitivity = MeanCounts / duration / ActivityConcentrationCorrected

        # # Process ResultTable (create new table to show)
        name = f'T:TABL SPECT Sensitivity {nodeSensitivity.GetAttribute(Attributes().acquisition)}'
        TableNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLTableNode')
        for node in TableNodes:
            if name == node.GetName():  # Table exists, erase it
                slicer.mrmlScene.RemoveNode(node)

        # # prepare clean table
        resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLTableNode')
        resultsTableNode.SetName(name)
        table = resultsTableNode.GetTable()

        segmentColumnValue = vtk.vtkStringArray()
        segmentColumnValue.SetName("Segment")
        table.AddColumn(segmentColumnValue)

        segmentColumnValue = vtk.vtkStringArray()
        segmentColumnValue.SetName("Volume (cm^3)")
        table.AddColumn(segmentColumnValue)

        segmentColumnValue = vtk.vtkStringArray()
        segmentColumnValue.SetName("Sensitivity (cps/MBq)")
        table.AddColumn(segmentColumnValue)

        table.SetNumberOfRows(1)

        table.SetValue(0, 0, SegmentName)
        table.SetValue(0, 1, segVolume / self.units['cm^3'])
        table.SetValue(0, 2, Sensitivity * self.units['MBq'])

        resultsTableNode.SetAttribute(
            Attributes().studyCreation, utils.getCurrentTime())

        # Show the Table
        vtkmrmlutils.showTable(resultsTableNode)

        # Save the results
        sensitivityJSON = config.readSPECTSensitivity()
        if not center in sensitivityJSON:
            sensitivityJSON[center] = {}

        if not isotopeText in sensitivityJSON[center]:
            sensitivityJSON[center][isotopeText] = {}

        date = nodeSensitivity.GetAttribute(Attributes().acquisition)
        sensitivityJSON[center][isotopeText][date] = {
            'Volume': tankVolume / self.units['cm^3'],  # Tank Volume
            'Sensitivity': Sensitivity * self.units['MBq'],
            'Units': 'counts/MBqs',
            'Time': duration,
            'WaitTime': restTime / self.units['sec']
        }
        config.writeSPECTSensitivity(sensitivityJSON)
