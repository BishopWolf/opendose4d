import platform
import re
import sentry_sdk as sentry

import Logic.config as config
import Logic.errors as errors


def init():
    # only init sentry if prod env
    if config.read('default', 'env') == 'prod':
        try:
            userID = config.read('default', 'userID')
        except:
            raise

        # enhance with attributes which should be set as a default for every log
        with sentry.configure_scope() as scope:
            scope.user = {'id': userID}
            scope.level = 'error'
            scope.set_tag('os', platform.platform())
            scope.set_tag('os.name', platform.system())
            scope.set_tag('os.version', platform.release())

        dns = config.read('sentry', 'dns')
        sentry.init(dns)


def extras(exception, attachment):
    # only include extras in prod env
    if config.read('default', 'env') != 'prod':
        return None

    else:
        extras = {}
        if isinstance(exception, errors.IOError):
            extras['issue'] = 'io'
            if attachment:
                extras['content'] = attachment

        elif isinstance(exception, errors.FileError):
            extras['issue'] = 'file'
            if attachment:
                extras['path'] = anonymizePath(attachment)

        elif isinstance(exception, errors.ConventionError):
            extras['issue'] = 'convention'
            if attachment:
                extras['name'] = attachment

        elif isinstance(exception, errors.DICOMError):
            extras['issue'] = 'dicom'
            if attachment:
                extras['dicom'] = anonymizeDICOM(attachment)

        elif isinstance(exception, errors.NetworkError):
            extras['issue'] = 'network'
            if attachment:
                extras['url'] = attachment

        elif isinstance(exception, errors.XMLError):
            extras['issue'] = 'xml'
            if attachment:
                extras['xml'] = attachment

        else:
            extras['issue'] = 'not-defined'
            if attachment:
                extras['extra'] = attachment

        return extras


def anonymizeDICOM(dataset):
    # remove all 'private' attributes, i.e. non-standard tags
    dataset.remove_private_tags()

    # remove all critical standard attributes
    # [0x0008, 0x0080] InstitutionName
    # [0x0008, 0x0081] InstitutionAddress
    # [0x0008, 0x0090] ReferringPhysicianName
    # [0x0008, 0x1010] StationName
    # [0x0008, 0x1040] InstitutionalDepartmentName
    # [0x0008, 0x1050] PerformingPhysicianName
    # [0x0010, 0x0010] PatientName
    # [0x0010, 0x0020] PatientID
    # [0x0010, 0x0030] PatientBirthDate
    # [0x0010, 0x0040] PatientSex
    # [0x0010, 0x1010] PatientAge
    # [0x0010, 0x1020] PatientSize
    # [0x0010, 0x1030] PatientWeight

    tags = [['0x0008', '0x0080'], ['0x0008', '0x0081'], ['0x0008', '0x0090'], ['0x0008', '0x1010'],
            ['0x0008', '0x1040'], ['0x0008', '0x1050'], [
                '0x0010', '0x0010'], ['0x0010', '0x0020'],
            ['0x0010', '0x0030'], ['0x0010', '0x0040'], [
                '0x0010', '0x1010'], ['0x0010', '0x1020'],
            ['0x0010', '0x1030']]

    for tag in tags:
        if tag in dataset:
            dataset[tag[0], tag[1]].value = ''

    return dataset


def anonymizePath(path):
    # NOTE only usable for darwin/linux or windows
    os = platform.system()
    if os == 'Darwin' or 'Linux':
        path = re.sub(r'Users\/\w+', 'Users/user', path)
    elif os == 'Windows':
        path = re.sub(r'Users\\\w+', 'Users\\user', path)
    else:
        return path

    return path
