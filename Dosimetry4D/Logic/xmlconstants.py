from Logic.singleton import Singleton


class XMLConstants(Singleton):

    def __init__(self):
        self.TimePointCategoryDict = {
            '6HR': '6h plus or minus 2h post RAIT timepoint',
            '24HR': '24h plus or minus 4h post RAIT timepoint',
            '48HR': '48h plus or minus 4h post RAIT timepoint',
            '72HR': '72h plus or minus 12h post RAIT timepoint',
            '96HR': '96h plus or minus 12h post RAIT timepoint',
            '168HR': '168h plus or minus 24h post RAIT timepoint'
        }

        self.OrgansCategoryDict = {
            'LLung': 'left lung',
            'RLung': 'right lung',
            'LKidney': 'left kidney',
            'RKidney': 'right kidney',
            'Bones': 'bone',
            'Air': 'air',
            'Tumour': 'tumor',
            'Boundary': 'body surface',
            'L2-L4': 'L2',
            'L2L4': 'L2'
        }

        self.IsotopesCategoryDict = {
            'Ra-223': 'radium223',
            'I-131': 'iodine131',
            'Lu-177': 'lutetium177',
            'Y-90': 'yttrium90',
            'Re-188': 'rhenium188',
            'Tb-161': 'terbium161'
        }

        self.RadiopharmaceuticalCategoryDict = {
            'NaI': "sodiumIodideI131"
        }

        self.FunctionTypeCategory = {
            'trapezoid': 'trapezoid',
            'mexp': 'mono_exponential',
            'bexp': 'bi_exponential',
            'texp': 'tri_exponential',
            'xexp': 'x_exponential'
        }

        self.CalculationAlgorithmCategory = {
            'LocalEnergyDepositionLED': 'local energy deposition',
            'FFT convolution (homogeneous)': 'FFT convolution',
            'Convolution (homogeneous)': 'homogeneous matrix convolution',
            'Convolution (heterogeneous)': 'heterogeneous matrix convolution',
            'MonteCarlo': 'Monte Carlo'
        }

        self.NonDICOMClass = {
            'VOI': 'VOI',
            'VOIs': "VOI superimposed on images",
            'DOSE': "3D absorbed dose map",
            'SEGM': "segmentation",
            'ACTM': "voxel activity map",
            'ADRM': "3D Energy Deposition Rate Matrix",
            'ACSC': "NM Tomo Reconstruction",
            'CTCT': "CT Reconstruction",
            'CTRS': "CT Reconstruction",
            'TRNF': "Registration Matrix",
            'DERS': "Density Image"
        }

        self.TransformationType = {
            'Elastix': 'Advanced elastix transformation',
            'useGeometryAlign': 'Linear transformation matrix',
            'BSpline': 'BSpline transformation'
        }

        self.ClinicalCenters = {
            'UKW': 'Wurburg',
            'UMR': 'Marburg',
            'IUCT-O': 'Toulouse IUCT',
            'RMH': 'Royal Marsden Hospital'
        }
