import json
from pathlib import Path
import time
import lxml.etree as ElementTree
import xmltodict
import xmlschema

import slicer

from Logic.errors import XMLError
import Logic.logging as logging
from Logic.utils import getScriptPath


class xmlutils:

    def __init__(self):
        self.logger = logging.getLogger('Dosimetry4D.xmlutils')
        self.xsd = None
        self.xml = None
        self.script_path = getScriptPath()

    def setXSD(self, path):
        lpath = Path(path)
        if lpath.exists():
            self.xsd = xmlschema.XMLSchema(str(lpath))
        else:
            self.logger.error('Could not find file with path: %s', path)

    def setXML(self, path):
        lpath = Path(path)
        if lpath.exists():
            self.xml = ElementTree.parse(str(lpath))
        else:
            self.logger.error('Could not find file with path: %s', lpath)

    def setXMLfromIO(self, IOobject):
        try:
            self.xml = ElementTree.parse(IOobject)
        except Exception as e:
            self.logger.error('Could not load: %s', e)

    def fromString(self, string):
        try:
            parser = ElementTree.XMLParser(
                remove_blank_text=True, remove_comments=True)
            self.xml = ElementTree.fromstring(string, parser=parser)
        except Exception as e:
            self.logger.error('Could not load: %s', e)

    def trim(self):
        parser = ElementTree.XMLParser(
            remove_blank_text=True, remove_comments=True)
        self.xml = ElementTree.fromstring(str(self), parser=parser)

    def toString(self):
        return str(self)

    def validate(self):
        '''
        Validate XML against XSD
        '''
        valid = False
        try:
            valid = self.xsd.is_valid(self.xml)
        except:
            self.logger.error('XSD or XML are not set.')
        finally:
            return valid

    def getDictionary(self):
        '''
        Returns an editable python dictionary for the XML with XSD validation
        '''
        import ast

        try:
            xmlstr = ElementTree.tostring(self.xml)
            return ast.literal_eval(json.dumps(xmltodict.parse(xmlstr)))
        except:
            self.logger.critical('XSD or XML are not set.')

    def setDictToXML(self, dictionary):
        '''
        validates a python dictionary and if it valid set up XML attributes accordingly
        '''
        xmlstr = xmltodict.unparse(dictionary)
        parser = ElementTree.XMLParser(
            remove_blank_text=True)  # lxml.etree only!
        self.xml = ElementTree.fromstring(bytes(xmlstr, 'utf-8'), parser)

    def writeXML(self, filename):
        '''
        Saves an XML in specified path using the info in self.xml
        '''
        if not self.validate():
            self.logger.error('XML object is not valid, impossible to save!')
            raise XMLError('XML object is not valid, impossible to save!')
        lfilename = Path(filename)
        self.xml.write(str(lfilename))

    def __repr__(self):
        return ElementTree.tostring(self.xml, encoding=str)

    def save(self):
        filename = Path.home() / \
            f'saved-scene-{time.strftime("%Y%m%d-%H%M%S")}.mrb'

        # Save scene
        if slicer.util.saveScene(filename):
            self.logger.info(f"Scene saved to: {filename}")
        else:
            self.logger.error("Scene saving failed")
