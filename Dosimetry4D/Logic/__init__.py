#Check requirements
from .utils import checkRequirements
checkRequirements()

# Import public classes
from .Dosimetry4DLogic import Dosimetry4DLogic
from .Dosimetry4DTest import Dosimetry4DTest
from .vtkmrmlutilsTest import vtkmrmlutilsTest
from .xmlutilsTest import xmlutilsTest
from .nodesTest import nodesTest
from .errors import *
