from datetime import datetime
from pathlib import Path
import tempfile
import pickle
import numpy as np
import qt
import vtk
from scipy import signal

import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModuleLogic
try:
    import Elastix
    USE_ELASTIX = True
except:
    USE_ELASTIX = False

from Logic.attributes import Attributes
from Logic.CLIScripts.Convolution import Convolution
import Logic.errors as errors
import Logic.fit_values as fit_values
import Logic.logging as logging
from Logic.PhysicalUnits import PhysicalUnits
from Logic.constants import Constants
import Logic.vtkmrmlutils as vtkmrmlutils
import Logic.utils as utils
import Logic.Process as Process
from Logic.nodes import Node


class Dosimetry4DLogic(ScriptedLoadableModuleLogic):
    """
    This class implements all the actual computation done by the module.
    The interface should be such, that other python code can import this class
    and make use of the functionality without requiring an instance of
    the Widget. Uses ScriptedLoadableModuleLogic base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent=None, isotopeText='Lu-177'):
        super().__init__(parent=parent)
        self.logger = logging.getLogger('Dosimetry4D.Logic')

        # Variables
        self.parent = parent
        self.data = {}
        self.tableheader = {}
        self.units = {}
        self.CpuCores, self.CpuProcessors, self.system = utils.getSystemCoresInfo()
        self.logger.info(
            f"Detected {self.CpuProcessors} processors with {self.CpuCores} physical cores")
        self.injectedActivity = -1

        # Setup Variables
        self.setupUnits()
        self.PETnodeColor = slicer.util.getFirstNodeByName('PET-Heat')
        self.GreynodeColor = slicer.util.getFirstNodeByName('Grey')
        self.script_path = utils.getScriptPath()
        self.logger.info(f'Loading data for isotope {isotopeText}')
        self.Isotope = Constants().isotopes[isotopeText]
        self.logyaxis = False
        self.referenceFolderID = 0

    def setupUnits(self):
        physicUnits = PhysicalUnits()
        self.units['MBq'] = physicUnits.getUnit('Activity', 'MBq')
        self.units['Gy'] = physicUnits.getUnit('Dose', 'Gy')
        self.units['mGy'] = physicUnits.getUnit('Dose', 'mGy')
        self.units['hour'] = physicUnits.getUnit('Time', 'h')
        self.units['gram'] = physicUnits.getUnit('Mass', 'g')
        self.units['kg'] = physicUnits.getUnit('Mass', 'kg')
        self.units['cm3'] = physicUnits.getUnit('Longitude', 'cm')**3
        self.units['mm'] = physicUnits.getUnit('Longitude', 'mm')
        self.units['mm3'] = self.units['mm']**3
        self.units['g_cm3'] = self.units['gram']/self.units['cm3']
        self.units['Joule'] = physicUnits.getUnit('Energy', 'J')
        self.units['MeV'] = physicUnits.getUnit('Energy', 'MeV')
        self.units['mGy_MBqs'] = self.units['mGy'] / self.units['MBq'] / \
            physicUnits.getUnit('Time', 's')
        self.units['mGy_h'] = self.units['mGy'] / self.units['hour']
        self.units['MBqh'] = self.units['MBq'] * self.units['hour']

    def standardise(self, injectedActivity=-1):
        '''
        method to prepare raw data to be grouped by subject hierarchy and image name
        NOTE your data must have names according to the convention of either "[J][0-9]" or "\\d+h|h\\d+"
        TODO we might require to have searchSTR as input to allow flexibility
        '''
        try:
            _, studyID = self.standardiseHierarchy()
        except errors.ConventionError as err:
            self.showError(err.message)
            return

        try:
            self.standardiseNodes(studyID, injectedActivity)
        except errors.ConventionError as err:
            self.showError(err.message)
            return

    def standardiseHierarchy(self):
        """
        Setup a default hierarchy or incorporate existing folder structure
        """
        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        sceneItemID = shNode.GetSceneItemID()

        # define subject
        subjectIDs = vtkmrmlutils.getSubjectIDs()
        if len(subjectIDs) == 0:
            subjectID = shNode.CreateSubjectItem(sceneItemID, 'Subject')
        elif len(subjectIDs) == 1:
            subjectID = subjectIDs[0]
        else:
            self.logger.error(
                "Failed to standardise hierarchy, multiple subjects exist")
            raise errors.ConventionError(
                f"Scene contains {len(subjectIDs)} subjects. Please remove all but one subject.")

        # get study
        studyIDs = vtkmrmlutils.getStudyIDs()
        if len(studyIDs) == 0:
            studyID = shNode.CreateStudyItem(subjectID, 'Study')
        elif len(studyIDs) >= 1:
            studyID = studyIDs[0]
        else:
            self.logger.error(
                "Failed to standardise hierarchy, multiple studies exist")
            raise errors.ConventionError(
                f"Subject contains {len(studyIDs)} studies. Please remove all but one study.")

        # get results folder
        resultIDs = vtkmrmlutils.getResultIDs()
        if len(resultIDs) == 0:
            _ = vtkmrmlutils.createResultsFolder(studyID)
        elif len(resultIDs) == 1:
            ResultFolder = resultIDs[0]
        else:
            self.logger.error(
                "Failed to standardise hierarchy, multiple results folder exist")
            raise errors.ConventionError(
                f"Study contains {len(resultIDs)} results folder. Please remove all but one results folder.")

        return subjectID, studyID

    def standardiseNodes(self, studyID, injectedActivity=-1):
        """
        Standardise node naming and move nodes to correct folder
        """
        volumeNodeIDs = vtkmrmlutils.getScalarVolumeNodes()

        # setup progress bar
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(volumeNodeIDs))
        if not volumeNodeIDs:
            ProgressDialog.close()
            self.showError('Please provide image data for renaming.')
            self.logger.error('Failed to rename files, no image data found')

        # search for initial acquisition time
        try:
            timeStamp, hours0, self.injectedActivity, injectionTime = Node.getInitialTime()
        except errors.ConventionError as e:
            ProgressDialog.close()
            raise e
        if self.injectedActivity < 0:
            self.injectedActivity = injectedActivity

        # process nodes (erase invalid ones)
        for index, volumeNodeID in enumerate(volumeNodeIDs):
            try:
                node = Node.new(volumeNodeID)
            except:
                node = vtkmrmlutils.getItemDataNode(volumeNodeID)
                self.logger.error(
                    f'Erasing non conforming node {node.GetName()}...')
                slicer.mrmlScene.RemoveNode(node)
                continue

            hours = vtkmrmlutils.getItemDataNodeAttributeValue(
                node.nodeID, Attributes().timeStamp)
            if not hours:
                vtkmrmlutils.setItemDataNodeAttribute(
                    node.nodeID, Attributes().injectionTime, injectionTime)
                acquisitionTimeStr = node.getAcquisition()
                if acquisitionTimeStr:
                    acquisitionTime = datetime.strptime(
                        acquisitionTimeStr, '%Y-%m-%d %H:%M:%S')
                    difference = acquisitionTime - timeStamp
                    hours = float(difference.total_seconds()) / \
                        float(self.units['hour']) + hours0

            if not hours:
                try:
                    _, hours = node.extractTime()
                except:
                    ProgressDialog.close()
                    raise

            hours = float(hours)
            vtkmrmlutils.setItemDataNodeAttribute(
                node.nodeID, Attributes().timeStamp, hours)

            ProgressDialog.labelText = f"Processing {node.name}..."
            ProgressDialog.setValue(index)
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            hours = int(round(float(node.getTimeStamp())))
            if hours is None:
                hours = 0

            if hours >= 0:
                if hours > 12:
                    # Avoid duplicating folders for the same day, except the first day
                    hours = round(hours/24)*24

            try:
                node.rename(hours)
            except:
                ProgressDialog.close()
                raise

            vtkmrmlutils.setAndObserveColorNode(node.nodeID)

            # create folder (named by hours) if not already exists inside study
            folderName = f'{hours}HR'
            folderID = vtkmrmlutils.getFolderChildByName(studyID, folderName)
            if not folderID:
                folderID = vtkmrmlutils.createFolder(studyID, folderName)

            vtkmrmlutils.setItemFolder(node.nodeID, folderID)

        ProgressDialog.close()

        self.referenceFolderID = self.getReferenceFolder()
        if self.referenceFolderID == 0:
            raise errors.ConventionError(
                'No CT study available, impossible to proceed')

        referencenodes = Node.getFolderChildren(self.referenceFolderID)
        if 'CTCT' in referencenodes:
            minCTID = referencenodes['CTCT'].data.GetID()
        elif 'CTRS' in referencenodes:
            minCTID = referencenodes['CTRS'].data.GetID()
        else:
            raise errors.ConventionError(
                'No CT study available, impossible to proceed')

        if 'ACSC' in referencenodes:
            minSPECTID = referencenodes['ACSC'].data.GetID()
            self.spacing = referencenodes['ACSC'].data.GetSpacing()
        else:
            raise errors.ConventionError(
                'No SPECT study available, impossible to proceed')

        vtkmrmlutils.setSlicerViews(minCTID, minSPECTID)

        self.fixManualTransformations()
        self.checkAttributes()

    def fixManualTransformations(self):
        # FIX manual transformations
        referencenodes = Node.getFolderChildren(self.referenceFolderID)
        folders = vtkmrmlutils.getFolderIDs()
        for index, folderID in enumerate(folders):
            nodes = Node.getFolderChildren(folderID)
            if 'TRNF' in nodes:
                node = nodes['TRNF'].data
                if not vtkmrmlutils.hasItemDataNodeAttribute(nodes['TRNF'].nodeID, Attributes().modality):
                    if 'CTCT' not in nodes:
                        node.SetAttribute(
                            Attributes().registrationVolume, str(nodes['ACSC'].nodeID))
                        node.SetAttribute(Attributes().registrationReference, str(
                            referencenodes['ACSC'].nodeID))
                    else:
                        node.SetAttribute(
                            Attributes().registrationVolume, str(nodes['ACSC'].nodeID))
                        node.SetAttribute(
                            Attributes().registrationReference, str(nodes['CTCT'].nodeID))
                    node.SetAttribute(
                        Attributes().registrationSampling, str(0.02))
                    node.SetAttribute(
                        Attributes().registrationMode, 'useGeometryAlign')
                    node.SetAttribute(Attributes().registrationRigid, 'True')
                    node.SetAttribute(Attributes().registrationAffine, 'False')
                    node.SetAttribute(
                        Attributes().registrationAlgorithm, 'Manual')
                    node.SetAttribute(
                        Attributes().studyCreation, utils.getCurrentTime())
                    node.SetAttribute(Attributes().modality, 'REG')

    def checkAttributes(self):
        '''
        Runs sanity checks for the each node in each folder against a reference node
        and sets default attributes if they're missing.
        '''
        folders = vtkmrmlutils.getFolderIDs()
        referenceFolderID = self.referenceFolderID

        try:
            referenceNodes = Node.getFolderChildren(referenceFolderID)
            referenceNode = referenceNodes['ACSC']
        except:
            self.logger.error(
                f'Could not find a reference node in reference folder with id: {referenceFolderID}')
            return False

        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(folders))

        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            ProgressDialog.labelText = f"Performing Sanity check on folder {folderName} ..."
            ProgressDialog.value = index
            ProgressDialog.show()
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            # exclude the reference folder from checks
            if folderID != referenceFolderID:
                try:
                    nodes = Node.getFolderChildren(folderID)
                    node = nodes['ACSC']
                except:
                    self.logger.error(
                        f'No node to compare to reference node in folder with id {folderID} found')
                    ProgressDialog.close()
                    return False

                if not node.compareAttributes(referenceNode):
                    self.showError(
                        'The attributes of the given nodes differ. Please check the integrity of the provided data.')
                    ProgressDialog.close()
                    return False

        ProgressDialog.close()
        self.logger.info("Sanity check successful!!")
        return True

    def showError(self, error):
        '''
        displays an error as a feedback to the user
        '''
        # TODO create a new class for all this frontend stuff
        errorPopup = qt.QErrorMessage()
        errorPopup.showMessage(error)
        errorPopup.exec_()

    def importMonteCarlo(self):
        '''
        imports monte carlo doses
        '''
        self.standardise()
        nodes = slicer.util.getNodesByClass('vtkMRMLScalarVolumeNode')
        folders = vtkmrmlutils.getFolderIDs()

        monteCarloNodes = [
            node for node in nodes if 'monte' in node.GetName().lower()]

        for node in monteCarloNodes:
            nodeID = vtkmrmlutils.getItemID(node)
            lnode = Node.new(nodeID)
            try:
                lnode.setModality()
            except:
                raise

    def clean(self):
        ''' Cleans the setup from extra files and reinitializes all important data
        '''
        self.data = {}
        self.tableheader = {}
        nodes = slicer.util.getNodesByClass('vtkMRMLScalarVolumeNode')
        for node in nodes:
            name = node.GetName()
            if not any(ext in name for ext in ('ACSC', 'CTCT', ': CT')):
                slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLTransformNode')
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLTableNode')
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLSegmentationNode')
        for node in nodes:
            if 'SEGM' in node.GetName():  # Only remove propagated segmentations
                slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLPlotSeriesNode')
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass('vtkMRMLPlotChartNode')
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)

    def getReferenceFolder(self):
        folders = vtkmrmlutils.getFolderIDs()
        minFolderID = 0
        hmin = 1000
        for folderID in folders:
            nodes = Node.getFolderChildren(folderID)

            if 'CTCT' in nodes and 'ACSC' in nodes:  # we need a folder with both CT and SPECT
                node = nodes['CTCT']
            else:
                continue

            _, hours = node.extractTime()

            if hours <= hmin:
                hmin = hours
                minFolderID = folderID

        return minFolderID

    def resampleCT(self):
        '''
        resample all CT nodes to SPECT resolution, keep original data
        '''
        folders = vtkmrmlutils.getFolderIDs()
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(folders))
        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            ProgressDialog.labelText = f"Processing {folderName}..."
            ProgressDialog.value = index
            ProgressDialog.show()
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            # SPECT
            nodeSPECT = nodes['ACSC']
            newName = nodeSPECT.name.replace('ACSC', 'CTRS')

            # CT
            if 'CTCT' in nodes:
                nodeCT = nodes['CTCT']
            else:
                minNodes = Node.getFolderChildren(self.referenceFolderID)
                nodeCT = minNodes['CTCT']

            if 'CTRS' in nodes:
                slicer.mrmlScene.RemoveNode(nodes['CTRS'].data)

            # Output Rescaled CT using SPECT sampling
            minCT = float(min(nodeCT.getArrayData().ravel()))
            CTRSNode = slicer.mrmlScene.AddNewNodeByClass(
                'vtkMRMLScalarVolumeNode')
            CTRSItemID = vtkmrmlutils.getItemID(CTRSNode)

            parameters = {'inputVolume': nodeCT.data, 'referenceVolume': nodeSPECT.data, 'outputVolume': CTRSNode,
                          'interpolationMode': 'Lanczos', 'defaultValue': minCT}

            slicer.cli.run(slicer.modules.brainsresample,
                           None, parameters, wait_for_completion=True, update_display=False)

            CTRSNode.SetAttribute(Attributes().patientID,
                                  nodeSPECT.getPatientID())
            CTRSNode.SetAttribute(
                Attributes().seriesInstanceUID, nodeCT.getSeriesInstanceUID())
            CTRSNode.SetAttribute(
                Attributes().studyInstanceUID, nodeCT.getStudyInstanceUID())
            CTRSNode.SetAttribute(Attributes().instanceUIDs,
                                  nodeSPECT.getInstanceUIDs())
            CTRSNode.SetAttribute(Attributes().acquisition,
                                  nodeSPECT.getAcquisition())
            CTRSNode.SetAttribute(
                Attributes().acquisitionDuration, nodeSPECT.getAcquisitionDuration())
            CTRSNode.SetAttribute(Attributes().patientName,
                                  nodeSPECT.getPatientName())
            CTRSNode.SetAttribute(Attributes().modality, 'CTRS')
            CTRSNode.SetAttribute(Attributes().timeStamp,
                                  nodeSPECT.getTimeStamp())
            CTRSNode.SetAttribute(Attributes().resamplingMode, 'Lanczos')
            CTRSNode.SetAttribute(
                Attributes().resamplingVolume, str(nodeCT.nodeID))
            CTRSNode.SetAttribute(
                Attributes().resamplingReference, str(nodeSPECT.nodeID))
            CTRSNode.SetAttribute(
                Attributes().studyCreation, utils.getCurrentTime())
            CTRSNode.Modified()

            nodes['CTRS'] = Node.new(CTRSItemID)
            nodes['CTRS'].setVisualization()
            nodes['CTRS'].setNameParent(newName, folderID)

            self.logger.info(f"Folder {folderName} processed")

        ProgressDialog.close()

    def scaleValues(self, calibration, injectedActivity):
        ''' method to perform image scaling by a factor
            passed as value of dictionnary
            TODO: rephrase
        '''
        folders = vtkmrmlutils.getFolderIDs()
        cSensitivity = calibration['SPECTSensitivity']['Value']
        # from counts/Bq to Bq/counts
        if "counts/" in calibration['SPECTSensitivity']['Units']:
            cSensitivity = 1/cSensitivity
        if not 'MBq' in calibration['SPECTSensitivity']['Units']:  # to MBq/counts
            cSensitivity /= 1e6
        correctFactor = 'Bqs' in calibration['SPECTSensitivity']['Units']
        if not correctFactor:
            cSensitivity *= calibration['SPECTSensitivity']['Time']

        ders = slicer.util.getNodes('*CTRS*')
        if len(ders) == 0:
            self.logger.error(
                'Resampled CT images not present, please create them before creating transforms')
            return
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(folders)*3)

        for index, folderID in enumerate(folders):
            # Initialize folder
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            ProgressDialog.labelText = f"Processing {folderName}...Activity Map"
            ProgressDialog.value = 3*index
            ProgressDialog.show()
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            # Scale SPECT to Activity Map
            ACSCNode = nodes['ACSC']
            newName = ACSCNode.name.replace('ACSC', 'ACTM')
            try:
                duration = float(actNode.getAcquisitionDuration())
            except:
                self.logger.error(
                    "Acquisition duration not set in DICOM, using calibration time")
                duration = calibration['SPECTSensitivity']['Time']

            try:
                localInjectedActivity = float(ACSCNode.getInjectedActivity())
            finally:
                if float(injectedActivity) != localInjectedActivity:
                    ACSCNode.data.SetAttribute(
                        Attributes().injectedActivity, str(injectedActivity))
                localInjectedActivity = float(injectedActivity)

            if 'ACTM' in nodes:
                slicer.mrmlScene.RemoveNode(nodes['ACTM'].data)

            ACTMNode = vtkmrmlutils.cloneNode(ACSCNode.data, newName)
            actmItemID = vtkmrmlutils.getItemID(ACTMNode)
            ACTMNode.SetAttribute(Attributes().sensitivity, str(
                calibration['SPECTSensitivity']['Value']))
            ACTMNode.SetAttribute(Attributes().sensitivityUnits, str(
                calibration['SPECTSensitivity']['Units']))
            ACTMNode.SetAttribute(
                Attributes().acquisitionDuration, str(duration))
            ACTMNode.SetAttribute(Attributes().calibrationDuration, str(
                calibration['SPECTSensitivity']['Time']))
            ACTMNode.SetAttribute(
                Attributes().isotope, str(self.Isotope['name']))
            ACTMNode.SetAttribute(
                Attributes().rescalingVolume, str(ACSCNode.nodeID))
            ACTMNode.SetAttribute(Attributes().modality, 'RWV')
            ACTMNode.SetAttribute(
                Attributes().studyCreation, utils.getCurrentTime())
            ACTMNode.SetAttribute(
                Attributes().injectedActivity, str(localInjectedActivity))

            vox_arr = ACSCNode.getArrayData()

            lsensitivity = cSensitivity / duration

            # to MBq, we consider MBq everywhere
            vox_arr = lsensitivity * vox_arr
            vox_arr.astype(int)

            nodes['ACTM'] = Node.new(actmItemID)
            nodes['ACTM'].setArrayData(vox_arr)

            # Scale rescaled CT to density map
            ProgressDialog.labelText = f"Processing {folderName}...CT rescaled density Map"
            ProgressDialog.value = 3*index + 1
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()
            if 'CTRS' in nodes:
                ctrsnode = nodes['CTRS']
            else:
                continue

            newName = ctrsnode.name.replace('CTRS', 'DERS')
            if 'DERS' in nodes:
                slicer.mrmlScene.RemoveNode(nodes['DERS'].data)

            DERSNode = vtkmrmlutils.cloneNode(ctrsnode.data, newName)
            dersItemID = vtkmrmlutils.getItemID(DERSNode)
            DERSNode.SetAttribute(Attributes().modality, 'RWV')
            DERSNode.SetAttribute(
                Attributes().calibrationCT, str(calibration))
            DERSNode.SetAttribute(
                Attributes().rescalingVolume, str(ctrsnode.nodeID))
            DERSNode.SetAttribute(
                Attributes().studyCreation, utils.getCurrentTime())

            vox_arr = ctrsnode.getArrayData()
            # use same scale as original CT
            vox_arr = np.where(vox_arr < 0,
                                calibration['CTCalibration']['a0'] *
                                vox_arr +
                                calibration['CTCalibration']['b0'],
                                calibration['CTCalibration']['a1'] *
                                vox_arr + calibration['CTCalibration']['b1']
                                )
            vox_arr = np.where(
                vox_arr < 0, 0, vox_arr * self.units['g_cm3'])

            nodes['DERS'] = Node.new(dersItemID)
            nodes['DERS'].setArrayData(vox_arr)

            # Scale also CT to Density map
            ProgressDialog.labelText = f"Processing {folderName}...CT density Map"
            ProgressDialog.value = 3*index + 2
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            if 'CTCT' in nodes:
                CTCTNode = nodes['CTCT']
            else:
                continue

            newName = CTCTNode.name.replace('CTCT', 'DENS')
            if 'DENS' in nodes:
                slicer.mrmlScene.RemoveNode(nodes['DENS'].data)

            DENSNode = vtkmrmlutils.cloneNode(CTCTNode.data, newName)
            densItemID = vtkmrmlutils.getItemID(DENSNode)
            DENSNode.SetAttribute(Attributes().modality, 'RWV')
            DENSNode.SetAttribute(Attributes().calibrationCT, str(
                calibration['CTCalibration']))
            DENSNode.SetAttribute(
                Attributes().rescalingVolume, str(CTCTNode.nodeID))
            DENSNode.SetAttribute(
                Attributes().studyCreation, utils.getCurrentTime())

            vox_arr = CTCTNode.getArrayData()
            vox_arr = np.where(vox_arr < 0,
                                calibration['CTCalibration']['a0'] *
                                vox_arr +
                                calibration['CTCalibration']['b0'],
                                calibration['CTCalibration']['a1'] *
                                vox_arr + calibration['CTCalibration']['b1']
                                )
            # to kg_m3, we need kg and common unit for distance
            vox_arr = np.where(
                vox_arr < 0, 0, vox_arr * self.units['g_cm3'])

            nodes['DENS'] = Node.new(densItemID)
            nodes['DENS'].setArrayData(vox_arr)

            self.logger.info(f"Folder {folderName} processed")

        ProgressDialog.close()

    def createTransforms(self, reference):
        ''' Creates the transformations in all time points
            Performs initial COM registration
            Performs Elastix Automatic registration
            TODO: Affine registration is not working properly but it is the default when no elastix is found
        '''
        global USE_ELASTIX
        if USE_ELASTIX:
            elastixLogic = Elastix.ElastixLogic()
            parameterFilenames = elastixLogic.getRegistrationPresets(
            )[0][Elastix.RegistrationPresets_ParameterFilenames]

        folders = vtkmrmlutils.getFolderIDs()
        referenceName = reference.GetName()
        folderIDR = vtkmrmlutils.getFolderID(referenceName)
        self.referenceFolderID = folderIDR
        nodes = Node.getFolderChildren(folderIDR)
        if 'DENS' not in nodes:
            self.logger.error(
                'Densities not present, please create them before creating transforms')
            return

        densRnode = nodes['DENS']
        spacing = reference.GetSpacing()
        densR_arr = slicer.util.arrayFromVolume(densRnode.data).astype(float)
        CMRef = utils.Center_of_Mass(densR_arr, spacing)
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(folders))

        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            if folderID == folderIDR:  # skip reference folder
                continue

            nodes = Node.getFolderChildren(folderID)
            if 'DENS' not in nodes or not nodes['DENS']:
                continue

            DENSNode = nodes['DENS']
            vtkmrmlutils.setSlicerViews(densRnode.data.GetID(), DENSNode.data.GetID())
            ProgressDialog.labelText = f"Registering {DENSNode.name} to {densRnode.name}..."
            ProgressDialog.value = index
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            # Create transformation
            transformName = DENSNode.name.replace(
                'DENS', 'TRNF').replace('CT', 'LinearTransform')
            if 'TRNF' not in nodes:
                transformNode = slicer.mrmlScene.AddNewNodeByClass(
                    'vtkMRMLTransformNode')
                transformNode.SetName(transformName)
                transformID = vtkmrmlutils.getItemID(transformNode)
                # here it is the SH ID
                vtkmrmlutils.setNodeFolder(transformNode, folderID)

                # Get center of mass and apply as initial transform
                densN_arr = slicer.util.arrayFromVolume(
                    DENSNode.data).astype(float)
                CMNode = utils.Center_of_Mass(densN_arr, spacing)
                TranslationVector = CMNode-CMRef
                vtransform = vtk.vtkTransform()
                vtransform.Translate(TranslationVector)
                transformNode.SetMatrixTransformToParent(vtransform.GetMatrix())
                useAffine = False
            else:
                transformNode = nodes['TRNF'].data
                useAffine = True
            # The procedures below require MRML ID
            transformID = transformNode.GetID()

            # Apply transformation to all volumes inside the folder
            for key, node in nodes.items():
                if key == 'TABL' or key == 'SEGM':
                    continue
                if key == 'ADRM':
                    for inode in node.values():
                        inode.data.SetAndObserveTransformNodeID(
                            transformID)
                else:
                    node.data.SetAndObserveTransformNodeID(transformID)

            # Perform the registration
            ProgressDialog.labelText = f"Performing registration in {folderName}..."
            slicer.app.processEvents()

            if USE_ELASTIX:  # Try first elastix
                self.logger.info("Using Elastix")
                transformNode.SetName(transformName.replace(
                    'LinearTransform', 'ElastixTransform'))
                registrationMode = 'Elastix'
                useAffine = False
                useRigid = False
                algorithm = 'Automatic registration'

                elastixLogic.registerVolumes(
                    densRnode.data, DENSNode.data,
                    parameterFilenames=parameterFilenames,
                    outputTransformNode=transformNode
                )

            else:  # elastix does not worked, try brainsfit
                self.logger.info("Using Brains linear")
                transformNode.SetName(transformName.replace(
                    'ElastixTransform', 'LinearTransform'))
                registrationMode = 'useGeometryAlign'
                algorithm = 'Semi-automatic registration'
                useRigid = True
                parameters = {
                    'fixedVolume': densRnode.data,
                    'movingVolume': DENSNode.data,
                    'samplingPercentage': 0.02,
                    'linearTransform': transformID,
                    'initialTransform': transformID,
                    'initializeTransformMode': registrationMode,
                    'useRigid': useRigid,
                    'useAffine': useAffine,
                    'maximumStepLength': 0.5}

                slicer.cli.run(slicer.modules.brainsfit, None,
                                parameters, wait_for_completion=True, update_display=False)

            transformNode.SetAttribute(
                Attributes().registrationVolume, str(densRnode.nodeID))
            transformNode.SetAttribute(
                Attributes().registrationReference, str(DENSNode.nodeID))
            transformNode.SetAttribute(
                Attributes().registrationSampling, str(0.02))
            transformNode.SetAttribute(
                Attributes().registrationMode, registrationMode)
            transformNode.SetAttribute(
                Attributes().registrationRigid, str(useRigid))
            transformNode.SetAttribute(
                Attributes().registrationAffine, str(useAffine))
            transformNode.SetAttribute(
                Attributes().registrationAlgorithm, algorithm)
            transformNode.SetAttribute(
                Attributes().studyCreation, utils.getCurrentTime())
            transformNode.SetAttribute(Attributes().modality, 'REG')
            slicer.app.processEvents()

        ProgressDialog.close()

    def o_LED(self, folderID, threshold):
        ''' Method to perform LED in one time point
        '''
        nodes = Node.getFolderChildren(folderID)
        if 'ACTM' not in nodes:
            self.logger.error(
                'Study scaling has not been performed, please do so before calling this method')
            return

        actNode = nodes['ACTM']

        if 'DERS' not in nodes:
            minNodes = Node.getFolderChildren(self.minFolderID)

            if 'DERS' not in minNodes:
                self.logger.error(
                    'Study scaling has not been performed, please do so before calling this method')
                return

            DENSNode = minNodes['DERS']
        else:
            DENSNode = nodes['DERS']

        self.logger.info(f"Reading node {actNode.name}")
        act_arr = actNode.getArrayData()
        # This threshold is to eliminate statistical noise
        th = max(act_arr.ravel()) * threshold
        # set threshold to avoid noise
        new_arr = np.where(act_arr > th, act_arr,
                           0)  # In this case we need Bq
        shape = np.shape(act_arr)
        size = 1
        for x in shape:
            size *= x

        voxelVolume = np.prod(actNode.data.GetSpacing()) * self.units['mm3']

        self.logger.info(f"Reading node {DENSNode.name}")
        mass_arr = DENSNode.getArrayData() * voxelVolume  # get the mass per voxel (kg)

        if np.array_equal(np.shape(act_arr), np.shape(mass_arr)):
            # The following is in mGy/h
            new_arr = np.divide(new_arr, mass_arr, out=np.zeros_like(new_arr), where=mass_arr > 0) * \
                self.units['MBq'] * self.Isotope['energy'] / \
                self.units['mGy_h']  # avoid division by 0
            # create the dose volume
            lmode = 'LocalEnergyDepositionLED'
            newName = actNode.name.replace('ACTM', 'ADRM').replace(
                'SPECT', lmode)
            if 'ADRM' in nodes and lmode in nodes['ADRM']:
                slicer.mrmlScene.RemoveNode(nodes['ADRM'][lmode].data)

            clonedNode = vtkmrmlutils.cloneNode(actNode.data, newName)
            clonedItemID = vtkmrmlutils.getItemID(clonedNode)
            clonedNode.SetAttribute(Attributes().modality, 'RTDOSE')
            clonedNode.SetAttribute(
                Attributes().isotope, str(self.Isotope['name']))
            clonedNode.SetAttribute(
                Attributes().doseACTMVolume, str(actNode.nodeID))
            clonedNode.SetAttribute(
                Attributes().doseAlgorithm, 'Local Energy Deposition')
            clonedNode.SetAttribute(
                Attributes().studyCreation, utils.getCurrentTime())

            DOSENode = Node.new(clonedItemID)
            DOSENode.setArrayData(new_arr)

            self.logger.info(f'Node {newName} created...')
            return True

        self.logger.error(
            f"Wrong imput: Images {actNode.name} and {DENSNode.name} don't have the same shape")
        return False

    def LED(self, threshold=0.1):
        ''' method to perform local energy deposition in each time point prior to integration
            TODO: this shall be parallel, but is not working, it is fast anyway
        '''

        self.logger.info("Performing local energy deposition")
        folders = vtkmrmlutils.getFolderIDs()
        for folderID in folders:
            self.o_LED(folderID, threshold)

    def build_kernel(self):
        kernel = np.zeros(
            (2 * self.topx + 1, 2 * self.topy + 1, 2 * self.topz + 1))
        for i in np.arange(-self.topx, self.topx + 1):
            for j in np.arange(-self.topy, self.topy + 1):
                for k in np.arange(-self.topz, self.topz + 1):
                    distance = np.linalg.norm(
                        [i, j, k]*self.DVK_size)/self.units['mm']
                    identifier = abs(i) + abs(j) + abs(k)
                    kernel[i + self.topx, j + self.topy, k + self.topz] = \
                        self.DVK[round(distance, 4)][identifier][0]
        return kernel

    def __convolute(self, folderID, lmode, threshold, multithreaded):
        ''' convolution in the entire 3D matrix for one time point
        '''
        nodes = Node.getFolderChildren(folderID)

        if 'ACTM' not in nodes:
            self.logger.error(
                'Study scaling has not been performed, please do so before calling this method')
            return

        actNode = nodes['ACTM']

        if 'DERS' not in nodes:
            minNodes = Node.getFolderChildren(self.minFolderID)

            if 'DERS' not in minNodes:
                self.logger.error(
                    'Study scaling has not been performed, please do so before calling this method')
                return

            DENSNode = minNodes['DERS']
        else:
            DENSNode = nodes['DERS']

        self.logger.info(f"Reading node {actNode.name}")
        act_arr = actNode.getArrayData()
        new_arr = np.zeros_like(act_arr)
        shape = np.shape(act_arr)
        size = 1
        for x in shape:
            size *= x
        self.size = size

        self.logger.info(f"Reading node {DENSNode.name}")
        densityArray = DENSNode.getArrayData()
        activityArray = act_arr * self.units['MBq']

        # This threshold is to eliminate statistical noise
        canceled = False
        act_ravel = activityArray.ravel()
        threshold = max(act_ravel) * threshold
        # indexes of valid activities
        indexes = np.array(np.nonzero(act_ravel > threshold)).ravel()
        if np.array_equal(np.shape(activityArray), np.shape(densityArray)):
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=100)
            labelText = f"Processing {vtkmrmlutils.getItemName(folderID)}..."
            ProgressDialog.labelText = labelText
            ProgressDialog.show()
            slicer.app.processEvents()
            if 'FFT' in lmode:  # Fourier
                ProgressDialog.value = 1
                slicer.app.processEvents()
                kernel = self.build_kernel()
                act_arr = np.where(  # Apply threshold for consistency
                    activityArray > threshold, activityArray, 0)
                conv_arr = signal.convolve(
                    in1=act_arr, in2=kernel, mode="same")
                # new_arr = np.divide(conv_arr, densityArray, out=np.zeros_like(conv_arr), where=densityArray>0) # Leaved for testing purposes
                new_arr = conv_arr / (1.0*self.units['g_cm3'])
                ProgressDialog.close()
                slicer.app.processEvents()
            else:  # matrix convolution
                new_arr1 = np.zeros(self.size)
                acc = 0
                length1 = np.prod(np.shape(indexes))
                # Initialize distance kernel
                distanceKernel = np.zeros(
                    (2 * self.topx + 1, 2 * self.topy + 1, 2 * self.topz + 1))
                for i in np.arange(-self.topx, self.topx+1):
                    i1 = i * self.DVK_size[0]/self.units['mm']
                    for j in np.arange(-self.topy, self.topy+1):
                        j1 = j * self.DVK_size[1]/self.units['mm']
                        for k in np.arange(-self.topz, self.topz+1):
                            k1 = k * self.DVK_size[2]/self.units['mm']
                            distanceKernel[i + self.topx, j + self.topy, k + self.topz] = np.linalg.norm(
                                [i1, j1, k1])
                distanceKernel = np.round(
                    distanceKernel, decimals=4)

                p0 = [self.topx, self.topy, self.topz]
                if 'homogeneous' in lmode:
                    num = 2
                else:
                    num = np.max(p0)*20

                if multithreaded:
                    ProgressDialog.close()
                    scriptPath = self.script_path / "Logic" / "CLIScripts" / "Convolution.py"

                    chunks = 100  # to avoid overheading
                    iteration = 0
                    args1 = {  # wrap arguments for multithreading, save common objects to share
                        'p0': p0,
                        'DVK': self.DVK,
                        'distance_kernel': distanceKernel,
                        'dens_array': densityArray,
                        'act_array': activityArray,
                        'num': num,
                        'boundary': 'repeat'
                    }
                    with tempfile.TemporaryDirectory() as dirpath:  # Create a temporary path
                        pickleFileName = Path(dirpath) / "OpenDose4D.pckl"
                        with pickleFileName.open('wb') as f:
                            pickle.dump(args1, f)

                        lindexes = {}
                        with Process.ProcessesLogic(windowTitle=labelText) as plogic:
                            while acc < length1:
                                stepsize = max(
                                    1, min(int(length1/chunks), length1-acc))
                                lindex = {'indexes': np.array(
                                    indexes[acc:acc + stepsize])}
                                lindexes[iteration] = lindex['indexes']
                                convolutionProcess = Process.ConvolutionProcess(
                                    scriptPath, str(pickleFileName), lindex, iteration)
                                plogic.addProcess(convolutionProcess)
                                acc += stepsize
                                iteration += 1

                            slicer.app.processEvents()
                            plogic.run()

                        plogic.waitForFinished()
                        canceled = plogic.canceled
                        if canceled:  # Avoid partial results
                            return False

                        for it, elem in lindexes.items():
                            new_arr1[elem] = plogic.results[it]

                else:
                    a1 = 0.0
                    chunks = 1000  # to avoid unresponsiveness
                    convolution = Convolution(
                        [], p0, self.DVK, distanceKernel, densityArray, activityArray, num, 'repeat')
                    while acc < length1:
                        stepsize = max(
                            1, min(int(length1/chunks), length1-acc))
                        if ProgressDialog.wasCanceled:
                            canceled = True
                            slicer.app.processEvents()
                            break
                        convolution.indexes = np.array(
                            indexes[acc:acc + stepsize])

                        new_arr2 = convolution.convolute()
                        new_arr1[convolution.indexes] = new_arr2

                        acc += stepsize
                        a1 += 100/chunks
                        if a1 >= 1:
                            ProgressDialog.value += a1
                            labelText1 = f"{labelText} \n{acc} of {length1}"
                            ProgressDialog.labelText = labelText1
                            a1 = 0

                        slicer.app.processEvents()

                    ProgressDialog.close()

                if canceled:  # Avoid partial results
                    return False

                new_arr = np.reshape(new_arr1, shape, 'C')

            # express in mGy/h
            new_arr = new_arr / self.units['mGy_h']

            newName = actNode.name.replace(
                'ACTM', 'ADRM').replace('SPECT', lmode)

            if 'ADRM' in nodes and lmode in nodes['ADRM']:
                slicer.mrmlScene.RemoveNode(nodes['ADRM'][lmode].data)

            clonedNode = vtkmrmlutils.cloneNode(actNode.data, newName)
            clonedItemID = vtkmrmlutils.getItemID(clonedNode)
            clonedNode.SetAttribute(Attributes().modality, 'RTDOSE')
            clonedNode.SetAttribute(
                Attributes().isotope, str(self.Isotope['name']))
            clonedNode.SetAttribute(
                Attributes().doseACTMVolume, str(actNode.nodeID))
            clonedNode.SetAttribute(Attributes().doseAlgorithm, 'Convolution')
            clonedNode.SetAttribute(
                Attributes().doseAlgorithmDescription, lmode)
            clonedNode.SetAttribute(
                Attributes().studyCreation, utils.getCurrentTime())
            clonedNode.SetAttribute(
                Attributes().doseKernelLimit, str(self.kernellimit))

            DOSENode = Node.new(clonedItemID)
            DOSENode.setArrayData(new_arr)

            self.logger.info(f'Node {newName} created...')
            return True
        else:
            self.logger.error(
                f"Wrong imput: Images {actNode.name} and {DENSNode.name} don't have the same shape")
            return False

    def convolution(self, isotopeText, DVK_size=None, DVK_density=1.0, threshold=0.1, kernellimit=0.0, mode='density_correction', multithreaded=False):
        """@brief Method to perform convolution in all time points prior to integration

            TODO: this shall be parallel, but is not working, it is fast anyway

        @param threshold this is the value in percentage of the maximum of the activity map that will be used for voxel discrimination.
        @param kernellimit this is the maximum distance allowed for the kernel, 0 means use all kernel.
        """
        import ast
        import Logic.dbutils as dbutils
        self.DVK = {}
        if DVK_size is None:
            self.DVK_size = np.round(
                self.spacing, decimals=2) * self.units['mm']
            self.logger.info(
                f"Loaded DVK {isotopeText} - {self.DVK_size/self.units['mm']} mm")
        elif np.isscalar(DVK_size):
            self.DVK_size = np.array([DVK_size * self.units['mm']]*3)
        else:
            self.DVK_size = np.array(DVK_size * self.units['mm'])

        self.DVK_density = DVK_density*self.units['g_cm3']

        DVK = dbutils.readDVK(isotopeText, self.DVK_size)

        topdist = 0
        self.topx = 0
        self.topy = 0
        self.topz = 0

        # Rectify limit so we dont have empty slices afterwards
        if kernellimit > 0:
            self.kernellimit = np.max(
                self.DVK_size*np.ceil(np.array([kernellimit]*3)/self.DVK_size[0]))
        else:
            self.kernellimit = 0

        for coord, values in DVK.items():
            l = ast.literal_eval(coord)
            distance = np.linalg.norm(l*self.DVK_size)/self.units['mm']
            if distance <= self.kernellimit or self.kernellimit == 0:  # kernel limit is already in mm
                d1 = round(distance, 4)
                if not d1 in self.DVK:
                    self.DVK[d1] = {}

                # avoid overwrite non simmetrycal voxels at exact same euclidean distance
                identifier = np.sum(np.abs(l))
                self.DVK[d1][identifier] = [values[0]*self.units['mGy_MBqs']*self.DVK_density,
                                            values[1]*self.units['mGy_MBqs']*self.DVK_density]
                if distance > topdist:
                    topdist = distance
                    self.topx = l[0]
                    self.topy = l[1]
                    self.topz = l[2]
        folders = vtkmrmlutils.getFolderIDs()
        for fID in folders:
            if not self.__convolute(fID, mode, threshold, multithreaded):
                break

    def Propagate(self, segmentationNode):
        ''' Propagates reference segmentation in all time points
          TODO: make all segmenttion not visible by default
        '''
        nodes = Node.getFolderChildren(self.referenceFolderID)
        if 'DERS' not in nodes:
            self.logger.error(
                'Densities not present, please create them before propagating segmentations')
            return

        folders = vtkmrmlutils.getFolderIDs()
        shNode = vtkmrmlutils.getSubjectHierarchyNode()

        oldsegmNode = segmentationNode.GetName()
        segnodeID = vtkmrmlutils.getItemID(segmentationNode)
        if not utils.checkTesting():
            segmentationNode.GetDisplayNode().SetVisibility(0)
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(folders))

        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            ProgressDialog.labelText = f"Processing {folderName}..."
            ProgressDialog.value = index
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()
            DERSNode = nodes['DERS']
            newName = DERSNode.name.replace('DERS', 'SEGM')
            if newName == oldsegmNode:  # This is already the reference, put it in the right place just in case
                vtkmrmlutils.setNodeFolder(segmentationNode, folderID)
                vtkmrmlutils.setItemName(segnodeID, newName)
                segmentationNode.SetAttribute(
                    Attributes().modality, 'RTSTRUCT')
                segmentationNode.SetAttribute(
                    Attributes().studyCreation, utils.getCurrentTime())
                segmentationNode.Modified()
                continue
            newsegmNode = slicer.util.getFirstNodeByClassByName(
                'vtkMRMLSegmentationNode', newName)
            while newsegmNode:
                slicer.mrmlScene.RemoveNode(newsegmNode)
                newsegmNode = slicer.util.getFirstNodeByClassByName(
                    'vtkMRMLSegmentationNode', newName)
            newsegmID = slicer.modules.subjecthierarchy.logic(
            ).CloneSubjectHierarchyItem(shNode, segnodeID, newName)
            newsegmNode = shNode.GetItemDataNode(newsegmID)
            if not utils.checkTesting():
                newsegmNode.CreateDefaultDisplayNodes()
                newsegmNode.CreateClosedSurfaceRepresentation()
                newsegmNode.GetDisplayNode().SetVisibility(0)
            newsegmNode.SetReferenceImageGeometryParameterFromVolumeNode(
                DERSNode.data)
            vtkmrmlutils.setNodeFolder(newsegmNode, folderID)
            newsegmNode.SetAttribute(Attributes().modality, 'RTSTRUCT')
            newsegmNode.SetAttribute(
                Attributes().studyCreation, utils.getCurrentTime())
            newsegmNode.Modified()

        ProgressDialog.close()

    def setupModeAlgo(self, mode, algo):
        # TODO remove this method with a better approach
        if 'dose' in mode.lower():
            if 'FFT' in algo:
                lalgo = 'FFT convolution (homogeneous)'
            elif 'homogeneous' in algo:
                lalgo = 'Convolution (homogeneous)'
            elif 'heterogeneous' in algo:
                lalgo = 'Convolution (heterogeneous)'
            elif 'monte' in algo.lower():
                lalgo = 'MonteCarlo'
            else:
                lalgo = 'LocalEnergyDepositionLED'
        else:
            lalgo = 'Activity'
        return lalgo

    def computeNewValues(self, mode, algorithm=''):
        '''
        update tables once images are normalized
        '''
        lalgo = self.setupModeAlgo(mode, algorithm)
        segmentationNodes = slicer.util.getNodesByClass(
            'vtkMRMLSegmentationNode')
        segmentationNodesNames = [segmentationNode.GetName()
                                  for segmentationNode in segmentationNodes]
        if len(segmentationNodesNames) == 0:
            slicer.util.errorDisplay(
                "No Segmentation found, please create one")
            self.logger.debug("No Segmentation found, please create one")
            return
        segmentationNode = segmentationNodes[0]
        folders = vtkmrmlutils.getFolderIDs()
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(folders))

        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            ProgressDialog.labelText = f"Processing {folderName}..."
            ProgressDialog.value = index
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            if 'SEGM' in nodes:
                segmNode = nodes['SEGM'].data
            else:
                segmNode = segmentationNode

            if 'dose' in mode.lower():
                masterNode = nodes['ADRM'][lalgo]
                newName = masterNode.name.replace(':ADRM', ':TABL').replace(
                    'LocalEnergyDepositionLED', 'DoseRateLED').replace('Convolution', 'DoseRateCONV').replace('fast convolution', 'DoseRateFFT')
            else:
                masterNode = nodes['ACTM']
                newName = masterNode.name.replace(
                    ':ACTM', ':TABL').replace('SPECT', 'Activity')
            vtkmrmlutils.setSlicerViews(nodes['DERS'].data.GetID(), masterNode.data.GetID())

            tmode = utils.extractMode(newName)
            if 'TABL' in nodes and tmode in nodes['TABL']:
                slicer.mrmlScene.RemoveNode(nodes['TABL'][tmode].data)

            resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(
                'vtkMRMLTableNode')
            resultsTableNode.SetName(newName)
            vtkmrmlutils.setNodeFolder(resultsTableNode, folderID)

            self.logger.info(
                f"Processing node {masterNode.name} with segmentation {segmNode.GetName()}")
            vtkmrmlutils.helperSegmentStatisticsTable(
                segmNode, masterNode.data, resultsTableNode)
            resultsTableNode.SetAttribute(
                Attributes().tableCreationMode, tmode)
            if not vtkmrmlutils.hasTable(resultsTableNode):
                raise errors.IOError(f"Failed to create Table {newName}")

            # Get the masses
            DERSNode = nodes['DERS']
            newName = DERSNode.name.replace(
                ':DERS', ':TABL').replace('SPECT', 'Densities')
            tmode = utils.extractMode(newName)
            if 'TABL' in nodes and tmode in nodes['TABL']:
                slicer.mrmlScene.RemoveNode(nodes['TABL'][tmode].data)

            resultsDTableNode = slicer.mrmlScene.AddNewNodeByClass(
                'vtkMRMLTableNode')
            resultsDTableNode.SetName(newName)
            vtkmrmlutils.setNodeFolder(resultsDTableNode, folderID)

            vtkmrmlutils.helperSegmentStatisticsTable(
                segmNode, DERSNode.data, resultsDTableNode)

            resultsDTableNode.SetAttribute(
                Attributes().tableCreationMode, tmode)

            if not vtkmrmlutils.hasTable(resultsDTableNode):
                raise errors.IOError(f"Failed to create Table {newName}")

        ProgressDialog.close()

    def processTimeIntegrationVariables(self, mode, algorithm=''):
        '''
        Creation of the data object to generate plots and integration in time
        '''
        lalgo = self.setupModeAlgo(mode, algorithm)
        if not mode in self.data:
            self.data[mode] = {}
            self.tableheader[mode] = {}
        self.tableheader[mode][lalgo] = []  # reinitialize the headers, always
        folders = vtkmrmlutils.getFolderIDs()
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(folders))

        l_dose = []
        l_density = []
        delay = []
        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            ProgressDialog.labelText = f"Processing {folderName}..."
            ProgressDialog.value = index
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()
            # Get time points in Hours
            timestamp = nodes['ACTM'].getTimeStamp()
            delay.append(float(timestamp))  # already in HR
            # Get segment mass in kg
            DERSNode = nodes['DERS']
            newName = DERSNode.name.replace(
                ':DERS', ':TABL').replace('SPECT', 'Densities')
            tmode = utils.extractMode(newName)
            tabledensitynode = vtkmrmlutils.getItemDataNode(nodes['TABL'][tmode].nodeID)
            densities_cols = []
            for row in range(tabledensitynode.GetNumberOfRows()):
                densities_cols.append(float(tabledensitynode.GetCellText(
                    row, 2)) * self.units['cm3'] * float(tabledensitynode.GetCellText(row, 3)))
            l_density.append(densities_cols)
            # Get the Absorbed Dose Rate (Activity) in Gy/s (MBq)
            if 'dose' in mode.lower():
                masterNode = nodes['ADRM'][lalgo]
                newName = masterNode.name.replace(':ADRM', ':TABL').replace(
                    'LocalEnergyDepositionLED', 'DoseRateLED').replace('Convolution', 'DoseRateCONV').replace('fast convolution', 'DoseRateFFT')
            else:
                masterNode = nodes['ACTM']
                newName = masterNode.name.replace(
                    ':ACTM', ':TABL').replace('SPECT', 'Activity')

            tmode = utils.extractMode(newName)
            resultsTableNode = nodes['TABL'][tmode]
            tableDOSENode = vtkmrmlutils.getItemDataNode(resultsTableNode.nodeID)

            self.tableheader[mode][lalgo].append(resultsTableNode.name)
            dose_cols = []
            for row in range(tableDOSENode.GetNumberOfRows()):
                if 'dose' in mode.lower():  # mean is what we want
                    dose_cols.append(
                        float(tableDOSENode.GetCellText(row, 3)))
                else:  # We need the total, not the mean
                    dose_cols.append(float(tableDOSENode.GetCellText(
                        row, 1)) * float(tableDOSENode.GetCellText(row, 3)))
            if lalgo == 'MonteCarlo':
                dose_cols = (np.array(dose_cols) /
                                self.units['MBq']).tolist()
            l_dose.append(dose_cols)
        ddose = np.array(l_dose)
        ddensity = np.array(l_density)
        delay = np.array(delay)
        ind = delay.argsort()  # Sort by time
        delay = delay[ind]
        ddose = ddose[ind]
        ddensity = ddensity[ind]
        ddosefit = np.copy(ddose)
        self.data[mode][lalgo] = np.vstack(
            [delay, ddensity.T, ddose.T, ddosefit.T])

        ProgressDialog.close()

    def showTableandPlotTimeIntegrationVariables(self, mode, algorithm='', showlegend=False):
        lalgo = self.setupModeAlgo(mode, algorithm)
        name = f'T:TABL {mode} {lalgo} summary'
        tableNode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLTableNode', name)
        if tableNode:
            slicer.mrmlScene.RemoveNode(tableNode)

        tableNode = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLTableNode', name)

        resultsTableNodeID = vtkmrmlutils.getResultIDs()[0]
        vtkmrmlutils.setNodeFolder(tableNode, resultsTableNodeID)

        ltnode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLTableNode', self.tableheader[mode][lalgo][0])
        lrows = ltnode.GetNumberOfRows()

        header_cells_mass = [f'M_{ltnode.GetCellText(i, 0)}'
                             for i in range(lrows)]
        if 'dose' in mode.lower():
            addedY = ' (mGy/h)'
            header_cells_doserate = [f'DR_{ltnode.GetCellText(i, 0)}'
                                     for i in range(lrows)]
            header_cells_doserate_fit = [f'DR_{ltnode.GetCellText(i, 0)}_fit'
                                         for i in range(lrows)]
        else:
            addedY = ' (MBq)'
            header_cells_doserate = [f'CA_{ltnode.GetCellText(i, 0)}'
                                     for i in range(lrows)]
            header_cells_doserate_fit = [f'CA_{ltnode.GetCellText(i, 0)}_fit'
                                         for i in range(lrows)]

        xname = "time (h)"
        columnNames = [xname] + header_cells_mass + \
            header_cells_doserate + header_cells_doserate_fit
        slicer.util.updateTableFromArray(
            tableNode, self.data[mode][lalgo].T, columnNames)
        tableNode.SetAttribute(
            Attributes().studyCreation, utils.getCurrentTime())
        tableNode.Modified()

        # Remove previous chart
        chartname = name.replace('TABL', 'CHRT')
        plotChartNode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLPlotChartNode', chartname)
        if plotChartNode:
            slicer.mrmlScene.RemoveNode(plotChartNode)
        # now create the new chart and the plots
        plotChartNode = slicer.mrmlScene.AddNewNodeByClass(
            "vtkMRMLPlotChartNode", chartname)
        plotChartNode.SetTitle(chartname)
        plotChartNode.SetXAxisTitle(xname)
        plotChartNode.SetYAxisTitle(mode + addedY)
        plotChartNode.SetYAxisLogScale(self.logyaxis)
        vtkmrmlutils.setNodeFolder(plotChartNode, resultsTableNodeID)
        Colors = {}

        for i, header in enumerate(header_cells_doserate):
            # Remove previous plots
            plotname = f"P:PLOT {lalgo} {header}"
            plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                "vtkMRMLPlotSeriesNode", plotname)
            while plotSeriesNode:
                slicer.mrmlScene.RemoveNode(plotSeriesNode)
                plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                    "vtkMRMLPlotSeriesNode", plotname)

            # Now create the plots
            plotSeriesNode = slicer.mrmlScene.AddNewNodeByClass(
                "vtkMRMLPlotSeriesNode", plotname)

            plotSeriesNode.SetAndObserveTableNodeID(tableNode.GetID())
            plotSeriesNode.SetXColumnName(xname)
            plotSeriesNode.SetYColumnName(header)
            plotSeriesNode.SetPlotType(
                slicer.vtkMRMLPlotSeriesNode.PlotTypeScatter)
            plotSeriesNode.SetLineStyle(
                slicer.vtkMRMLPlotSeriesNode.LineStyleNone)
            plotSeriesNode.SetMarkerStyle(
                slicer.vtkMRMLPlotSeriesNode.MarkerStyleCircle)
            plotSeriesNode.SetUniqueColor()
            Colors[i] = plotSeriesNode.GetColor()
            vtkmrmlutils.setNodeFolder(plotSeriesNode, resultsTableNodeID)
            # Add to chart
            plotChartNode.AddAndObservePlotSeriesNodeID(plotSeriesNode.GetID())

        for i, header in enumerate(header_cells_doserate_fit):
            # Remove previous plots
            plotname = f"P:PLOT {lalgo} {header}"
            plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                "vtkMRMLPlotSeriesNode", plotname)
            while plotSeriesNode:
                slicer.mrmlScene.RemoveNode(plotSeriesNode)
                plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                    "vtkMRMLPlotSeriesNode", plotname)

            # Now create the plots
            plotSeriesNode = slicer.mrmlScene.AddNewNodeByClass(
                "vtkMRMLPlotSeriesNode", plotname)
            vtkmrmlutils.setNodeFolder(plotSeriesNode, resultsTableNodeID)

            plotSeriesNode.SetAndObserveTableNodeID(tableNode.GetID())
            plotSeriesNode.SetXColumnName(xname)
            plotSeriesNode.SetYColumnName(header)
            plotSeriesNode.SetPlotType(
                slicer.vtkMRMLPlotSeriesNode.PlotTypeScatter)
            plotSeriesNode.SetLineStyle(
                slicer.vtkMRMLPlotSeriesNode.LineStyleSolid)
            plotSeriesNode.SetMarkerStyle(
                slicer.vtkMRMLPlotSeriesNode.MarkerStyleNone)
            plotSeriesNode.SetColor(Colors[i])
            # Add to chart
            plotChartNode.AddAndObservePlotSeriesNodeID(plotSeriesNode.GetID())

        plotChartNode.SetLegendVisibility(showlegend)
        plotChartNode.Modified()
        # Show chart in layout
        vtkmrmlutils.showChartinLayout(plotChartNode)
        # Refresh Results folder view
        vtkmrmlutils.RefreshFolderView(resultsTableNodeID)

    def integrate(self, mode, algorithm='', incorporationmode='linear', integrationmode='trapezoid'):
        from numpy import log, exp
        lalgo = self.setupModeAlgo(mode, algorithm)
        name = f'T:TABL {mode} {lalgo} summary'
        tableNode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLTableNode', name)
        if 'dose' in mode.lower():
            lunit = '(Gy)'
            ltext = 'Absorbed Dose'
        else:
            lunit = '(MBqh)'
            ltext = 'Cummulated Activity'
        x_values = self.data[mode][lalgo][0]
        ltnode = slicer.util.getFirstNodeByClassByName(
            'vtkMRMLTableNode', self.tableheader[mode][lalgo][0])
        lrows = ltnode.GetNumberOfRows()
        header_cells_mass = [f'M_{ltnode.GetCellText(i, 0)}'
                             for i in range(lrows)]
        if 'dose' in mode.lower():
            header_cells_doserate = [f'DR_{ltnode.GetCellText(i, 0)}'
                                     for i in range(lrows)]
            header_cells_doserate_fit = [f'DR_{ltnode.GetCellText(i, 0)}_fit'
                                         for i in range(lrows)]
            dose_values = self.data[mode][lalgo][len(header_cells_mass)+1:]
        else:
            header_cells_doserate = [f'CA_{ltnode.GetCellText(i, 0)}'
                                     for i in range(lrows)]
            header_cells_doserate_fit = [f'CA_{ltnode.GetCellText(i, 0)}_fit'
                                         for i in range(lrows)]
            dose_values = self.data[mode][lalgo][len(header_cells_mass)+1:]

        mass_values = self.data[mode][lalgo][1:len(header_cells_mass)+1]

        TableNodes = slicer.util.getNodesByClass('vtkMRMLTableNode')
        name = f'T:TABL {mode} {lalgo} integrated'
        for node in TableNodes:
            if name == node.GetName():  # Table exists, erase it
                slicer.mrmlScene.RemoveNode(node)

        # prepare clean table
        resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLTableNode')
        resultsTableNode.RemoveAllColumns()
        resultsTableNodeID = vtkmrmlutils.getResultIDs()[0]
        vtkmrmlutils.setNodeFolder(resultsTableNode, resultsTableNodeID)

        resultsTableNode.SetName(name)
        table = resultsTableNode.GetTable()

        segmentColumnValue = vtk.vtkStringArray()
        segmentColumnValue.SetName("Segment")
        table.AddColumn(segmentColumnValue)

        segmentColumnValue = vtk.vtkStringArray()
        segmentColumnValue.SetName("Mass (kg)")
        table.AddColumn(segmentColumnValue)

        trapintColumnValue = vtk.vtkStringArray()
        trapintColumnValue.SetName(f'Data points integral {lunit}')
        table.AddColumn(trapintColumnValue)

        TotalColumnValue = vtk.vtkStringArray()
        TotalColumnValue.SetName(f'{ltext} {lunit}')
        table.AddColumn(TotalColumnValue)

        if not 'dose' in mode.lower():
            doseColumnValue = vtk.vtkStringArray()
            doseColumnValue.SetName('Absorbed Dose by LED (Gy)')
            table.AddColumn(doseColumnValue)

        differenceColumnValue = vtk.vtkStringArray()
        differenceColumnValue.SetName('STDDEV (Gy)')
        table.AddColumn(differenceColumnValue)

        FitColumnValue = vtk.vtkStringArray()
        FitColumnValue.SetName('Fit function')
        table.AddColumn(FitColumnValue)

        table.SetNumberOfRows(len(header_cells_doserate))
        xeff = x_values  # this in hours for fitting
        if xeff[0] <= 0:
            zeroincluded = True
        else:
            zeroincluded = False
            xeff = np.insert(xeff, 0, 0)
        xeff = np.append(xeff, x_values[-1]*10)

        resultsTableNode.SetAttribute("PKincorporation", incorporationmode)
        resultsTableNode.SetAttribute("PKalgorithm", integrationmode)
        resultsTableNode.SetAttribute("ADalgorithm", lalgo)

        T_h = self.Isotope['T_h']  # in hours
        self.logger.info(f'T1/2 = {T_h:.3e}h')
        lamda = np.log(2) / T_h
        for i, (mass, dose) in enumerate(zip(mass_values, dose_values)):
            yeff = dose
            self.logger.info(f"Processing {header_cells_doserate[i]}")
            if not zeroincluded:
                if x_values[0] > 0:
                    if 'linear' in incorporationmode:  # incorporation part
                        yeff = np.insert(yeff, 0, 0)
                    elif 'const' in incorporationmode:
                        yeff = np.insert(yeff, 0, yeff[0])
                    elif 'exp' in incorporationmode:
                        yeff = np.insert(yeff, 0, yeff[0]*exp(lamda*xeff[1]))

            yeff = np.append(yeff, 0)
            fitrow = 2*lrows + i + 1
            amass = np.average(mass)

            fit = fit_values.FitValues(
                x=xeff, y=yeff, fit_type=integrationmode.lower())
            data_integral, data_integral_error, total_integral, dy, lamda, fit_function = fit.integrate(
                T_h=T_h)
            newy=[]
            c = 0
            for lx, ly in zip(xeff, yeff):
                if utils.isNumber(lx) and utils.isNumber(ly):
                    newy.append(dy[c])
                    c+=1
                else:
                    newy.append(np.nan)
            xeff = np.ma.array(xeff, mask=np.isnan(xeff))
            yeff = np.ma.array(yeff, mask=np.isnan(yeff))
            newy = np.ma.array(newy, mask=np.isnan(newy))
            if zeroincluded:
                self.data[mode][lalgo][fitrow] = newy[:-1]
            else:
                self.data[mode][lalgo][fitrow] = newy[1:-1]
            # To display the equations change to info
            self.logger.info(f'Fit function: {fit_function}')

            error = np.sqrt(np.std(newy[1:-1]-yeff[1:-1])* \
                       (xeff[-2]-xeff[1])**2 + data_integral_error**2)
            table.SetValue(i, 0, header_cells_doserate[i])
            table.SetValue(i, 1, f'{amass:.3e}')
            if 'dose' in mode.lower():  # units mGy/h * h -> mGy
                # output is in Gy
                table.SetValue(
                    i, 2, f"{data_integral * self.units['mGy']:.3e}")
                table.SetValue(
                    i, 3, f"{total_integral * self.units['mGy']:.3e}")
                table.SetValue(i, 4, f"{error * self.units['mGy']:.3e}")
                table.SetValue(i, 5, fit_function)
                self.logger.info(f"AD to {header_cells_doserate[i]} = ({total_integral * self.units['mGy']:.3e} +/- {error * self.units['mGy']:.3e}) Gy")
            else:  # units MBq * h -> MBqh
                table.SetValue(i, 2, f'{data_integral:.3e}')  # output in MBqh
                table.SetValue(i, 3, f'{total_integral:.3e}')
                ldose = total_integral * \
                    self.units['MBqh'] * self.Isotope['energy'] / \
                    amass  # output is in Gy
                table.SetValue(i, 4, f'{ldose:.3e}')
                table.SetValue(i, 5, f'{error * ldose / total_integral:.3e}')
                table.SetValue(i, 6, fit_function)
                self.logger.info(f"AD to {header_cells_doserate[i]} = ({ldose:.3e} +/- {error * ldose / total_integral:.3e}) Gy")

        xname = "time (h)"
        columnNames = [xname] + header_cells_mass + \
            header_cells_doserate + header_cells_doserate_fit
        slicer.util.updateTableFromArray(
            tableNode, self.data[mode][lalgo].T, columnNames)

        resultsTableNode.Modified()
        # Show the Table
        vtkmrmlutils.showTable(resultsTableNode)

        # Refresh Results folder view
        vtkmrmlutils.RefreshFolderView(resultsTableNodeID)
