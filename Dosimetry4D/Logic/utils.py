from datetime import datetime
from pathlib import Path
import sys
import re
import numpy as np
import slicer

from Logic import errors

# attribute values for creating new test nodes
def whoami():
    return sys._getframe(1).f_code.co_name

def checkTesting():
    try:
        mainWindow = slicer.util.mainWindow()
        if mainWindow is not None:
            layoutManager = slicer.app.layoutManager()
            if layoutManager is not None:
                return False
        return True
    except:
        return True

def getScriptPath():
    try:
        script_path = Path(__file__).resolve().parents[1]
    except:
        from inspect import getsourcefile
        script_path = Path(getsourcefile(lambda: 0)).resolve().parents[1]
    finally:
        return script_path


def getCurrentTime():
    return str(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))


def getSystemCoresInfo():
    if sys.platform.startswith('win'):
        from multiprocessing import cpu_count
        cpu_cores = cpu_count()
        cpu_processors = cpu_cores
        lsystem = 'windows'
    else:
        try:
            from cpu_cores import CPUCoresCounter
        except:
            from slicer.util import pip_install
            pip_install('cpu_cores')
            from cpu_cores import CPUCoresCounter

        fac = CPUCoresCounter.factory()
        cpu_cores = fac.get_physical_cores_count()
        cpu_processors = fac.get_physical_processors_count()
        lsystem = 'posix'

    return cpu_cores, cpu_processors, lsystem


def extractMode(name):
    return ' '.join(name.split(':')[1].split()[1:-1])


def get_valid_filename(s):
    """
    Return the given string converted to a string that can be used for a clean
    filename. Remove leading and trailing spaces; convert other spaces to
    underscores; and remove anything that is not an alphanumeric, dash,
    underscore, or dot.
    >>> get_valid_filename("john's portrait in 2004.jpg")
    'johns_portrait_in_2004.jpg'
    """
    s = str(s).strip().replace(' ', '_')
    return re.sub(r'(?u)[^-\w.]', '', s)


def Center_of_Mass(Matrix, spacing):
    ''' Calculates the center of mass of a 3D matrix
    '''
    # Shape
    shape = np.shape(Matrix)
    # Axis
    x = np.arange(0, shape[0])*spacing[0]
    y = np.arange(0, shape[1])*spacing[1]
    z = np.arange(0, shape[2])*spacing[2]
    # Mass along axis
    mx = np.sum(Matrix, axis=(1, 2))
    my = np.sum(Matrix, axis=(0, 2))
    mz = np.sum(Matrix, axis=(0, 1))
    # center of Masses
    comx = np.sum(x*mx)/np.sum(mx)
    comy = np.sum(y*my)/np.sum(my)
    comz = np.sum(z*mz)/np.sum(mz)
    return np.array([comx, comy, comz])

def checkRequirements():
    # Install all requirements
    import importlib
    from slicer.util import pip_install
    # pip_install('pip -U')
    spectList = ['pyxb', 'scipy', 'pydicom', 'sentry_sdk', 'lxml', 'xmltodict',
                'xmlschema', 'simpleeval', 'uncertainties', 'lmfit']

    for spec in spectList:
        moduleLoader = importlib.util.find_spec(spec)
        if not moduleLoader:
            pip_install(f"{spec} -U")


def installModule(module="SlicerElastix"):
    emm = slicer.app.extensionsManagerModel()
    if emm.isExtensionInstalled(module):
        print(f"Detected {module}")
        return

    md = emm.retrieveExtensionMetadataByName(module)
    if not md or 'extension_id' not in md:
        raise errors.IOError(f"{module} is not installed properly")

    if emm.downloadAndInstallExtension(md['extension_id']):
        slicer.app.confirmRestart(f"Restart to complete {module} installation")
    else:
        raise errors.IOError(f"{module} was not installed properly")

def isNumber(x):
    try:
        if float(x)==0.0:
            return True
        lx = float(x) / float(x)  # 0 would have failed this
        if lx==1.0:
            return True
        else:
            return False
    except:
        return False
