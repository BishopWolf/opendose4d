from Logic.singleton import Singleton
import Logic.dbutils as dbutils

class Constants(Singleton):

    def __init__(self):

        self.elements = {  # one*, two, three and all characters identifying the element
            'I': ['i', 'io', 'iod', 'iodine'],
            'Lu': ['lu', 'lut', 'luthetium'],
            'Y': ['y', 'yt', 'ytt', 'yttrium'],
            'Tb': ['tb', 'ter', 'terbium']
        }

        self.isotopes = {
            'I-131': {'Z':53 , 'A':131},
            'Lu-177': {'Z':71 , 'A':177},
            'Y-90': {'Z':39 , 'A':90},
            'Tb-161': {'Z':65 , 'A':161},
            'F-18': {'Z':9 , 'A':18}
        }

        for isotope in self.isotopes:
            newdata = dbutils.setupIsotope(isotope)
            for data in newdata:
                self.isotopes[isotope][data] = newdata[data]
