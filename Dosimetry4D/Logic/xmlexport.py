import importlib
import shutil
from pathlib import Path

import pyxb
import pyxb.utils.domutils
import pyxb.binding.datatypes as xs

import vtk
import slicer
from lxml import etree

import Logic.nonDicomFileSetDescriptor as nonDicomFileSetDescriptor
import Logic.utils as utils
import Logic.vtkmrmlutils as vtkmrmlutils
from Logic.nodes import Node
from Logic.attributes import Attributes
from Logic.xmlconstants import XMLConstants

pyxb.utils.domutils.BindingDOMSupport.Reset()
namespace = pyxb.namespace.NamespaceForURI(
    'https://www.irdbb-medirad.com', create_if_missing=True)
namespace.configureCategories(['typeBinding', 'elementBinding'])
pyxb.utils.domutils.BindingDOMSupport.DeclareNamespace(namespace, 'irdbb')

mod = importlib.import_module('Logic', 'nonDicomFileSetDescriptor')
importlib.reload(mod)


class DescriptorWrapper(nonDicomFileSetDescriptor.NonDicomFileSetDescriptor_):
    pass


nonDicomFileSetDescriptor.NonDicomFileSetDescriptor_._SetSupersedingClass(
    DescriptorWrapper)

class XML_MEDIRAD():

    def __init__(self, referenceFolderID, outputFolder, ClinicalStudyTitle, ClinicalStudyID):
        self.referenceFolderID = referenceFolderID
        self.outputFolder = outputFolder
        self.ClinicalStudyTitle = ClinicalStudyTitle
        self.ClinicalStudyID = ClinicalStudyID
        self.patientFolder = ''
        self.studyFolder = ''
        self.dataFolder = ''

        self.organs = []
        for organ in nonDicomFileSetDescriptor.OrganOrTissue.values():
            self.organs.append(str(organ).lower())

        self.folders = vtkmrmlutils.getFolderIDs()
        self.nodes = Node.getFolderChildren(referenceFolderID)
        self.folderNames = vtkmrmlutils.getFolderNames()

    def __getSchemasVersion(self):
        xsdPath = utils.getScriptPath() / "Resources" / "XSD" / \
            "NonDicomFileSetDescriptor.xsd"
        with xsdPath.open("r") as f:
            lines = f.read().splitlines()
            for line in lines:
                if "VersionSchemas" in line:
                    return str(line[4:-3])
        return "VersionSchemas : unknown"

    def __getAdministeredActivity(self, value):
        AdministeredActivity = nonDicomFileSetDescriptor.AdministeredActivity()
        AdministeredActivity.AdministeredActivityValue = xs.float(value)
        AdministeredActivity.ActivityUnit = xs.string("megabecquerel")
        return AdministeredActivity

    def __getAcquisitionSettings(self):
        AcquisitionSettings = nonDicomFileSetDescriptor.AcquisitionSettings()
        AcquisitionSettings.Siteadministeringthetreatment = self.center
        AcquisitionSettings.Dateandtimeofinjection = self.nodes["ACSC"].data.GetAttribute(
            Attributes().injectionTime)
        AcquisitionSettings.PreAdministeredActivity = self.__getAdministeredActivity(
            self.nodes["ACTM"].data.GetAttribute(Attributes().injectedActivity))
        AcquisitionSettings.PostAdministeredActivity = self.__getAdministeredActivity(
            0)
        # TODO add more definitions in XSD
        AcquisitionSettings.Radiopharmaceutical = "sodiumIodideI131"
        AcquisitionSettings.Isotope = XMLConstants(
        ).IsotopesCategoryDict[self.nodes["ACTM"].data.GetAttribute(Attributes().isotope)]

        return AcquisitionSettings

    def __getAcquisitionSettingsCalibration(self, tankNode):
        AcquisitionSettings = nonDicomFileSetDescriptor.AcquisitionSettings()
        AcquisitionSettings.Siteadministeringthetreatment = self.center
        AcquisitionSettings.Dateandtimeofinjection = tankNode.data.GetAttribute(
            Attributes().injectionTime)
        AcquisitionSettings.PreAdministeredActivity = self.__getAdministeredActivity(
            tankNode.data.GetAttribute(Attributes().injectedActivity))
        AcquisitionSettings.PostAdministeredActivity = self.__getAdministeredActivity(
            0)
        # TODO add more definitions in XSD
        AcquisitionSettings.Radiopharmaceutical = "sodiumIodideI131"
        AcquisitionSettings.Isotope = XMLConstants(
        ).IsotopesCategoryDict[tankNode.data.GetAttribute(Attributes().isotope)]

        return AcquisitionSettings

    def __prepareDirectory(self, study):
        if "CTCT" in self.nodes:
            patientName = self.nodes["CTCT"].data.GetAttribute(
                Attributes().patientName)
        elif "ACSC" in self.nodes:
            patientName = self.nodes["ACSC"].data.GetAttribute(
                Attributes().patientName)
        else:
            raise IOError(
                "Reference node does not have either CT or SPECT acquisition")

        patientName = utils.get_valid_filename(patientName)
        try:
            self.center = XMLConstants(
            ).ClinicalCenters[patientName.split('-')[2]]
        except:
            self.center = 'CRCT'

        self.patientFolder = Path(self.outputFolder) / patientName
        if not self.patientFolder.exists():
            self.patientFolder.mkdir()

        self.studyFolder = self.patientFolder / study
        if self.studyFolder.exists():
            shutil.rmtree(self.studyFolder)
        self.studyFolder.mkdir()

        self.dataFolder = self.studyFolder / "Data"
        if self.dataFolder.exists():
            shutil.rmtree(self.dataFolder)
        self.dataFolder.mkdir()

    def __printOutput(self, xmlSchema, elementName):
        root = etree.fromstring(xmlSchema.toxml(element_name=elementName))
        comment = etree.Comment(self.__getSchemasVersion())
        root.insert(0, comment)
        return etree.tostring(root, encoding='UTF-8', xml_declaration=True, pretty_print=True).decode()

    def __writeOutput(self, xmlSchema, fileName):
        root = etree.fromstring(xmlSchema.toxml())
        comment = etree.Comment(self.__getSchemasVersion())
        root.insert(0, comment)
        XMLString = etree.tostring(
            root, encoding='UTF-8', xml_declaration=True, pretty_print=True).decode()
        with fileName.open('w') as f:
            f.write(XMLString)

    def __getGeometricalTransformation(self, node, nodeName):
        GeometricalTransformation = nonDicomFileSetDescriptor.GeometricalTransformation()
        GeometricalTransformation.GeometricalTransformationValue = nonDicomFileSetDescriptor.NonDICOMDataContainer()
        nonDICOMData = self.__getNonDICOMData(node, nodeName, 'TRNF')
        GeometricalTransformation.GeometricalTransformationValue.NonDICOMData.append(
            nonDICOMData)
        try:
            GeometricalTransformation.TransformationType = XMLConstants(
            ).TransformationType[node.GetAttribute(Attributes().registrationMode)]
        except:
            GeometricalTransformation.TransformationType = XMLConstants(
            ).TransformationType['useGeometryAlign']
        GeometricalTransformation.TransformationIdentifier = str.replace(
            nodeName, ":", "")

        fixedID = int(node.GetAttribute(Attributes().registrationReference))
        fixedName = vtkmrmlutils.getItemName(fixedID)
        fixedNode = vtkmrmlutils.getItemDataNode(fixedID)
        if 'CTCT' in fixedName or 'CTRS' in fixedName:
            GeometricalTransformation.DICOMCTDestinationCoordinateSpaceUsed = nonDicomFileSetDescriptor.DICOMDataContainer()
            GeometricalTransformation.DICOMCTDestinationCoordinateSpaceUsed.DICOMData.append(
                self.__getDICOMData(fixedNode))
        elif 'ACSC' in fixedName:
            GeometricalTransformation.DICOMSPECTDestinationCoordinateSpaceUsed = nonDicomFileSetDescriptor.DICOMDataContainer()
            GeometricalTransformation.DICOMSPECTDestinationCoordinateSpaceUsed.DICOMData.append(
                self.__getDICOMData(fixedNode))
        else:
            GeometricalTransformation.NonDICOMDestinationCoordinateSpace = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            GeometricalTransformation.NonDICOMDestinationCoordinateSpace.NonDICOMData.append(
                self.__getNonDICOMData(fixedNode, fixedName, 'DERS'))

        movingID = int(node.GetAttribute(Attributes().registrationVolume))
        movingName = vtkmrmlutils.getItemName(movingID)
        movingNode = vtkmrmlutils.getItemDataNode(movingID)
        if 'CTCT' in movingName or 'CTRS' in movingName:
            GeometricalTransformation.DICOMCTDestinationCoordinateSpaceUsed = nonDicomFileSetDescriptor.DICOMDataContainer()
            GeometricalTransformation.DICOMCTDestinationCoordinateSpaceUsed.DICOMData.append(
                self.__getDICOMData(movingNode))
        elif 'ACSC' in movingName:
            GeometricalTransformation.DICOMSPECTDestinationCoordinateSpaceUsed = nonDicomFileSetDescriptor.DICOMDataContainer()
            GeometricalTransformation.DICOMSPECTDestinationCoordinateSpaceUsed.DICOMData.append(
                self.__getDICOMData(movingNode))
        else:
            GeometricalTransformation.NonDICOMDestinationCoordinateSpace = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            GeometricalTransformation.NonDICOMDestinationCoordinateSpace.NonDICOMData.append(
                self.__getNonDICOMData(movingNode, movingName, 'ACTM'))

        return GeometricalTransformation

    def __getNonDICOMData(self, node, nodeName, nodeClass):
        if node is None:
            raise IOError(f'{nodeName} is not valid node')

        nonDICOMData = nonDicomFileSetDescriptor.NonDICOMData()
        fileHeaderName = str.replace(nodeName, ":", "")
        if nodeClass == 'TRNF':
            nonDICOMData.NonDICOMDataFormat = "HDF5 format"
            nonDICOMData.NonDICOMDataFileName = f'Data/{fileHeaderName}.h5'
            try:
                nonDICOMData.NonDICOMDataClass = XMLConstants(
                ).TransformationType[node.GetAttribute(Attributes().registrationMode)]
            except:
                nonDICOMData.NonDICOMDataClass = XMLConstants(
                ).TransformationType['useGeometryAlign']
        else:
            nonDICOMData.NonDICOMDataFormat = "NRRD format"
            nonDICOMData.NonDICOMDataFileName = f'Data/{fileHeaderName}.nrrd'
            nonDICOMData.NonDICOMDataClass = XMLConstants(
            ).NonDICOMClass[nodeClass]

        transfornationNode = node.GetParentTransformNode()
        transformationID = vtkmrmlutils.getItemID(transfornationNode)
        if transformationID > 0:
            nonDICOMData.TransformationsUsed = str.replace(
                vtkmrmlutils.getItemName(transformationID), ":", "")

        fileName = self.studyFolder / nonDICOMData.NonDICOMDataFileName
        if not fileName.exists():
            vtkmrmlutils.saveNode(node, fileName)

        return nonDICOMData

    def __getDICOMData(self, node):
        DICOMData = nonDicomFileSetDescriptor.DICOMData()
        DICOMData.DICOMStudyUID = node.GetAttribute(
            Attributes().studyInstanceUID)
        DICOMData.DICOMSeriesUID = node.GetAttribute(
            Attributes().seriesInstanceUID)
        return DICOMData

    def __getProcessExecutionContext(self, studyCreation):
        ProcessExecutionContext = nonDicomFileSetDescriptor.ProcessExecutionContext()
        ProcessExecutionContext.DateTimeProcessStarted = studyCreation
        ProcessExecutionContext.PerformingInstitution = "CRCT"
        return ProcessExecutionContext

    def __getNMRelevantCalibrationReference(self, node):
        NMRelevantCalibrationReference = nonDicomFileSetDescriptor.NMRelevantCalibrationReference()
        NMRelevantCalibrationReference.ReferenceCalibrationDate = node.GetAttribute(
            Attributes().acquisition)
        NMRelevantCalibrationReference.Isotope = XMLConstants(
        ).IsotopesCategoryDict[node.GetAttribute(Attributes().isotope)]
        return NMRelevantCalibrationReference

    def __getSPECTAcqCTAcqAndReconstruction(self, folderName):

        SPECTAcqCTAcqAndReconstruction = nonDicomFileSetDescriptor.SPECTAcqCTAcqAndReconstruction()
        if "CTCT" in self.nodes:
            SPECTAcqCTAcqAndReconstruction.CTReconProduced = self.__getDICOMData(
                self.nodes["CTCT"].data)
            SPECTAcqCTAcqAndReconstruction.CTRelevantCalibrationReference = nonDicomFileSetDescriptor.CTRelevantCalibrationReference()
            SPECTAcqCTAcqAndReconstruction.CTRelevantCalibrationReference.ReferenceCalibrationDate = self.nodes["CTCT"].data.GetAttribute(
                Attributes().acquisition)
        elif "CTRS" in self.nodes:
            SPECTAcqCTAcqAndReconstruction.CTReconProduced = self.__getDICOMData(
                self.nodes["CTRS"].data)
            SPECTAcqCTAcqAndReconstruction.CTRelevantCalibrationReference = nonDicomFileSetDescriptor.CTRelevantCalibrationReference()
            SPECTAcqCTAcqAndReconstruction.CTRelevantCalibrationReference.ReferenceCalibrationDate = self.nodes["CTRS"].data.GetAttribute(
                Attributes().acquisition)
        else:
            SPECTAcqCTAcqAndReconstruction.CTReconProduced = nonDicomFileSetDescriptor.DICOMData(
                "", "")

        if "ACSC" in self.nodes:
            SPECTAcqCTAcqAndReconstruction.NMTomoProduced = self.__getDICOMData(
                self.nodes["ACSC"].data)
            SPECTAcqCTAcqAndReconstruction.NMRelevantCalibrationReference = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
        else:
            SPECTAcqCTAcqAndReconstruction.NMTomoProduced = nonDicomFileSetDescriptor.DICOMData(
                "", "")

        TPDE = nonDicomFileSetDescriptor.TimePointDescriptionElement()
        if folderName in XMLConstants().TimePointCategoryDict.keys():
            TPDE.TimePointCategory = XMLConstants(
            ).TimePointCategoryDict[folderName]
        elif abs(6-int(folderName[:-2])) <= 2:
            TPDE.TimePointCategory = XMLConstants(
            ).TimePointCategoryDict['6HR']
        else:
            TPDE.TimePointCategory = "additional timepoint"
        TPDE.TimeUnit = "hours"
        TPDE.TimePointDistanceFromReferenceEventValue = xs.integer(
            float(self.nodes["ACSC"].data.GetAttribute(Attributes().timeStamp)))
        TPDE.TimePointIdentifier = folderName
        SPECTAcqCTAcqAndReconstruction.TimePointDescription = nonDicomFileSetDescriptor.TimePointDescription()
        SPECTAcqCTAcqAndReconstruction.TimePointDescription.TimePointDescriptionElement.append(
            TPDE)

        return SPECTAcqCTAcqAndReconstruction

    def fillACTM_XMLfile(self):

        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(self.folders))

        self.__prepareDirectory('ActivityIntegration')
        tableNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLTableNode')
        nameActivity = 'T:TABL Activity Activity summary'
        nameDose = 'T:TABL Activity Activity integrated'
        activityTableB = False
        doseTableB = False
        for node in tableNodes:
            if nameActivity == node.GetName():
                activityTable = node.GetTable()
                activityTableNode = node
                activityTableB = True
            elif nameDose == node.GetName():
                doseTable = node.GetTable()
                doseTableNode = node
                doseTableB = True

        if not(activityTableB and doseTableB):
            raise IOError('Absorbed Dose table not yet created')

        activityTableArray = vtkmrmlutils.GetTableAsArray(activityTableNode)
        doseTableArray = vtkmrmlutils.GetTableAsArray(doseTableNode)

        u3DSlide1 = nonDicomFileSetDescriptor.NonDicomFileSetDescriptor()

        u3DSlide1.ReferencedClinicalResearchStudy = nonDicomFileSetDescriptor.ReferencedClinicalResearchStudy()
        u3DSlide1.ReferencedClinicalResearchStudy.ClinicalResearchStudyID = self.ClinicalStudyID
        u3DSlide1.ReferencedClinicalResearchStudy.ClinicalResearchStudyTitle = self.ClinicalStudyTitle

        if "CTCT" in self.nodes:
            u3DSlide1.PatientId = self.nodes["CTCT"].data.GetAttribute(
                Attributes().patientID)
        elif "ACSC" in self.nodes:
            u3DSlide1.PatientId = self.nodes["ACSC"].data.GetAttribute(
                Attributes().patientID)
        else:
            raise IOError(
                "Reference node does not have either CT or SPECT acquisition")

        u3DSlide1.AcquisitionSettings = self.__getAcquisitionSettings()

        if "SEGM" not in self.nodes:
            SegmentationNodes = slicer.mrmlScene.GetNodesByClass(
                'vtkMRMLSegmentationNode')
            for segmentation in SegmentationNodes:
                referenceSegmentationNode = segmentation
                break

            if not 'referenceSegmentationNode' in locals():
                raise IOError("No segmentations found")

        u3DSlide1.ThreeDimDosimetrySlide1workflow = nonDicomFileSetDescriptor.ThreeDimDosimetrySlide1workflow()
        u3DSlide1.ThreeDimDosimetrySlide1workflow.SPECTDataAcquisitionAndReconstruction = nonDicomFileSetDescriptor.SPECTDataAcquisitionAndReconstruction()
        u3DSlide1.ThreeDimDosimetrySlide1workflow.SPECTDataAcquisitionAndReconstruction.SPECTAcqCTAcqAndReconstructionContainer = nonDicomFileSetDescriptor.SPECTAcqCTAcqAndReconstructionContainer()
        u3DSlide1.ThreeDimDosimetrySlide1workflow.RegistrationVOISegmentationAndPropagationContainer = nonDicomFileSetDescriptor.RegistrationVOISegmentationAndPropagationContainer()
        u3DSlide1.ThreeDimDosimetrySlide1workflow.VOIActivityDeterminationContainer = nonDicomFileSetDescriptor.VOIActivityDeterminationContainer()
        u3DSlide1.ThreeDimDosimetrySlide1workflow.TimeActivityCurveFitIn3DDosimetryContainer = nonDicomFileSetDescriptor.TimeActivityCurveFitIn3DDosimetryContainer()
        u3DSlide1.ThreeDimDosimetrySlide1workflow.AbsorbedDoseCalculationInVOI = nonDicomFileSetDescriptor.AbsorbedDoseCalculationInVOI()

        u3DSlide1.ThreeDimDosimetrySlide1workflow.AbsorbedDoseCalculationInVOI.ProcessExecutionContext = self.__getProcessExecutionContext(
            activityTableNode.GetAttribute(Attributes().studyCreation))
        u3DSlide1.ThreeDimDosimetrySlide1workflow.AbsorbedDoseCalculationInVOI.AbsorbedDoseCalculationMethodUsed = "Local Energy Deposition"
        u3DSlide1.ThreeDimDosimetrySlide1workflow.AbsorbedDoseCalculationInVOI.AbsorbedDoseInVOIContainer = nonDicomFileSetDescriptor.AbsorbedDoseInVOIContainer()

        for folderIndex, folderID in enumerate(self.folders):
            folderName = vtkmrmlutils.getItemName(folderID)

            ProgressDialog.labelText = f"Processing {folderName}..."
            ProgressDialog.value = folderIndex
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            self.nodes = Node.getFolderChildren(folderID)
            SPECTAcqCTAcqAndReconstruction = self.__getSPECTAcqCTAcqAndReconstruction(
                folderName)
            u3DSlide1.ThreeDimDosimetrySlide1workflow.SPECTDataAcquisitionAndReconstruction.SPECTAcqCTAcqAndReconstructionContainer.SPECTAcqCTAcqAndReconstruction.append(
                SPECTAcqCTAcqAndReconstruction)

            RegistrationVOISegmentationAndPropagation = nonDicomFileSetDescriptor.RegistrationVOISegmentationAndPropagation()
            RegistrationVOISegmentationAndPropagation.TimePointIdentifierUsed = folderName
            RegistrationVOISegmentationAndPropagation.ImageProcessingMethodMethodUsed = "Semi-automatic segmentation"
            RegistrationVOISegmentationAndPropagation.Segmentation = nonDicomFileSetDescriptor.Segmentation()
            RegistrationVOISegmentationAndPropagation.CTReconUsed = nonDicomFileSetDescriptor.DICOMDataContainer()
            RegistrationVOISegmentationAndPropagation.CTReconUsed.DICOMData.append(
                SPECTAcqCTAcqAndReconstruction.CTReconProduced)
            RegistrationVOISegmentationAndPropagation.NMTomoReconUsed = nonDicomFileSetDescriptor.DICOMDataContainer()
            RegistrationVOISegmentationAndPropagation.NMTomoReconUsed.DICOMData.append(
                SPECTAcqCTAcqAndReconstruction.NMTomoProduced)
            RegistrationVOISegmentationAndPropagation.DensityImageProduced = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            if "DERS" in self.nodes:
                nonDICOMData = self.__getNonDICOMData(
                    self.nodes["DERS"].data, self.nodes["DERS"].name, 'DERS')
            else:
                nonDICOMData = nonDicomFileSetDescriptor.NonDICOMData()
            RegistrationVOISegmentationAndPropagation.DensityImageProduced.NonDICOMData.append(
                nonDICOMData)

            if "TRNF" in self.nodes:
                RegistrationVOISegmentationAndPropagation.GeometricalTransformationContainer = nonDicomFileSetDescriptor.GeometricalTransformationContainer()
                GeometricalTransformation = self.__getGeometricalTransformation(
                    self.nodes["TRNF"].data, self.nodes["TRNF"].name)
                RegistrationVOISegmentationAndPropagation.GeometricalTransformationContainer.GeometricalTransformation.append(
                    GeometricalTransformation)

            activityRow = activityTableArray['time (h)'].index(
                float(self.nodes["ACTM"].data.GetAttribute(Attributes().timeStamp)))

            if "SEGM" in self.nodes:
                segmentationNode = self.nodes["SEGM"].data
                segmentationName = self.nodes["SEGM"].name
            else:
                segmentationNode = referenceSegmentationNode
                segmentationName = segmentationNode.GetName()

            fileHeaderName = str.replace(segmentationName, ":", "")
            visibleSegmentIDs = vtk.vtkStringArray()
            segmentationNode.GetDisplayNode().SetAllSegmentsVisibility(True)
            segmentationNode.GetDisplayNode().GetVisibleSegmentIDs(visibleSegmentIDs)

            segmentationCreation = segmentationNode.GetAttribute(
                Attributes().studyCreation)
            if not segmentationCreation:
                segmentationCreation = utils.getCurrentTime()
            RegistrationVOISegmentationAndPropagation.ProcessExecutionContext = self.__getProcessExecutionContext(
                segmentationCreation)
            RegistrationVOISegmentationAndPropagation.Segmentation.SegmentationIdentifier = fileHeaderName
            RegistrationVOISegmentationAndPropagation.Segmentation.VOIContainer = nonDicomFileSetDescriptor.VoiProducedContainer()

            VOIActivityDetermination = nonDicomFileSetDescriptor.VOIActivityDetermination()
            VOIActivityDetermination.TimePointIdentifierUsed = folderName
            VOIActivityDetermination.SegmentationIdentifierUsed = fileHeaderName
            VOIActivityDetermination.ProcessExecutionContext = self.__getProcessExecutionContext(
                self.nodes["ACTM"].data.GetAttribute(Attributes().studyCreation))
            VOIActivityDetermination.SPECTCalibrationFactorReferenceUsed = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
            VOIActivityDetermination.SPECTRecoveryCoefficientCurveReferenceUsed = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
            VOIActivityDetermination.VoxelActivityMapProduced = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            nonDICOMData = self.__getNonDICOMData(
                self.nodes["ACTM"].data, self.nodes["ACTM"].name, 'ACTM')
            VOIActivityDetermination.VoxelActivityMapProduced.NonDICOMData.append(
                nonDICOMData)

            VOIActivityDetermination.DataActivityPerVOIAtTimePointContainer = nonDicomFileSetDescriptor.DataActivityPerVOIAtTimePointContainer()

            for segmentIndex in range(visibleSegmentIDs.GetNumberOfValues()):
                segmentID = visibleSegmentIDs.GetValue(segmentIndex)
                segmentName = segmentationNode.GetSegmentation().GetSegment(segmentID).GetName()
                segmentIdentifier = segmentIndex*1000 + folderIndex + 1000
                for name in activityTableArray:
                    if segmentName in name:
                        if 'M_' in name:
                            marray = activityTableArray[name]
                        elif 'CA_' in name:
                            carray = activityTableArray[name]

                VOI = nonDicomFileSetDescriptor.VOI()
                VOI.VOIIdentifier = segmentIdentifier
                VOI.TimePointIdentifier = folderName
                if segmentName.lower() in self.organs:
                    VOI.OrganOrTissue = segmentName.lower()
                elif segmentName in XMLConstants().OrgansCategoryDict:
                    VOI.OrganOrTissue = XMLConstants(
                    ).OrgansCategoryDict[segmentName]
                else:
                    VOI.OrganOrTissue = 'soft tissue'
                VOI.OrganMass = nonDicomFileSetDescriptor.OrganMass()
                VOI.OrganMass.OrganMassValue = xs.float(marray[activityRow])
                VOI.OrganMass.OrganMassUnit = 'kilogram'
                RegistrationVOISegmentationAndPropagation.Segmentation.VOIContainer.VOIProduced.append(
                    VOI)

                DataActivityPerVOIAtTimePoint = nonDicomFileSetDescriptor.DataActivityPerVOIAtTimePoint()
                DataActivityPerVOIAtTimePoint.VOIIdentifier = segmentIdentifier
                DataActivityPerVOIAtTimePoint.TimePointIdentifier = folderName
                DataActivityPerVOIAtTimePoint.DataActivityValue = xs.float(
                    carray[activityRow])
                DataActivityPerVOIAtTimePoint.ActivityUnit = 'megabecquerel'
                VOIActivityDetermination.DataActivityPerVOIAtTimePointContainer.DataActivityPerVOIAtTimePointProduced.append(
                    DataActivityPerVOIAtTimePoint)

                if folderIndex == 0:
                    TimeActivityCurveFitIn3DDosimetry = nonDicomFileSetDescriptor.TimeActivityCurveFitIn3DDosimetry()
                    TimeActivityCurveFitIn3DDosimetry.ProcessExecutionContext = self.__getProcessExecutionContext(
                        activityTableNode.GetAttribute(Attributes().studyCreation))
                    TimeActivityCurveFitIn3DDosimetry.PreAdministeredActivityUsed = u3DSlide1.AcquisitionSettings.PreAdministeredActivity
                    TimeActivityCurveFitIn3DDosimetry.PostAdministeredActivityUsed = u3DSlide1.AcquisitionSettings.PostAdministeredActivity
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced = nonDicomFileSetDescriptor.TimeIntegratedActivityPerVOI()
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.VOIIdentifierList = nonDicomFileSetDescriptor.VOIIdentifierContainer()
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.VOIIdentifierList.append(
                        segmentIdentifier)

                    rowDoseArray = doseTableArray['Segment'].index(
                        f"CA_{segmentName}")
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.TimeIntegratedActivityPerVOIValue = xs.float(
                        str(doseTableArray['Cummulated Activity (MBqh)'][rowDoseArray]))
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.TimeIntegratedActivityPerVOIUnit = 'MegabecquerelXHour'
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.ResidenceTimePerVOIValue = xs.float(str(
                        TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.TimeIntegratedActivityPerVOIValue / TimeActivityCurveFitIn3DDosimetry.PreAdministeredActivityUsed.AdministeredActivityValue))
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.TimeUnit = "hours"
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.PKAssessmentMethodUsed = nonDicomFileSetDescriptor.CurveFittingMethod()
                    algorithm = str.split(
                        doseTableArray['Fit function'][rowDoseArray], ':')[0]
                    algorithm = XMLConstants().FunctionTypeCategory[algorithm]
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.PKAssessmentMethodUsed.IncorporationFunction = xs.string(
                        doseTableNode.GetAttribute("PKincorporation"))
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.PKAssessmentMethodUsed.IntegrationAlgorithm = xs.string(
                        algorithm)
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.PKAssessmentMethodUsed.FittingFunction = xs.string(
                        doseTableArray['Fit function'][rowDoseArray])

                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityCoefficientPerVOIProduced = nonDicomFileSetDescriptor.TimeIntegratedActivityCoefficientPerVOI()
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityCoefficientPerVOIProduced.TimeIntegratedActivityCoefficientPerVOIValue = TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityPerVOIProduced.ResidenceTimePerVOIValue
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityCoefficientPerVOIProduced.TimeUnit = "hours"
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityCoefficientPerVOIProduced.VOIIdentifierList = nonDicomFileSetDescriptor.VOIIdentifierContainer()
                    TimeActivityCurveFitIn3DDosimetry.TimeIntegratedActivityCoefficientPerVOIProduced.VOIIdentifierList.append(
                        segmentIdentifier)
                    u3DSlide1.ThreeDimDosimetrySlide1workflow.TimeActivityCurveFitIn3DDosimetryContainer.TimeActivityCurveFitIn3DDosimetry.append(
                        TimeActivityCurveFitIn3DDosimetry)

                    AbsorbedDoseInVOIProduced = nonDicomFileSetDescriptor.AbsorbedDoseInVOI()
                    AbsorbedDoseInVOIProduced.VOIIdentifierList = nonDicomFileSetDescriptor.VOIIdentifierContainer()
                    AbsorbedDoseInVOIProduced.VOIIdentifierList.append(
                        segmentIdentifier)
                    AbsorbedDoseInVOIProduced.AbsorbedDoseInVOIValue = xs.float(
                        str(doseTableArray['Absorbed Dose by LED (Gy)'][rowDoseArray]))
                    AbsorbedDoseInVOIProduced.AbsorbedDoseUnit = "gray"
                    u3DSlide1.ThreeDimDosimetrySlide1workflow.AbsorbedDoseCalculationInVOI.AbsorbedDoseInVOIContainer.AbsorbedDoseInVOIProduced.append(
                        AbsorbedDoseInVOIProduced)
                else:
                    u3DSlide1.ThreeDimDosimetrySlide1workflow.TimeActivityCurveFitIn3DDosimetryContainer.TimeActivityCurveFitIn3DDosimetry[
                        segmentIndex].TimeIntegratedActivityPerVOIProduced.VOIIdentifierList.append(segmentIdentifier)
                    u3DSlide1.ThreeDimDosimetrySlide1workflow.AbsorbedDoseCalculationInVOI.AbsorbedDoseInVOIContainer.AbsorbedDoseInVOIProduced[segmentIndex].VOIIdentifierList.append(
                        segmentIdentifier)
                    u3DSlide1.ThreeDimDosimetrySlide1workflow.TimeActivityCurveFitIn3DDosimetryContainer.TimeActivityCurveFitIn3DDosimetry[
                        segmentIndex].TimeIntegratedActivityCoefficientPerVOIProduced.VOIIdentifierList.append(segmentIdentifier)

            RegistrationVOISegmentationAndPropagation.Segmentation.NonDICOMDataContainer = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            nonDICOMData = self.__getNonDICOMData(
                segmentationNode, segmentationName, 'SEGM')
            RegistrationVOISegmentationAndPropagation.Segmentation.NonDICOMDataContainer.NonDICOMData.append(
                nonDICOMData)

            RegistrationVOISegmentationAndPropagation.NonDICOMCTReconResampledOnCommonReferenceProduced = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            nonDICOMData = self.__getNonDICOMData(
                self.nodes["CTRS"].data, self.nodes["CTRS"].name, 'CTRS')
            RegistrationVOISegmentationAndPropagation.NonDICOMCTReconResampledOnCommonReferenceProduced.NonDICOMData.append(
                nonDICOMData)

            RegistrationVOISegmentationAndPropagation.NonDICOMNMTomoReconResampledOnCommonReferenceProduced = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            nonDICOMData = self.__getNonDICOMData(
                self.nodes["ACSC"].data, self.nodes["ACSC"].name, 'ACSC')
            RegistrationVOISegmentationAndPropagation.NonDICOMNMTomoReconResampledOnCommonReferenceProduced.NonDICOMData.append(
                nonDICOMData)

            u3DSlide1.ThreeDimDosimetrySlide1workflow.RegistrationVOISegmentationAndPropagationContainer.RegistrationVOISegmentationAndPropagation.append(
                RegistrationVOISegmentationAndPropagation)
            u3DSlide1.ThreeDimDosimetrySlide1workflow.VOIActivityDeterminationContainer.VOIActivityDetermination.append(
                VOIActivityDetermination)

        pyxb.RequireValidWhenGenerating(True)  # TODO: remove when ready
        ProgressDialog.close()
        self.__writeOutput(u3DSlide1, self.studyFolder /
                           '3D-DosimetrySlide1Worflow.xml')

    def __setupModeAlgo(self, mode, algorithm):
        if 'dose' in mode.lower():
            if 'FFT' in algorithm:
                localAlgorithm = 'FFT convolution (homogeneous)'
            elif 'homogeneous' in algorithm:
                localAlgorithm = 'Convolution (homogeneous)'
            elif 'heterogeneous' in algorithm:
                localAlgorithm = 'Convolution (heterogeneous)'
            elif 'monte' in algorithm.lower():
                localAlgorithm = 'MonteCarlo'
            else:
                localAlgorithm = 'LocalEnergyDepositionLED'
        else:
            localAlgorithm = 'Activity'
        return localAlgorithm

    def fillADRM_XMLfile(self, mode, algorithm=''):
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(self.folders))
        self.__prepareDirectory('ADRIntegration')
        tableNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLTableNode')
        localAlgorithm = self.__setupModeAlgo(mode, algorithm)
        nameDoseRate = f'T:TABL {mode} {localAlgorithm} summary'
        nameDose = f'T:TABL {mode} {localAlgorithm} integrated'
        doseRateTableB = False
        doseTableB = False
        for node in tableNodes:
            if nameDoseRate == node.GetName():
                doseRateTable = node.GetTable()
                doseRateTableNode = node
                doseRateTableB = True
            elif nameDose == node.GetName():
                doseTable = node.GetTable()
                doseTableNode = node
                doseTableB = True

        if not(doseRateTableB and doseTableB):
            raise IOError('Absorbed Dose table not yet created')

        doseRateTableArray = vtkmrmlutils.GetTableAsArray(doseRateTableNode)
        doseTableArray = vtkmrmlutils.GetTableAsArray(doseTableNode)

        u3DSlide2 = nonDicomFileSetDescriptor.NonDicomFileSetDescriptor()

        u3DSlide2.ReferencedClinicalResearchStudy = nonDicomFileSetDescriptor.ReferencedClinicalResearchStudy()
        u3DSlide2.ReferencedClinicalResearchStudy.ClinicalResearchStudyID = self.ClinicalStudyID
        u3DSlide2.ReferencedClinicalResearchStudy.ClinicalResearchStudyTitle = self.ClinicalStudyTitle

        if "CTCT" in self.nodes:
            u3DSlide2.PatientId = self.nodes["CTCT"].data.GetAttribute(
                Attributes().patientID)
        elif "ACSC" in self.nodes:
            u3DSlide2.PatientId = self.nodes["ACSC"].data.GetAttribute(
                Attributes().patientID)
        else:
            raise IOError(
                "Reference node does not have either CT or SPECT acquisition")

        u3DSlide2.AcquisitionSettings = self.__getAcquisitionSettings()

        if "SEGM" not in self.nodes:
            SegmentationNodes = slicer.mrmlScene.GetNodesByClass(
                'vtkMRMLSegmentationNode')
            for segmentation in SegmentationNodes:
                referenceSegmentationNode = segmentation
                break

            if 'referenceSegmentationNode' not in locals():
                raise IOError("No segmentations found")

        u3DSlide2.ThreeDimDosimetrySlide2workflow = nonDicomFileSetDescriptor.ThreeDimDosimetrySlide2workflow()
        u3DSlide2.ThreeDimDosimetrySlide2workflow.SPECTDataAcquisitionAndReconstruction = nonDicomFileSetDescriptor.SPECTDataAcquisitionAndReconstruction()
        u3DSlide2.ThreeDimDosimetrySlide2workflow.SPECTDataAcquisitionAndReconstruction.SPECTAcqCTAcqAndReconstructionContainer = nonDicomFileSetDescriptor.SPECTAcqCTAcqAndReconstructionContainer()
        u3DSlide2.ThreeDimDosimetrySlide2workflow.RegistrationVOISegmentationAndPropagationContainer = nonDicomFileSetDescriptor.RegistrationVOISegmentationAndPropagationContainer()
        u3DSlide2.ThreeDimDosimetrySlide2workflow.VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculationContainer = nonDicomFileSetDescriptor.VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculationContainer()
        u3DSlide2.ThreeDimDosimetrySlide2workflow.TimeActivityCurveFitIn3DDosimetryContainer = nonDicomFileSetDescriptor.TimeActivityCurveFitIn3DDosimetryContainer()
        u3DSlide2.ThreeDimDosimetrySlide2workflow.AbsorbedDoseCalculationInVOI = nonDicomFileSetDescriptor.AbsorbedDoseCalculationInVOI()

        u3DSlide2.ThreeDimDosimetrySlide2workflow.AbsorbedDoseCalculationInVOI.ProcessExecutionContext = self.__getProcessExecutionContext(
            doseRateTableNode.GetAttribute(Attributes().studyCreation))
        u3DSlide2.ThreeDimDosimetrySlide2workflow.AbsorbedDoseCalculationInVOI.AbsorbedDoseCalculationMethodUsed = doseTableNode.GetAttribute(
            "ADalgorithm")
        u3DSlide2.ThreeDimDosimetrySlide2workflow.AbsorbedDoseCalculationInVOI.AbsorbedDoseInVOIContainer = nonDicomFileSetDescriptor.AbsorbedDoseInVOIContainer()

        u3DSlide2.ThreeDimDosimetrySlide2workflow.DoseRateCurveFitVOITimeIntegrationContainer = nonDicomFileSetDescriptor.DoseRateCurveFitVOITimeIntegrationContainer()

        for folderIndex, folderID in enumerate(self.folders):
            folderName = vtkmrmlutils.getItemName(folderID)

            ProgressDialog.labelText = f"Processing {folderName}..."
            ProgressDialog.value = folderIndex
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            self.nodes = Node.getFolderChildren(folderID)
            SPECTAcqCTAcqAndReconstruction = self.__getSPECTAcqCTAcqAndReconstruction(
                folderName)
            u3DSlide2.ThreeDimDosimetrySlide2workflow.SPECTDataAcquisitionAndReconstruction.SPECTAcqCTAcqAndReconstructionContainer.SPECTAcqCTAcqAndReconstruction.append(
                SPECTAcqCTAcqAndReconstruction)

            RegistrationVOISegmentationAndPropagation = nonDicomFileSetDescriptor.RegistrationVOISegmentationAndPropagation()
            RegistrationVOISegmentationAndPropagation.TimePointIdentifierUsed = folderName
            RegistrationVOISegmentationAndPropagation.ImageProcessingMethodMethodUsed = "Semi-automatic segmentation"
            RegistrationVOISegmentationAndPropagation.Segmentation = nonDicomFileSetDescriptor.Segmentation()
            RegistrationVOISegmentationAndPropagation.CTReconUsed = nonDicomFileSetDescriptor.DICOMDataContainer()
            RegistrationVOISegmentationAndPropagation.CTReconUsed.DICOMData.append(
                SPECTAcqCTAcqAndReconstruction.CTReconProduced)
            RegistrationVOISegmentationAndPropagation.NMTomoReconUsed = nonDicomFileSetDescriptor.DICOMDataContainer()
            RegistrationVOISegmentationAndPropagation.NMTomoReconUsed.DICOMData.append(
                SPECTAcqCTAcqAndReconstruction.NMTomoProduced)
            RegistrationVOISegmentationAndPropagation.DensityImageProduced = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            if "DERS" in self.nodes:
                nonDICOMData = self.__getNonDICOMData(
                    self.nodes["DERS"].data, self.nodes["DERS"].name, 'DERS')
            else:
                nonDICOMData = nonDicomFileSetDescriptor.NonDICOMData()
            RegistrationVOISegmentationAndPropagation.DensityImageProduced.NonDICOMData.append(
                nonDICOMData)

            if "TRNF" in self.nodes:
                RegistrationVOISegmentationAndPropagation.GeometricalTransformationContainer = nonDicomFileSetDescriptor.GeometricalTransformationContainer()
                GeometricalTransformation = self.__getGeometricalTransformation(
                    self.nodes["TRNF"].data, self.nodes["TRNF"].name)
                RegistrationVOISegmentationAndPropagation.GeometricalTransformationContainer.GeometricalTransformation.append(
                    GeometricalTransformation)

            doseRateRow = doseRateTableArray['time (h)'].index(
                float(self.nodes["ADRM"][localAlgorithm].data.GetAttribute(Attributes().timeStamp)))

            if "SEGM" in self.nodes:
                segmentationNode = self.nodes["SEGM"].data
                segmentationName = self.nodes["SEGM"].name
            else:
                segmentationNode = referenceSegmentationNode
                segmentationName = segmentationNode.GetName()

            fileHeaderNameSegmentation = str.replace(segmentationName, ":", "")
            visibleSegmentIDs = vtk.vtkStringArray()
            segmentationNode.GetDisplayNode().SetAllSegmentsVisibility(True)
            segmentationNode.GetDisplayNode().GetVisibleSegmentIDs(visibleSegmentIDs)

            segmentationCreation = segmentationNode.GetAttribute(
                Attributes().studyCreation)
            if not segmentationCreation:
                segmentationCreation = utils.getCurrentTime()
            RegistrationVOISegmentationAndPropagation.ProcessExecutionContext = self.__getProcessExecutionContext(
                segmentationCreation)
            RegistrationVOISegmentationAndPropagation.Segmentation.SegmentationIdentifier = fileHeaderNameSegmentation
            RegistrationVOISegmentationAndPropagation.Segmentation.VOIContainer = nonDicomFileSetDescriptor.VoiProducedContainer()

            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation = nonDicomFileSetDescriptor.VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation()

            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination = nonDicomFileSetDescriptor.VOISegmentationVOIMassDetermination()
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination.ProcessExecutionContext = self.__getProcessExecutionContext(
                segmentationCreation)
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination.TimePointIdentifierUsed = folderName
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination.CTReconUsed = SPECTAcqCTAcqAndReconstruction.CTReconProduced
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination.NMTomoReconUsed = SPECTAcqCTAcqAndReconstruction.NMTomoProduced
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination.VOISegmentationMethodUsed = RegistrationVOISegmentationAndPropagation.ImageProcessingMethodMethodUsed
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination.CTReconResampledOnNMReferenceProduced = RegistrationVOISegmentationAndPropagation.CTReconUsed
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination.MassPerVOIAtTimePointContainer = nonDicomFileSetDescriptor.MassPerVOIAtTimePointContainer()

            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOIActivityDetermination = nonDicomFileSetDescriptor.VOIActivityDetermination()
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOIActivityDetermination.TimePointIdentifierUsed = folderName
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOIActivityDetermination.SegmentationIdentifierUsed = fileHeaderNameSegmentation
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOIActivityDetermination.ProcessExecutionContext = self.__getProcessExecutionContext(
                self.nodes["ACTM"].data.GetAttribute(Attributes().studyCreation))
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOIActivityDetermination.SPECTCalibrationFactorReferenceUsed = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOIActivityDetermination.SPECTRecoveryCoefficientCurveReferenceUsed = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOIActivityDetermination.VoxelActivityMapProduced = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            nonDICOMData = self.__getNonDICOMData(
                self.nodes["ACTM"].data, self.nodes["ACTM"].name, 'ACTM')
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOIActivityDetermination.VoxelActivityMapProduced.NonDICOMData.append(
                nonDICOMData)

            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry = nonDicomFileSetDescriptor.EnergyDepositionRateCalculationIn3DDosimetry()
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry.ProcessExecutionContext = self.__getProcessExecutionContext(
                self.nodes["ADRM"][localAlgorithm].data.GetAttribute(Attributes().studyCreation))
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry.TimePointIdentifierUsed = folderName
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry.CalculationAlgorithmUsed = XMLConstants(
            ).CalculationAlgorithmCategory[localAlgorithm]
            if 'convolution' in XMLConstants().CalculationAlgorithmCategory[localAlgorithm].lower():
                VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry.KernelLimitForConvolutionsUsed = nonDicomFileSetDescriptor.KernelLimitForConvolutions()
                VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry.KernelLimitForConvolutionsUsed.KernelLimitForConvolutionsValue = self.nodes["ADRM"][localAlgorithm].data.GetAttribute(
                    Attributes().doseKernelLimit)
                VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry.KernelLimitForConvolutionsUsed.KernelLimitForConvolutionsUnit = 'millimeter'

            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry.CTReconResampledOnNMReferenceUsed = SPECTAcqCTAcqAndReconstruction.CTReconProduced
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry.NMTomoReconUsed = SPECTAcqCTAcqAndReconstruction.NMTomoProduced
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.EnergyDepositionRateCalculationIn3DDosimetry.ThreeDimEnergyDepositionRateMatrixAtTimePointProduced = self.__getNonDICOMData(
                self.nodes["ADRM"][localAlgorithm].data, self.nodes["ADRM"][localAlgorithm].name, 'ADRM')

            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.SumAndScalingAbsorbedDoseRateCalculationContainer = nonDicomFileSetDescriptor.SumAndScalingAbsorbedDoseRateCalculationContainer()

            SumAndScalingAbsorbedDoseRateCalculation = nonDicomFileSetDescriptor.SumAndScalingAbsorbedDoseRateCalculation()
            SumAndScalingAbsorbedDoseRateCalculation.ProcessExecutionContext = self.__getProcessExecutionContext(
                doseRateTableNode.GetAttribute(Attributes().studyCreation))
            # SumAndScalingAbsorbedDoseRateCalculation.VOIIdentifierUsed = nonDicomFileSetDescriptor.VOIIdentifierContainer()
            SumAndScalingAbsorbedDoseRateCalculation.TimePointIdentifierUsed = folderName
            SumAndScalingAbsorbedDoseRateCalculation.SegmentationUsed = fileHeaderNameSegmentation
            SumAndScalingAbsorbedDoseRateCalculation.ThreeDimEnergyDepositionRateMatrixAtTimePointUsed = self.__getNonDICOMData(
                self.nodes["ADRM"][localAlgorithm].data, self.nodes["ADRM"][localAlgorithm].name, 'ADRM')
            SumAndScalingAbsorbedDoseRateCalculation.AbsorbedDoseRatePerVOIAtTimePointProduced = nonDicomFileSetDescriptor.AbsorbedDoseRatePerVOIAtTimePointContainer()

            for segmentIndex in range(visibleSegmentIDs.GetNumberOfValues()):
                segmentID = visibleSegmentIDs.GetValue(segmentIndex)
                segmentName = segmentationNode.GetSegmentation().GetSegment(segmentID).GetName()
                segmentIdentifier = segmentIndex*1000 + folderIndex + 1000
                for name in doseRateTableArray:
                    if segmentName in name:
                        if 'M_' in name:
                            marray = doseRateTableArray[name]
                        elif 'DR_' in name:
                            carray = doseRateTableArray[name]

                VOI = nonDicomFileSetDescriptor.VOI()
                VOI.VOIIdentifier = segmentIdentifier
                VOI.TimePointIdentifier = folderName
                if segmentName.lower() in self.organs:
                    VOI.OrganOrTissue = segmentName.lower()
                elif segmentName in XMLConstants().OrgansCategoryDict:
                    VOI.OrganOrTissue = XMLConstants(
                    ).OrgansCategoryDict[segmentName]
                else:
                    VOI.OrganOrTissue = 'soft tissue'
                VOI.OrganMass = nonDicomFileSetDescriptor.OrganMass()
                VOI.OrganMass.OrganMassValue = xs.float(marray[doseRateRow])
                VOI.OrganMass.OrganMassUnit = 'kilogram'
                RegistrationVOISegmentationAndPropagation.Segmentation.VOIContainer.VOIProduced.append(
                    VOI)

                MassPerVOIAtTimePoint = nonDicomFileSetDescriptor.MassPerVOIAtTimePoint()
                MassPerVOIAtTimePoint.VOIIdentifier = segmentIdentifier
                MassPerVOIAtTimePoint.TimePointIdentifier = folderName
                MassPerVOIAtTimePoint.MassValue = xs.float(marray[doseRateRow])
                MassPerVOIAtTimePoint.MassUnit = 'kilogram'
                VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination.MassPerVOIAtTimePointContainer.MassPerVOIAtTimePointProduced.append(
                    MassPerVOIAtTimePoint)

                AbsorbedDoseRatePerVOIAtTimePoint = nonDicomFileSetDescriptor.AbsorbedDoseRatePerVOIAtTimePoint()
                AbsorbedDoseRatePerVOIAtTimePoint.TimePointIdentifier = folderName
                AbsorbedDoseRatePerVOIAtTimePoint.VOIIdentifier = segmentIdentifier
                AbsorbedDoseRatePerVOIAtTimePoint.AbsorbedDoseRateValue = xs.float(
                    carray[doseRateRow])
                AbsorbedDoseRatePerVOIAtTimePoint.AbsorbedDoseRateUnit = 'milligray per hour'
                SumAndScalingAbsorbedDoseRateCalculation.AbsorbedDoseRatePerVOIAtTimePointProduced.AbsorbedDoseRatePerVOIAtTimePointProduced.append(
                    AbsorbedDoseRatePerVOIAtTimePoint)

                if folderIndex == 0:
                    DoseRateCurveFitVOITimeIntegration = nonDicomFileSetDescriptor.DoseRateCurveFitVOITimeIntegration()
                    DoseRateCurveFitVOITimeIntegration.ProcessExecutionContext = self.__getProcessExecutionContext(
                        doseRateTableNode.GetAttribute(Attributes().studyCreation))
                    DoseRateCurveFitVOITimeIntegration.TimePointIdentifierUsed = folderName
                    DoseRateCurveFitVOITimeIntegration.VOIIdentifierUsed = nonDicomFileSetDescriptor.VOIIdentifierContainer()
                    DoseRateCurveFitVOITimeIntegration.VOIIdentifierUsed.VOIIdentifierUsed.append(
                        segmentIdentifier)
                    DoseRateCurveFitVOITimeIntegration.PreAdministeredActivityUsed = u3DSlide2.AcquisitionSettings.PreAdministeredActivity
                    DoseRateCurveFitVOITimeIntegration.PostAdministeredActivityUsed = u3DSlide2.AcquisitionSettings.PostAdministeredActivity

                    rowDoseArray = doseTableArray['Segment'].index(
                        f"DR_{segmentName}")
                    DoseRateCurveFitVOITimeIntegration.PKAssessmentMethodUsed = nonDicomFileSetDescriptor.CurveFittingMethod()
                    algorithm = str.split(
                        doseTableArray['Fit function'][rowDoseArray], ':')[0]
                    algorithm = XMLConstants().FunctionTypeCategory[algorithm]
                    DoseRateCurveFitVOITimeIntegration.PKAssessmentMethodUsed.IncorporationFunction = xs.string(
                        doseTableNode.GetAttribute("PKincorporation"))
                    DoseRateCurveFitVOITimeIntegration.PKAssessmentMethodUsed.IntegrationAlgorithm = xs.string(
                        algorithm)
                    DoseRateCurveFitVOITimeIntegration.PKAssessmentMethodUsed.FittingFunction = xs.string(
                        doseTableArray['Fit function'][rowDoseArray])

                    DoseRateCurveFitVOITimeIntegration.AbsorbedDoseInVOIProduced = nonDicomFileSetDescriptor.AbsorbedDoseInVOIContainer()
                    AbsorbedDoseinVOI = nonDicomFileSetDescriptor.AbsorbedDoseInVOI()
                    AbsorbedDoseinVOI.AbsorbedDoseInVOIValue = xs.float(
                        str(doseTableArray['Absorbed Dose (Gy)'][rowDoseArray]))
                    AbsorbedDoseinVOI.AbsorbedDoseUnit = 'gray'
                    AbsorbedDoseinVOI.AbsorbedDoseInVOIUncertainty = xs.float(
                        str(doseTableArray['STDDEV (Gy)'][rowDoseArray]))
                    AbsorbedDoseinVOI.VOIIdentifierList = nonDicomFileSetDescriptor.VOIIdentifierContainer()
                    AbsorbedDoseinVOI.VOIIdentifierList.VOIIdentifierUsed.append(
                        segmentIdentifier)
                    DoseRateCurveFitVOITimeIntegration.AbsorbedDoseInVOIProduced.AbsorbedDoseInVOIProduced.append(
                        AbsorbedDoseinVOI)
                    u3DSlide2.ThreeDimDosimetrySlide2workflow.DoseRateCurveFitVOITimeIntegrationContainer.DoseRateCurveFitVOITimeIntegration.append(
                        DoseRateCurveFitVOITimeIntegration)

                    AbsorbedDoseInVOIProduced = nonDicomFileSetDescriptor.AbsorbedDoseInVOI()
                    AbsorbedDoseInVOIProduced.VOIIdentifierList = nonDicomFileSetDescriptor.VOIIdentifierContainer()
                    AbsorbedDoseInVOIProduced.VOIIdentifierList.append(
                        segmentIdentifier)
                    AbsorbedDoseInVOIProduced.AbsorbedDoseInVOIValue = xs.float(
                        str(doseTableArray['Absorbed Dose (Gy)'][rowDoseArray]))
                    AbsorbedDoseInVOIProduced.AbsorbedDoseUnit = "gray"
                    u3DSlide2.ThreeDimDosimetrySlide2workflow.AbsorbedDoseCalculationInVOI.AbsorbedDoseInVOIContainer.AbsorbedDoseInVOIProduced.append(
                        AbsorbedDoseInVOIProduced)
                else:
                    u3DSlide2.ThreeDimDosimetrySlide2workflow.DoseRateCurveFitVOITimeIntegrationContainer.DoseRateCurveFitVOITimeIntegration[
                        segmentIndex].VOIIdentifierUsed.VOIIdentifierUsed.append(segmentIdentifier)
                    u3DSlide2.ThreeDimDosimetrySlide2workflow.DoseRateCurveFitVOITimeIntegrationContainer.DoseRateCurveFitVOITimeIntegration[
                        segmentIndex].AbsorbedDoseInVOIProduced.AbsorbedDoseInVOIProduced[0].VOIIdentifierList.VOIIdentifierUsed.append(segmentIdentifier)
                    u3DSlide2.ThreeDimDosimetrySlide2workflow.AbsorbedDoseCalculationInVOI.AbsorbedDoseInVOIContainer.AbsorbedDoseInVOIProduced[segmentIndex].VOIIdentifierList.append(
                        segmentIdentifier)

            RegistrationVOISegmentationAndPropagation.Segmentation.NonDICOMDataContainer = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            nonDICOMData = self.__getNonDICOMData(
                segmentationNode, segmentationName, 'SEGM')
            RegistrationVOISegmentationAndPropagation.Segmentation.NonDICOMDataContainer.NonDICOMData.append(
                nonDICOMData)
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.VOISegmentationVOIMassDetermination.SegmentationProduced = RegistrationVOISegmentationAndPropagation.Segmentation
            VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.SumAndScalingAbsorbedDoseRateCalculationContainer.SumAndScalingAbsorbedDoseRateCalculation.append(
                SumAndScalingAbsorbedDoseRateCalculation)

            RegistrationVOISegmentationAndPropagation.NonDICOMCTReconResampledOnCommonReferenceProduced = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            nonDICOMData = self.__getNonDICOMData(
                self.nodes["CTRS"].data, self.nodes["CTRS"].name, 'CTRS')
            RegistrationVOISegmentationAndPropagation.NonDICOMCTReconResampledOnCommonReferenceProduced.NonDICOMData.append(
                nonDICOMData)

            RegistrationVOISegmentationAndPropagation.NonDICOMNMTomoReconResampledOnCommonReferenceProduced = nonDicomFileSetDescriptor.NonDICOMDataContainer()
            nonDICOMData = self.__getNonDICOMData(
                self.nodes["ACSC"].data, self.nodes["ACSC"].name, 'ACSC')
            RegistrationVOISegmentationAndPropagation.NonDICOMNMTomoReconResampledOnCommonReferenceProduced.NonDICOMData.append(
                nonDICOMData)

            u3DSlide2.ThreeDimDosimetrySlide2workflow.RegistrationVOISegmentationAndPropagationContainer.RegistrationVOISegmentationAndPropagation.append(
                RegistrationVOISegmentationAndPropagation)
            u3DSlide2.ThreeDimDosimetrySlide2workflow.VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculationContainer.VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.append(
                VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation)

        pyxb.RequireValidWhenGenerating(True)  # TODO: remove when ready
        ProgressDialog.close()
        self.__writeOutput(u3DSlide2, self.studyFolder /
                           '3D-DosimetrySlide2Worflow.xml')

    def fillSensitivity_XMLfile(self, tankNode):
        uCalibration = nonDicomFileSetDescriptor.NonDicomFileSetDescriptor()

        uCalibration.ReferencedClinicalResearchStudy = nonDicomFileSetDescriptor.ReferencedClinicalResearchStudy()
        uCalibration.ReferencedClinicalResearchStudy.ClinicalResearchStudyID = self.ClinicalStudyID
        uCalibration.ReferencedClinicalResearchStudy.ClinicalResearchStudyTitle = self.ClinicalStudyTitle

        uCalibration.PatientId = tankNode.GetAttribute(Attributes().patientID)

        uCalibration.AcquisitionSettings = self.__getAcquisitionSettingsCalibration(tankNode)