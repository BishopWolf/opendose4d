import sqlite3

from Logic.PhysicalUnits import PhysicalUnits
from Logic.utils import getScriptPath


def setupIsotope(isotopeText='Lu-177'):
    Isotope = {}
    db = getScriptPath() / "Resources" / "Dosimetry4D.db3"
    with sqlite3.connect(str(db)) as connectDosimetry:
        cursor = connectDosimetry.cursor()
        sql = f'SELECT * FROM isotopes WHERE name="{isotopeText}"'
        row = cursor.execute(sql).fetchall()[0]
        Isotope['name'] = isotopeText
        Isotope['energy'] = row[2] * PhysicalUnits().getUnit('Energy',
                                                             'MeV')  # to Joules
        Isotope['T_h'] = row[3]  # already in hours

    return Isotope


def readDVK(isotopeText='Lu-177', DVK_size=None):
    DVK = {}
    db = getScriptPath() / "Resources" / "Dosimetry4D.db3"
    with sqlite3.connect(str(db)) as connectDosimetry:
        cursor = connectDosimetry.cursor()
        size = round(DVK_size[0]/PhysicalUnits().getUnit('Longitude', 'mm'), 2)
        sql = f"""SELECT * from vsv
                    WHERE isotope IN (SELECT id FROM isotopes WHERE name=\"{isotopeText}\")
                    AND voxel_size={size}
        """

        rows = cursor.execute(sql).fetchall()

        for row in rows:
            DVK[row[3]] = [row[4], row[5], row[6]]

    return DVK


def getIsotopes():
    Isotopes = []
    db = getScriptPath() / "Resources" / "Dosimetry4D.db3"
    with sqlite3.connect(str(db)) as connectDosimetry:
        cursor = connectDosimetry.cursor()
        sql = f'SELECT * FROM isotopes'
        rows = cursor.execute(sql).fetchall()
        for row in rows:
            Isotope = {}
            Isotope['name'] = row[1]
            Isotope['energy'] = row[2] * \
                PhysicalUnits().getUnit('Energy', 'MeV')  # to Joules
            Isotope['T_h'] = row[3]  # already in hours
            Isotopes.append(Isotope)

    return Isotopes
