from Logic.singleton import Singleton

class Attributes(Singleton):

    def __init__(self):
        self.instanceUIDs = 'DICOM.instanceUIDs'
        self.studyInstanceUID = 'DICOM.StudyInstanceUID'
        self.seriesInstanceUID = 'DICOM.SeriesInstanceUID'
        self.patientID = 'DICOM.PatientID'
        self.patientName = 'DICOM.PatientName'
        self.studyCreation = 'DICOM.Study.Date'

        self.modality = 'DICOM.Modality'
        self.acquisition = 'DICOM.Acquisition.Date'
        self.acquisitionDuration = 'DICOM.Acquisition.Duration'
        self.acquisitionFrames = 'DICOM.Acquisition.Frames'
        self.injectionTime = 'DICOM.Injection.Date'
        self.injectedActivity = 'DICOM.Injection.Activity'
        self.radiopharmaceutical = 'DICOM.Injection.Radiopharmaceutical'
        self.isotope = 'DICOM.Isotope'

        self.acquisitionFramesDuration = 'DICOM.AcquisitionFramesDuration'
        self.acquisitionFramesTotal = 'DICOM.AcquisitionFramesTotal'

        self.calibrationDuration = 'DICOM.Calibration.Duration'
        self.calibrationCT = 'DICOM.Calibration.HUtoDENSfactor'
        self.sensitivity = 'DICOM.Calibration.Sensitivity'
        self.sensitivityUnits = 'DICOM.Calibration.SensitivityUnits'

        self.resamplingMode = 'Slicer.Resampling.Mode'
        self.resamplingVolume = 'Slicer.Resampling.OriginalVolume'
        self.resamplingReference = 'Slicer.Resampling.ReferenceVolume'
        self.rescalingVolume = 'Slicer.Rescaling.OriginalVolume'
        self.registrationVolume = 'Slicer.Registration.MovingVolume'
        self.registrationReference = 'Slicer.Registration.FixedVolume'
        self.registrationMode = 'Slicer.Registration.initializeTransformMode'
        self.registrationSampling = 'Slicer.Registration.samplingPercentage'
        self.registrationRigid = 'Slicer.Registration.Rigid'
        self.registrationAffine = 'Slicer.Registration.Affine'
        self.registrationAlgorithm = 'Slicer.Registration.Algorithm'

        self.doseACTMVolume = 'Dosimetry4D.ADR.ACTMVolume'
        self.doseAlgorithm = 'Dosimetry4D.ADR.Algorithm'
        self.doseAlgorithmDescription = 'Dosimetry4D.ADR.Description'
        self.doseKernelLimit = 'Dosimetry4D.ADR.KernelLimit'
        self.tableCreationMode = 'Dosimetry4D.CreationMode'
        self.timeStamp = 'Dosimetry4D.TimeStamp'
