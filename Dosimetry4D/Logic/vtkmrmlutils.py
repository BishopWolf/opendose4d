from difflib import SequenceMatcher
import numpy as np
import SegmentStatistics
import vtk

import slicer

import Logic.logging as logging
import Logic.utils as utils
import Logic.vtkutils as vtkutils
import Logic.errors as errors

logger = logging.getLogger("Dosimetry4D.vtkmrmlutils")


def isDICOM(itemID):
    attribute = getItemDataNode(itemID).GetAttribute("DICOM.instanceUIDs")
    return bool(attribute)


def getSubjectHierarchyNode():
    """
    Gets the subject hierarchy node
    """
    return slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)


def getSubjectIDs():
    # NOTE assumes that: scene --> subject
    shNode = getSubjectHierarchyNode()
    sceneID = shNode.GetSceneItemID()
    subjectIDs = []

    rootNodesIDs = getAllFolderChildren(sceneID)
    for nodeID in rootNodesIDs:
        level = getItemAttributeValue(nodeID, "Level")
        if level == "Patient":
            subjectIDs.append(nodeID)

    return subjectIDs


def getStudyIDs():
    #  NOTE assumes that: scene --> subject --> study
    try:
        subjectID = getSubjectIDs()[0]
    except:
        raise

    studyIDs = []
    nodeIDs = getAllFolderChildren(subjectID)
    for nodeID in nodeIDs:
        level = getItemAttributeValue(nodeID, "Level")
        if level == "Study":
            studyIDs.append(nodeID)

    return studyIDs


def getResultIDs():
    # NOTE assumes that: scene --> subject --> study --> results
    shNode = getSubjectHierarchyNode()
    try:
        studyID = getStudyIDs()[0]
    except:
        raise

    resultIDs = []
    nodeIDs = getAllFolderChildren(studyID)
    for nodeID in nodeIDs:
        level = getItemAttributeValue(nodeID, "Level")
        if (level == "Folder") and (shNode.HasItemAttribute(nodeID, "Results")):
            resultIDs.append(nodeID)

    return resultIDs


def getFolderIDs():
    # NOTE assumes that: scene --> subject --> study --> folder
    shNode = getSubjectHierarchyNode()
    try:
        # expect it to have only one study
        studyID = getStudyIDs()[0]
    except:
        raise

    foldersIDs = []
    nodeIDs = getAllFolderChildren(studyID)
    for nodeID in nodeIDs:
        level = getItemAttributeValue(nodeID, "Level")
        if (level == "Folder") and not shNode.HasItemAttribute(nodeID, "Results"):
            foldersIDs.append(nodeID)

    return foldersIDs


def getScalarVolumeNodes():
    """
    Gets all scalarVolumeNodes, no matter where in the hierarchy they exist
    """
    shNode = getSubjectHierarchyNode()
    nodes = slicer.util.getNodesByClass("vtkMRMLScalarVolumeNode")
    itemIDs = []

    for node in nodes:
        nodeID = shNode.GetItemByDataNode(node)
        itemIDs.append(nodeID)

    return itemIDs


def getFirstNodeByName(name):
    shNode = getSubjectHierarchyNode()
    node = slicer.util.getFirstNodeByName(name)
    nodeID = shNode.GetItemByDataNode(node)
    return nodeID


def getItemDataNode(itemID):
    shNode = getSubjectHierarchyNode()
    node = shNode.GetItemDataNode(itemID)
    return node


def getItemDataNodeName(itemID):
    node = getItemDataNode(itemID)
    name = node.GetName()
    return name


def getItemID(node):
    """
    Gets itemID of a node
    """
    shNode = getSubjectHierarchyNode()
    try:
        return shNode.GetItemByDataNode(node)
    except:
        return 0


def getItemDataNodeAttributeNames(itemID):
    node = getItemDataNode(itemID)
    attributeNames = node.GetAttributeNames()
    return attributeNames


def hasItemDataNodeAttribute(itemID, attributeName):
    attributeNames = getItemDataNodeAttributeNames(itemID)

    if attributeName in attributeNames:
        logger.debug(f"Item with id '{itemID}' has attribute '{attributeName}'")
        return True

    logger.info(
        f"Item with id '{itemID}' don't has attribute '{attributeName}'")
    return False


def getItemDataNodeAttributeValue(itemID, attributeName):
    node = getItemDataNode(itemID)
    attributeValue = node.GetAttribute(attributeName)

    if attributeValue is None:
        logger.info(
            f"Item with id '{itemID}' do not has attribute '{attributeName}'")
    else:
        logger.debug(f"Item with id '{itemID}' has attribute '{attributeName}'")

    return attributeValue


def setItemDataNodeAttribute(itemID, attributeName, attributeValue):
    node = getItemDataNode(itemID)

    try:
        node.SetAttribute(attributeName, str(attributeValue))
        logger.debug(
            f"Created attribute '{attributeName}' with value '{attributeValue}' for item data-node with id '{itemID}'")
    except:
        logger.error(
            f"Failed to create attribute '{attributeName}' with value '{attributeValue}' for item data-node with id '{itemID}'")


def getItemName(itemID):
    """
    Gets item name of a node
    """
    shNode = getSubjectHierarchyNode()

    return shNode.GetItemName(itemID)


def setItemName(itemID, name):
    """
    Sets item name of a node
    """
    shNode = getSubjectHierarchyNode()
    shNode.SetItemName(itemID, name)


def getItemAttributeNames(itemID):
    """
    Gets all attribute names of an item
    """
    shNode = getSubjectHierarchyNode()
    attributeNames = shNode.GetItemAttributeNames(itemID)

    if len(attributeNames) is 0:
        logger.error(f"Item with id '{itemID}' has no attributes")
    else:
        logger.debug(
            f"Item with id '{itemID}' has attributes '{attributeNames}'")

    return attributeNames


def getItemTransformNodeID(itemID):
    """
    Gets the transformation Node of an item
    """
    node = getItemDataNode(itemID)
    transformNode = node.GetParentTransformNode()
    return getItemID(transformNode)


def hasItemAttribute(itemID, attributeName):
    """
    Checks if given attribute exists form an item
    """
    shNode = getSubjectHierarchyNode()
    attributeNames = shNode.GetItemAttributeNames(itemID)

    if attributeName in attributeNames:
        logger.debug(f"Item with id '{itemID}' has attribute '{attributeName}'")
        return True

    logger.debug(f"Item with id '{itemID}' has no attribute '{attributeName}'")
    return False


def getItemAttributeValue(itemID, attributeName):
    """
    Gets the value of an attribute
    """
    shNode = getSubjectHierarchyNode()
    attributeValue = shNode.GetItemAttribute(itemID, attributeName)

    if attributeValue is "":
        logger.info(
            f"Item with id '{itemID}' has no attribute '{attributeName}'")
    else:
        logger.debug(
            f"Item with id '{itemID}' has attribute '{attributeName}' with value '{attributeValue}'")

    return attributeValue


def setItemAttribute(itemID, attributeName, attributeValue):
    """
    Sets the an attribute and a value for an item
    """
    shNode = getSubjectHierarchyNode()
    try:
        shNode.SetItemAttribute(itemID, attributeName, str(attributeValue))
        logger.debug(
            f"Created attribute '{attributeName}' with value '{attributeValue}' for item with id '{itemID}'")
    except:
        logger.info(
            f"Failed to create attribute '{attributeName}' with value '{attributeValue}' for item with id '{itemID}'")


def setItemFolder(itemID, folderID):
    shNode = getSubjectHierarchyNode()
    shNode.SetItemParent(itemID, folderID)


def setNodeFolder(dataNode, folderID):
    shNode = getSubjectHierarchyNode()
    shNode.CreateItem(folderID, dataNode)


def createFolder(parentID, name):
    shNode = getSubjectHierarchyNode()
    folderID = shNode.CreateFolderItem(parentID, name)

    pluginHandler = slicer.qSlicerSubjectHierarchyPluginHandler().instance()
    folderPlugin = pluginHandler.pluginByName("Folder")
    folderPlugin.setDisplayVisibility(folderID, 1)

    return folderID


def createResultsFolder(parentID):
    ResultID = createFolder(parentID, "Results")
    setItemAttribute(ResultID, "Results", 1)

    return ResultID


def getFolderByName(name):
    shNode = getSubjectHierarchyNode()
    subjectID = getSubjectIDs()[0]

    studyIDs = getAllFolderChildren(subjectID)

    for studyID in studyIDs:
        folderIDs = getAllFolderChildren(studyID)

        for folderID in folderIDs:
            folderName = shNode.GetItemName(folderID)
            if name == folderName:
                return folderID

    return 0  # only if folder do not exists


def getFolderNames():
    folders = getFolderIDs()
    folderNames = []
    for folderID in folders:
        folderNames.append(getItemName(folderID))
    return folderNames


def getFolderID(nodeName):
    """
    Gets a specific folder
    NOTE if folder does not exists, returns 0
    """
    shNode = getSubjectHierarchyNode()
    referenceNode = slicer.util.getFirstNodeByClassByName(
        "vtkMRMLScalarVolumeNode", nodeName)
    nodeID = shNode.GetItemByDataNode(referenceNode)
    folderID = shNode.GetItemParent(nodeID)

    return folderID


def getParentFolder(itemID):
    shNode = getSubjectHierarchyNode()
    folderID = shNode.GetItemParent(itemID)

    return folderID


def getAllFolderChildren(folderID):
    shNode = getSubjectHierarchyNode()

    # empty list of node id"s gets filled
    items = vtk.vtkIdList()
    shNode.GetItemChildren(folderID, items)

    # move vtkList to list
    itemIDs = []
    for i in np.arange(items.GetNumberOfIds()):
        itemID = items.GetId(i)
        itemIDs.append(itemID)

    return itemIDs


def getFolderChildByName(folderID, name):
    shNode = getSubjectHierarchyNode()
    itemIDs = getAllFolderChildren(folderID)

    for itemID in itemIDs:
        itemName = shNode.GetItemName(itemID)
        if itemName == name:
            return itemID


def makeSlicerLinkedCompositeNodes():
    # Set linked slice views  in all existing slice composite nodes and in the default node
    sliceCompositeNodes = slicer.util.getNodesByClass(
        "vtkMRMLSliceCompositeNode")
    defaultSliceCompositeNode = slicer.mrmlScene.GetDefaultNodeByClass(
        "vtkMRMLSliceCompositeNode")
    if not defaultSliceCompositeNode:
        defaultSliceCompositeNode = slicer.mrmlScene.AddNewNodeByClass(
            "vtkMRMLSliceCompositeNode")
        slicer.mrmlScene.AddDefaultNode(defaultSliceCompositeNode)
    for sliceCompositeNode in sliceCompositeNodes:
        sliceCompositeNode.SetLinkedControl(True)
    defaultSliceCompositeNode.SetLinkedControl(True)


def setSlicerViews(backgroundID, foregroundID):
    if not utils.checkTesting():
        makeSlicerLinkedCompositeNodes()

        slicer.util.setSliceViewerLayers(background=str(
            backgroundID), foreground=str(foregroundID), foregroundOpacity=0.5)

        slicer.app.layoutManager().setLayout(
            slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView)
        slicer.util.resetSliceViews()
        slicer.app.processEvents()


def getFolderChildrenByPattern(folderID, pattern):
    """
    Gets all children of a specific folder with a name following the given pattern
    NOTE Could also return an empty array if pattern does not get matched for any children
    """
    # TODO refactor the return data structure to use the node class
    shNode = getSubjectHierarchyNode()
    children = vtk.vtkIdList()
    shNode.GetItemChildren(folderID, children)
    nodes = []

    for i in np.arange(children.GetNumberOfIds()):
        nodeID = children.GetId(i)

        if pattern in shNode.GetItemName(nodeID):
            nodes.append(nodeID)

    return nodes


def setAndObserveColorNode(itemID):
    """
    set observable in color node
    """
    node = getItemDataNode(itemID)
    name = getItemName(itemID)
    displayNode = node.GetScalarVolumeDisplayNode()

    if displayNode is not None:
        petID = getColorNodePETID()
        greyID = getColorNodeGreyID()

        if "acsc" in name.lower():
            displayNode.SetAndObserveColorNodeID(petID)
        elif "ctct" in name.lower() or "ctrs" in name.lower():
            displayNode.SetAndObserveColorNodeID(greyID)
        elif "dens" in name or "ders" in name.lower():
            displayNode.SetAndObserveColorNodeID(greyID)
        elif "actm" in name or "dose" in name.lower():
            displayNode.SetAndObserveColorNodeID(petID)

        # Refresh Window Level
        displayNode.AutoWindowLevelOff()
        displayNode.AutoWindowLevelOn()


def getColorNodePETID():
    # this color node is hidden, but initially instantiated at slicer startup
    return slicer.util.getFirstNodeByName("PET-Heat").GetID()


def getColorNodeGreyID():
    # this color node is hidden, but initially instantiated at slicer startup
    return slicer.util.getFirstNodeByName("Grey").GetID()


def getCommonNameSubString(itemName_0, itemName_1):
    # find the documentation: https://docs.python.org/2/library/difflib.html#difflib.SequenceMatcher.find_longest_match
    matcher = SequenceMatcher(None, itemName_0, itemName_1)
    sequence = matcher.find_longest_match(
        0, len(itemName_0), 0, len(itemName_1))
    match = itemName_1[sequence.b: sequence.b + sequence.size]
    return match


def hasImageData(volumeNode):
    """
    This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not volumeNode:
        logger.info(f"{utils.whoami()} failed: no volume node")
        return False
    if volumeNode.GetImageData() is None:
        logger.info(f"{utils.whoami()} failed: no image data in volume node")
        return False
    return True


def hasSegmentation(segmentationNode):
    """
    This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not segmentationNode:
        logger.info(f"{utils.whoami()} failed: no segmentation node")
        return False
    segmnode = slicer.util.getFirstNodeByClassByName(
        "vtkMRMLSegmentationNode", segmentationNode.GetName())
    if not segmnode:
        logger.info(
            f"{utils.whoami()} failed: no segmentation in segmentation node")
        return False
    return True

def hasTable(TableNode):
    """
    This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not TableNode:
        logger.info(f"{utils.whoami()} failed: no table node")
        return False
    tablenode = slicer.util.getFirstNodeByClassByName(
        "vtkMRMLTableNode", TableNode.GetName())
    if not tablenode:
        logger.info(
            f"{utils.whoami()} failed: no table in tablenode")
        return False
    table = tablenode.GetTable()
    if table.GetNumberOfColumns()<=0:
        logger.info(
            f"{utils.whoami()} failed: tablenode is empty")
        return False
    return True


def GetTableAsArray(tableNode):
    TableArray = {}
    table = tableNode.GetTable()
    for col in range(tableNode.GetNumberOfColumns()):
        column = table.GetColumn(col)
        TableArray[column.GetName()] = list(vtkutils.convertArray(column))
    return TableArray


def helperSegmentStatisticsTable(segmentationNode, masterVolumeNode, resultsTableNode):
    segnodeID = getItemID(segmentationNode)
    if not utils.checkTesting():
        displayNode = segmentationNode.GetDisplayNode()
        if displayNode is not None:
            displayNode.SetVisibility(1)
    segStatLogic = SegmentStatistics.SegmentStatisticsLogic()
    parameters = segStatLogic.getParameterNode()
    parameters.SetParameter("visibleSegmentsOnly", "False")
    parameters.SetParameter("Segmentation", segmentationNode.GetID())
    parameters.SetParameter("ScalarVolume", masterVolumeNode.GetID())
    parameters.SetParameter("LabelmapSegmentStatisticsPlugin.enabled", "False")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.enabled", "True")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.voxel_count.enabled", "True")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.volume_mm3.enabled", "False")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.min.enabled", "False")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.max.enabled", "False")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.mean.enabled", "True")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.median.enabled", "True")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.std.enabled", "False")
    parameters.SetParameter(
        "PETVolumeSegmentStatisticsPlugin.enabled", "False")
    segStatLogic.computeStatistics()
    segStatLogic.exportToTable(resultsTableNode)

    resultsTableNode.SetAttribute("Segmentation.Reference", str(segnodeID))
    resultsTableNode.SetAttribute("masterVolumeNode", str(getItemID(masterVolumeNode)))
    resultsTableNode.SetAttribute("DICOM.Study.Date", utils.getCurrentTime())

    if not hasTable(resultsTableNode):
        raise errors.IOError(f"Failed to create Table {resultsTableNode.GetName()}")

    if not utils.checkTesting() and displayNode is not None:
        displayNode.SetVisibility(0)


def showTable(table):
    """
    Switch to a layout where tables are visible and show the selected table
    """
    if not utils.checkTesting():
        layoutManager = slicer.app.layoutManager()
        currentLayout = layoutManager.layout
        layoutWithTable = slicer.modules.tables.logic().GetLayoutWithTable(currentLayout)
        layoutManager.setLayout(layoutWithTable)
        appLogic = slicer.app.applicationLogic()
        appLogic.GetSelectionNode().SetActiveTableID(table.GetID())
        appLogic.PropagateTableSelection()
        return

    # Test environment does not have layoutManager
    print("Can't display table")

def showChartinLayout(chart):
    """
    Switch to a layout where charts are visible and show the selected plot
    """
    if not utils.checkTesting():
        slicer.modules.plots.logic().ShowChartInLayout(chart)
        return

    # Test environment does not have layoutManager
    print("Can't display chart")

def RefreshFolderView(folderItemID):
    if not utils.checkTesting():
        pluginHandler = slicer.qSlicerSubjectHierarchyPluginHandler().instance()
        folderPlugin = pluginHandler.pluginByName("Folder")
        folderPlugin.setDisplayVisibility(folderItemID, 0)
        folderPlugin.setDisplayVisibility(folderItemID, 1)
        return

    # Test environment does not have layoutManager
    print("Can't refresh folder view")

def cloneNode(node, newName):
    shNode = getSubjectHierarchyNode()
    nodeID = shNode.GetItemByDataNode(node)
    newnode = slicer.util.getFirstNodeByClassByName(
        'vtkMRMLScalarVolumeNode', newName)

    if newnode:
        slicer.mrmlScene.RemoveNode(newnode)

    clonedNode = slicer.modules.volumes.logic().CloneVolume(node, newName)
    clonedItemID = getItemID(clonedNode)
    clonedNodeName = getItemDataNodeName(clonedItemID)
    if clonedNodeName != newName:
        shNode.SetItemName(clonedItemID, newName)

    parent = shNode.GetItemParent(nodeID)
    shNode.SetItemParent(clonedItemID, parent)

    transformNode = node.GetParentTransformNode()
    if transformNode is not None:
        clonedNode.SetAndObserveTransformNodeID(transformNode.GetID())

    return clonedNode


def saveNode(node, fileName, orientation='LPS'):
    # Reorient the node first
    node.reorient(orientation)
    # Get the parent Transform
    transformNode = node.getTransformNode()
    # Remove transformation for saving
    node.removeTransformNode()
    # Save
    slicer.util.saveNode(node.data, str(fileName), {'useCompression':False})
    # Restore previous transformation
    if transformNode:
        node.setTransformNode(transformNode)
