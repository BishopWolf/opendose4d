import configparser
import os
import uuid
import json

from Logic.errors import IOError
from Logic.utils import getScriptPath


def init():
    write()
    return read('default', 'env')


def write():
    # NOTE currently (over) writes the whole ini file, instead of only adding new attributes
    userID = uuid.uuid4()
    config = configparser.ConfigParser()
    config['default'] = {'userID': str(userID), 'env': 'test'}
    config['sentry'] = {
        'dns': 'https://e2f1b6f0916c4f169572536431ac62c4@sentry.io/1779783'}
    path = getScriptPath() / 'config.ini'

    if not os.path.exists(path):
        with open(path, 'w') as configFile:
            try:
                config.write(configFile)
            except IOError as err:
                raise err


def read(section, attribute):
    path = getScriptPath() / 'config.ini'
    if os.path.exists(path):
        config = configparser.ConfigParser()
        config.read(path)
        try:
            value = config[section][attribute]
            return value
        except Exception as err:
            raise err
    else:
        return init()


def writeSPECTSensitivity(sensitivity):
    path = getScriptPath() / 'Resources' / 'JSON' / 'SPECTSensitivity.json'
    path.touch()  # Create if it does not exists
    with path.open('w') as f:
        f.write(json.dumps(sensitivity, indent=2))


def readSPECTSensitivity():
    path = getScriptPath() / 'Resources' / 'JSON' / 'SPECTSensitivity.json'
    if not path.exists():
        return {}
    with path.open('r') as f:
        return json.load(f)


def writeClinicalCenters(clinicalcenters):
    path = getScriptPath() / 'Resources' / 'JSON' / 'ClinicalCenters.json'
    path.touch()  # Create if it does not exists
    with path.open('w') as f:
        f.write(json.dumps(clinicalcenters, indent=2))


def readClinicalCenters():
    path = getScriptPath() / 'Resources' / 'JSON' / 'ClinicalCenters.json'
    if not path.exists():
        return {}
    with path.open('r') as f:
        return json.load(f)
