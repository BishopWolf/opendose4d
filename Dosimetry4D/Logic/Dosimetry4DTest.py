import importlib
import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModuleTest

import Logic.logging as logging
import Logic.utils as utils
from Logic.attributes import Attributes
import Logic.errors as errors
from Logic.Dosimetry4DLogic import Dosimetry4DLogic
import Logic.testutils as testutils
import Logic.testbuilder as testbuilder
import Logic.vtkmrmlutils as vtkmrmlutils
from Logic.constants import Constants
from Logic.nodes import Node


class Dosimetry4DTest(ScriptedLoadableModuleTest):
    """
    This is the test case for your scripted module.
    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        self.tearDown()

        # logger
        self.logger = logging.getLogger('Dosimetry4D.Tests')
        self.logger.setLevel(logging.INFO)

        # modules
        self.logic = Dosimetry4DLogic()
        self.attributes = Attributes()
        self.testValues = testutils.TestValues()

    def tearDown(self):
        slicer.mrmlScene.Clear(0)
        slicer.app.processEvents()

    def runTest(self):
        """
        Run as few or as many tests as needed here.
        """
        self.setUp()
        self.logger.info('\n\n***** Dosimetry4D Module Testing *****\n')

        # since first test, setup database
        try:
            testbuilder.initDICOMDatabase()
        except:
            raise

        self.standardiseHierarchy_set_positive()
        self.standardiseHierarchy_default_positive()
        self.standardiseHierarchy_subjects_negative()
        self.standardiseHierarchy_results_negative()
        self.standardiseNodes_DICOM_positive()
        self.standardiseNodes_MRB_positive()
        self.standardiseNodes_negative()
        self.standardise_positive()
        self.setupIsotope_default_positive()
        self.setupIsotope_custom_positive()
        # self.checkAttributes_positive()
        # self.checkAttributes_negative()

        self.FullTest_positive()

        self.tearDown()
        self.logger.info('\n******** Dosimetry4D Tests passed ***************\n')

    def standardiseHierarchy_set_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        subjectName = 'Predefined Subject'
        studyName = 'Predefined Study'

        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), subjectName)
        studyID = shNode.CreateStudyItem(subjectID, studyName)

        # act
        subjectID_actual, studyID_actual = self.logic.standardiseHierarchy()

        # assert
        self.assertEqual(subjectID, subjectID_actual)
        self.assertEqual(
            subjectName, vtkmrmlutils.getItemName(subjectID_actual))
        self.assertEqual(studyID, studyID_actual)
        self.assertEqual(studyName, vtkmrmlutils.getItemName(studyID_actual))

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def standardiseHierarchy_default_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        subjectName = 'Subject'
        studyName = 'Study'

        # act
        subjectID, studyID = self.logic.standardiseHierarchy()

        # assert
        self.assertEqual(subjectName, vtkmrmlutils.getItemName(subjectID))
        self.assertEqual(studyName, vtkmrmlutils.getItemName(studyID))

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def standardiseHierarchy_subjects_negative(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        subjectName_0 = 'Subject 0'
        subjectName_1 = 'Subject 1'

        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        _ = shNode.CreateSubjectItem(shNode.GetSceneItemID(), subjectName_0)
        _ = shNode.CreateSubjectItem(shNode.GetSceneItemID(), subjectName_1)

        # act & assert
        with self.assertRaises(errors.ConventionError) as context:
            _, _ = self.logic.standardiseHierarchy()
            exception = context.exception.message
            self.assertEqual(
                'Scene contains 2 subjects. Please remove all but one subject.', exception)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def standardiseHierarchy_studies_negative(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        subjectName = 'Subject'
        studyName_0 = 'Study 0'
        studyName_1 = 'Study 1'

        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), subjectName)
        _ = shNode.CreateStudyItem(subjectID, studyName_0)
        _ = shNode.CreateStudyItem(subjectID, studyName_1)

        # act & assert
        with self.assertRaises(errors.ConventionError) as context:
            _, _ = self.logic.standardiseHierarchy()
            exception = context.exception.message
            self.assertEqual(
                'Subject contains 2 studies. Please remove all but one study.', exception)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def standardiseHierarchy_results_negative(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        subjectName = 'Subject'
        studyName = 'Study'
        resultName_0 = 'Results 0'
        resultName_1 = 'Results 1'

        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), subjectName)
        studyID = shNode.CreateStudyItem(subjectID, studyName)
        resultID_0 = vtkmrmlutils.createFolder(studyID, resultName_0)
        resultID_1 = vtkmrmlutils.createFolder(studyID, resultName_1)

        vtkmrmlutils.setItemAttribute(resultID_0, 'Results', 1)
        vtkmrmlutils.setItemAttribute(resultID_1, 'Results', 1)

        # act & assert
        with self.assertRaises(errors.ConventionError) as context:
            _, _ = self.logic.standardiseHierarchy()

        exception = context.exception.message
        self.assertEqual(
            'Study contains 2 results folder. Please remove all but one results folder.', exception)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def standardiseNodes_DICOM_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = testbuilder.setupDICOM()
        studyID = vtkmrmlutils.getStudyIDs()[0]

        # act
        self.logic.standardiseNodes(studyID)

        # assert
        folderIDs = vtkmrmlutils.getFolderIDs()
        folderID = vtkmrmlutils.getAllFolderChildren(folderIDs[0])
        childID = folderID[0]

        self.assertTrue(6, len(folderIDs))
        self.assertTrue(2, len(folderID))
        self.assertEqual(itemIDs[0], childID)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def standardiseNodes_MRB_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = testbuilder.setupDICOM()
        studyID = vtkmrmlutils.getStudyIDs()[0]
        # dicom to mrb
        self.logic.standardiseNodes(studyID)

        # act
        self.logic.standardiseNodes(studyID)

        # assert
        folderIDs = vtkmrmlutils.getFolderIDs()
        folderID = vtkmrmlutils.getAllFolderChildren(folderIDs[0])
        childID = folderID[0]

        self.assertTrue(6, len(folderIDs))
        self.assertTrue(2, len(folderID))
        self.assertEqual(itemIDs[0], childID)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def standardiseNodes_negative(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject')
        studyID = shNode.CreateStudyItem(subjectID, 'Study')

        # willingly just place node somewhere
        _ = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLScalarVolumeNode', 'some invalid name')

        # act & assert
        with self.assertRaises(errors.ConventionError):
            self.logic.standardiseNodes(studyID)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def standardise_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = testbuilder.setupDICOM()

        # act
        self.logic.standardise()

        # assert
        resultsIDs = vtkmrmlutils.getResultIDs()
        folderIDs = vtkmrmlutils.getFolderIDs()
        folderID = vtkmrmlutils.getAllFolderChildren(folderIDs[0])
        childID = folderID[0]

        self.assertEqual(1, len(resultsIDs))
        self.assertTrue(6, len(folderIDs))
        self.assertTrue(2, len(folderID))
        self.assertEqual(itemIDs[0], childID)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def setObserveColorNode_CT_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        _ = testbuilder.setupNonDICOM()
        volumeNode = slicer.mrmlScene.GetFirstNodeByClass(
            'vtkMRMLScalarVolumeNode')

        # act
        self.logic.setObserveColorNode(volumeNode, self.testValues.node.ct_48h)

        # assert
        displayNode = volumeNode.GetScalarVolumeDisplayNode()
        self.assertEqual(displayNode.GetColorNodeID(),
                         self.logic.GreynodeColor.GetID())

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def importMonteCarlo_hierarchy_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        _ = testbuilder.setupNonDICOM(self.testValues.node.monteCarlo_48h)

        # act
        self.logic.importMonteCarlo()

        # assert
        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        sceneItemID = shNode.GetSceneItemID()

        subjectID = shNode.GetItemChildWithName(sceneItemID, 'Subject')
        studyID = shNode.GetItemChildWithName(subjectID, '48HR')
        volumeNode = shNode.GetItemChildWithName(
            studyID, 'J2:DOSE MonteCarlo 48HR')

        self.assertIsNotNone(subjectID)
        self.assertIsNotNone(studyID)
        self.assertIsNotNone(volumeNode)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def setupIsotope_default_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        name = 'Lu-177'
        energy = 2.3699904492587984e-14
        T_h = 6.647 * 24
        Z = 71
        A = 177

        # act
        self.logic.Isotope = Constants().isotopes[name]

        # assert
        self.assertEqual(name, self.logic.Isotope['name'])
        self.assertEqual(Z, self.logic.Isotope['Z'])
        self.assertEqual(A, self.logic.Isotope['A'])
        self.assertEqual(energy, self.logic.Isotope['energy'])
        self.assertEqual(T_h, self.logic.Isotope['T_h'])

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def setupIsotope_custom_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        name = 'I-131'
        energy = 3.073742414439343e-14
        T_h = 8.0207 * 24
        Z = 53
        A = 131

        # act
        self.logic.Isotope = Constants().isotopes[name]

        # assert
        self.assertEqual(name, self.logic.Isotope['name'])
        self.assertEqual(Z, self.logic.Isotope['Z'])
        self.assertEqual(A, self.logic.Isotope['A'])
        self.assertEqual(energy, self.logic.Isotope['energy'])
        self.assertEqual(T_h, self.logic.Isotope['T_h'])

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def checkAttributes_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        _ = testbuilder.setupDICOM()
        self.logic.standardise()

        # act
        ok = self.logic.checkAttributes()

        # assert
        self.assertTrue(ok)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def checkAttributes_negative(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = testbuilder.setupDICOM()
        vtkmrmlutils.setItemAttribute(
            itemIDs[0], self.attributes.patientID, '0')

        self.logic.standardise()

        # act
        ok = self.logic.checkAttributes()

        # assert
        self.assertFalse(ok)

        self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')

    def FullTest_positive(self):
        self.logger.info(f'Test {utils.whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = testbuilder.setupDICOM()
        name = 'Lu-177'
        Z = 71
        A = 177
        energy = 2.3699904492587984e-14
        T_h = 6.647 * 24

        # act Standardise
        self.logger.info('Standardise started!')
        self.logic.standardise()

        # assert
        resultsIDs = vtkmrmlutils.getResultIDs()
        folderIDs = vtkmrmlutils.getFolderIDs()
        folderID = vtkmrmlutils.getAllFolderChildren(folderIDs[0])
        childID = folderID[0]

        self.assertEqual(1, len(resultsIDs))
        self.assertTrue(6, len(folderIDs))
        self.assertTrue(2, len(folderID))
        self.assertEqual(itemIDs[0], childID)
        self.logger.info('Standardise passed!')
        slicer.app.processEvents()


        # act setupIsotope
        self.logger.info('Setup isotope started!')
        self.logic.Isotope = Constants().isotopes['Lu-177']

        # assert
        self.assertEqual(name, self.logic.Isotope['name'])
        self.assertEqual(Z, self.logic.Isotope['Z'])
        self.assertEqual(A, self.logic.Isotope['A'])
        self.assertEqual(energy, self.logic.Isotope['energy'])
        self.assertEqual(T_h, self.logic.Isotope['T_h'])
        self.logger.info('Setup Isotope passed!')
        slicer.app.processEvents()

        # act Resample CT
        self.logger.info('Resample started!')
        self.logic.resampleCT()

        # assert
        for folderID in folderIDs:
            nodes = Node.getFolderChildren(folderID)
            self.assertTrue('CTRS' in nodes)
            self.assertTrue(vtkmrmlutils.hasImageData(nodes['CTRS'].data))
            if 'CTCT' in nodes and '1HR' in nodes['CTCT'].name:
                referenceNode = nodes['CTCT']
                self.assertTrue(vtkmrmlutils.hasImageData(nodes['CTCT'].data))
        self.assertTrue(vtkmrmlutils.hasImageData(referenceNode.data))
        self.logger.info('Resample passed!')
        slicer.app.processEvents()

        # act Rescale
        self.logger.info('Rescale started!')
        calibration = {}
        calibration['CTCalibration'] = {
            "a0": 0.001, "b0": 1, "a1": 0.0005116346986394071, "b1": 1
        }
        calibration['SPECTSensitivity'] = {
            'Value': 122.6,
            'Units': 'Bq/counts',
            'Time': 1800
        }
        injectedActivity = 6848.0
        self.logic.scaleValues(
            calibration=calibration,
            injectedActivity=injectedActivity
        )

        # assert
        for folderID in folderIDs:
            nodes = Node.getFolderChildren(folderID)
            self.assertTrue('DERS' in nodes)
            self.assertTrue(vtkmrmlutils.hasImageData(nodes['DERS'].data))
            self.assertTrue('DENS' in nodes)
            self.assertTrue(vtkmrmlutils.hasImageData(nodes['DENS'].data))
            self.assertTrue('ACTM' in nodes)
            self.assertTrue(vtkmrmlutils.hasImageData(nodes['ACTM'].data))
        self.logger.info('Rescale passed!')
        slicer.app.processEvents()

        # act registration
        self.logger.info('Registration started!')
        moduleLoader = importlib.util.find_spec("Elastix")
        if not moduleLoader:
            try:
                utils.installModule("SlicerElastix")
                moduleLoader = importlib.util.find_spec("Elastix")
            except errors.IOError as e:
                print(e.message)
        # self.assertTrue(bool(moduleLoader)) # TODO Install Elastix by default and activate this test
        # TODO add statistics for registration
        self.logic.createTransforms(referenceNode.data)
        self.logger.info('Registration passed!')
        slicer.app.processEvents()

        # load segmentation
        self.logger.info('Segmentation started!')
        segNode = testbuilder.loadSegmentation()
        self.assertTrue(vtkmrmlutils.hasSegmentation(segNode.data))
        self.logic.Propagate(segNode.data)
        self.logger.info('Segmentation passed!')
        slicer.app.processEvents()

        self.logger.info('\n******** ACTM workflow Tests started ***************\n')

        self.logger.info('create tables started!')
        self.logic.computeNewValues(mode='Activity')
        self.logger.info('create tables passed!')
        slicer.app.processEvents()

        self.logger.info('create plots started!')
        self.logic.processTimeIntegrationVariables(mode='Activity')
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode='Activity', showlegend='False')
        self.logger.info('create plots passed!')
        slicer.app.processEvents()

        self.logger.info('integration started!')
        self.logic.integrate(
            mode='Activity',
            algorithm='',
            incorporationmode='exponential',
            integrationmode='auto-fit'
        )
        self.logger.info('integration passed!')
        slicer.app.processEvents()

        self.logger.info('\n******** ACTM workflow Tests passed ***************\n')
        self.logger.info('\n******** ADR workflow Tests started ***************\n')

        self.logger.info('Convolution started!')
        DRMode = 'FFT convolution (homogeneous)'
        self.logic.convolution(
                    isotopeText='Lu-177',
                    DVK_density=1.00,
                    threshold=0.0,
                    kernellimit=0.0,
                    mode=DRMode,
                    multithreaded=False
                )
        self.logger.info('Convolution passed!')
        slicer.app.processEvents()

        self.logger.info('create tables started!')
        self.logic.computeNewValues(mode='DoseRate', algorithm=DRMode)
        self.logger.info('create tables passed!')
        slicer.app.processEvents()

        self.logger.info('create plots started!')
        self.logic.processTimeIntegrationVariables(mode='DoseRate', algorithm=DRMode)
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode='DoseRate', algorithm=DRMode, showlegend='False')
        self.logger.info('create plots passed!')
        slicer.app.processEvents()

        self.logger.info('integration started!')
        self.logic.integrate(
            mode='DoseRate',
            algorithm=DRMode,
            incorporationmode='exponential',
            integrationmode='auto-fit'
        )
        self.logger.info('integration passed!')
        slicer.app.processEvents()

        self.logger.info('\n******** ADR workflow Tests passed ***************\n')

        #TODO add statistics and output verification

        if utils.checkTesting():
            self.tearDown()
        self.logger.info(f'Test {utils.whoami()} passed!')