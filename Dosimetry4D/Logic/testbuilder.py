import qt
import slicer
from pathlib import Path

import DICOMLib.DICOMUtils as DICOMUtils
from DICOMScalarVolumePlugin import DICOMScalarVolumePluginClass

from Logic.attributes import Attributes
import Logic.logging as logging
from Logic.testutils import AttributeValues, NodeValues, DicomValues, SegmentationValues
import Logic.vtkmrmlutils as vtkmrmlutils
from Logic.errors import Error
import Logic.utils as utils
from Logic.nodes import Node

DATABASE_INITIALIZED = False

def setupNonDICOM(name='J2:CTCT 48h'):
    """
    Setup up a volume node for testing
    """
    attributes = Attributes()
    attributeValues = AttributeValues()
    nodeValues = NodeValues()

    # create hierarchy
    shNode = vtkmrmlutils.getSubjectHierarchyNode()
    subjectID = shNode.CreateSubjectItem(shNode.GetSceneItemID(), 'Subject')
    studyID = shNode.CreateStudyItem(subjectID, 'Study')

    # create nodes
    defaultVolumeNode = slicer.mrmlScene.AddNewNodeByClass(
        'vtkMRMLScalarVolumeNode', nodeValues.acsc_48h)
    defaultItemID = shNode.GetItemByDataNode(defaultVolumeNode)

    volumeNode = slicer.mrmlScene.AddNewNodeByClass(
        'vtkMRMLScalarVolumeNode', name)
    itemID = shNode.GetItemByDataNode(volumeNode)

    shNode.SetItemParent(defaultItemID, studyID)
    shNode.SetItemParent(itemID, studyID)

    # attributes
    vtkmrmlutils.setItemDataNodeAttribute(
        defaultItemID, attributes.acquisition, attributeValues.acquisition)
    vtkmrmlutils.setItemDataNodeAttribute(
        defaultItemID, attributes.acquisitionDuration, attributeValues.acquisitionDuration)
    vtkmrmlutils.setItemDataNodeAttribute(
        defaultItemID, attributes.modality, attributeValues.modality_acsc)

    vtkmrmlutils.setItemAttribute(
        defaultItemID, attributes.acquisition, attributeValues.acquisition)

    return defaultItemID, itemID


def setupDICOM():
    '''
    Setup a complex environment based on the loaded dicom files
    '''
    logger = logging.getLogger('Dosimetry4D.testbuilder')
    dicomValues = DicomValues()

    try:
        openDICOMDatabase()
    except:
        raise

    try:
        slicer.modules.dicomPlugins
    except:
        slicer.modules.dicomPlugins = {}

    if slicer.modules.dicomPlugins:
        oldPluginList = slicer.modules.dicomPlugins
        slicer.modules.dicomPlugins = {}
    else:
        oldPluginList = {}

    slicer.modules.dicomPlugins['DICOMScalarVolumePlugin'] = DICOMScalarVolumePluginClass

    try:
        _ = DICOMUtils.loadPatientByName(dicomValues.patientName)
    except:
        logger.error('Could not load dicom files from database')
        raise Error('Unable to load series from database')

    studyID = vtkmrmlutils.getStudyIDs()[0]
    itemIDs = vtkmrmlutils.getAllFolderChildren(studyID)

    slicer.modules.dicomPlugins = oldPluginList

    return itemIDs


def initDICOMDatabase():
    '''
    Writes the data from folder <./Dosimetry4D/Resources/Dicom> to a dicom database
    '''
    global DATABASE_INITIALIZED
    logger = logging.getLogger('Dosimetry4D.testbuilder')
    dicomValues = DicomValues()

    try:
        openDICOMDatabase()
    except:
        raise

    if DATABASE_INITIALIZED:
        logger.info("Database is already initialized")
        return

    # see https://github.com/Slicer/Slicer/blob/master/Modules/Scripted/DICOMLib/DICOMUtils.py#L240
    logger.error('DICOM test data dir: ' + dicomValues.folderPath)
    success = DICOMUtils.importDicom(dicomValues.folderPath)
    if not success:
        logger.error('Could not import DICOM test data from directory: {}'.format(
            dicomValues.folderPath))

    # NOTE currently assumes that only 1 subject -> 1 study -> 1 series is loaded
    patientList = slicer.dicomDatabase.patients()
    studyList = slicer.dicomDatabase.studiesForPatient(patientList[0])
    seriesList = slicer.dicomDatabase.seriesForStudy(studyList[0])

    logger.error('Successfully loaded patient {} with StudyInstanceUID: {} and SeriesInstanceUID: {}'.format(
        patientList[0], studyList[0], seriesList[0]))

    DATABASE_INITIALIZED = True


def openDICOMDatabase():
    '''
    Opens a connection to the dicom database
    '''
    # NOTE this method uses a hardcoded dicom directory
    # see: https://github.com/Slicer/Slicer/blob/master/Modules/Scripted/DICOMLib/DICOMUtils.py#L60
    logger = logging.getLogger('Dosimetry4D.testbuilder')

    if slicer.dicomDatabase.isOpen:
        logger.info('Database is already opened')
        return

    settings = qt.QSettings()
    if not settings.contains('DatabaseDirectory'):
        DB_Directory = Path.home() / 'SlicerDICOMDatabase'
        qt.QSettings().setValue('DatabaseDirectory', str(DB_Directory))

    try:
        databaseDirectory = Path(settings.value('DatabaseDirectory'))
        databaseDirectory.mkdir(exist_ok=True)
    except FileNotFoundError as e:
        logger.error("The database directory cannot be created \n{}".format(e))

    try:
        databaseFilePath = databaseDirectory / 'ctkDICOM.sql'
        databaseFilePath.touch(exist_ok=True)
    except Exception as e:
        logger.error("The database directory cannot be created \n{}: {}".format(type(e), e))

    logger.info(f"Database created in {databaseFilePath}")
    if not (databaseDirectory.is_dir() and databaseFilePath.is_file()):
        logger.error(
            f"The database file path '{databaseFilePath}' cannot be opened, please adjust the read/write priviliges")
        raise OSError('Database has invalid read/write privilidges')

    slicer.dicomDatabase.openDatabase(str(databaseFilePath))
    if not slicer.dicomDatabase.isOpen:
        logger.error('Unable to reconnect database')
        raise Error('Unable to connect to database')

def loadSegmentation():
    '''
    Loads the predefined segmentation for the tests
    '''
    # NOTE this method uses a hardcoded segmentation file defined exclusively for testing
    segmentationValues = SegmentationValues()

    seg = slicer.util.loadSegmentation(segmentationValues.filePath)
    seg.CreateDefaultDisplayNodes()
    seg.CreateClosedSurfaceRepresentation()
    segID = vtkmrmlutils.getItemID(seg)
    vtkmrmlutils.setItemName(segID, "J0:SEGM 1HR")
    return Node.new(segID)
